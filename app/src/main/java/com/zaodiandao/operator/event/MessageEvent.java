package com.zaodiandao.operator.event;

/**
 * Event Bus消息实体
 */
public class MessageEvent {
    public final int code;
    public final String message;

    public MessageEvent(int code, String message) {
        this.code = code;
        this.message = message;
    }
}