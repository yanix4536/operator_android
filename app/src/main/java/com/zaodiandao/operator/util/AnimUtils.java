package com.zaodiandao.operator.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;

/**
 * Created by yangx on 2015/12/31.
 */
public class AnimUtils {
    private ViewGroup anim_mask_layout; // 动画层
    private Activity activity;
    private ImageView shopCart; // 购物车
    TranslateAnimation translateAnimationX;
    TranslateAnimation translateAnimationY;
    AnimationSet set;
    ScaleAnimation big;

    public AnimUtils(Activity activity, ImageView shopCart){
        this.activity = activity;
        this.shopCart = shopCart;
        big = new ScaleAnimation(1.0f, 1.3f, 1.0f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    }

    public Bitmap getAddDrawBitMap() {
        Tools tools = new Tools();
        View drawableViewPar = LayoutInflater.from(activity).inflate(R.layout.food_list_item_operation, null);
        ImageView ball = (ImageView) drawableViewPar.findViewById(R.id.iv_ball);
        return tools.convertViewToBitmap(ball);
    }

    private View addViewToAnimLayout(final ViewGroup vg, final View view, int[] location) {
        int x = location[0];
        int y = location[1];
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = x - view.getWidth()/2;
        lp.topMargin = y - view.getHeight()/2;
        view.setLayoutParams(lp);
        return view;
    }

    private ViewGroup createAnimLayout(ViewGroup viewGroup) {
        ViewGroup rootView = null;
        if (viewGroup == null) {
            rootView = (ViewGroup) activity.getWindow().getDecorView();
        } else {
            rootView = viewGroup;
        }
        LinearLayout animLayout = new LinearLayout(activity);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        animLayout.setLayoutParams(lp);
        animLayout.setBackgroundResource(android.R.color.transparent);
        rootView.addView(animLayout);
        return animLayout;
    }

    public void setAnim(final View v, int[] start_location, ViewGroup viewGroup) {
        anim_mask_layout = createAnimLayout(viewGroup);
        anim_mask_layout.addView(v);// 把动画小球添加到动画层
        final View view = addViewToAnimLayout(anim_mask_layout, v, start_location);
        int[] end_location = new int[2];// 这是用来存储动画结束位置的X、Y坐标
        shopCart.getLocationInWindow(end_location);// shopCart是那个购物车

        // 计算位移
        int endX = 0 - start_location[0] + shopCart.getMeasuredWidth()/2;// 动画位移的X坐标
        int endY = end_location[1] - start_location[1];// 动画位移的y坐标
        translateAnimationX = new TranslateAnimation(-20, endX, 0, 0);
        translateAnimationX.setInterpolator(new LinearInterpolator());
        translateAnimationX.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationX.setFillAfter(true);

        translateAnimationY = new TranslateAnimation(0, 0, 0, endY);
        translateAnimationY.setInterpolator(new AccelerateInterpolator());
        translateAnimationY.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationX.setFillAfter(true);

        set = new AnimationSet(false);
        set.setFillAfter(false);
        set.addAnimation(translateAnimationY);
        set.addAnimation(translateAnimationX);
        set.setDuration(400);// 动画的执行时间
        view.startAnimation(set);
        // 动画监听事件
        set.setAnimationListener(new Animation.AnimationListener() {
            // 动画的开始
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            // 动画的结束
            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
                /*if (big == null) {
                    big = new ScaleAnimation(1.0f, 1.3f, 1.0f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                }
                big.setDuration(500);
                shopCart.startAnimation(big);*/
                if (translateAnimationX != null) {
                    translateAnimationX = null;
                }
                if (translateAnimationY != null) {
                    translateAnimationY = null;
                }
                if (set != null) {
                    set = null;
                }
            }
        });
    }

    public void setAnimReverse(final View v, int[] start_location, ViewGroup viewGroup) {
        anim_mask_layout = createAnimLayout(viewGroup);
        anim_mask_layout.addView(v);// 把动画小球添加到动画层
        int[] end_location = new int[2];// 这是用来存储动画结束位置的X、Y坐标
        shopCart.getLocationInWindow(end_location);// shopCart是那个购物车
        final View view = addViewToAnimLayout(anim_mask_layout, v, end_location);

        // 计算位移
        int endX = start_location[0] - 40;// 动画位移的X坐标
        // int endY = start_location[1] + shopCart.getHeight() - end_location[1];// 动画位移的y坐标
        float endY = end_location[1]/2;// 动画位移的y坐标
        TranslateAnimation translateAnimationX = new TranslateAnimation(0, endX, 0, 0);
        translateAnimationX.setInterpolator(new LinearInterpolator());
        translateAnimationX.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationX.setFillAfter(true);

        TranslateAnimation translateAnimationY = new TranslateAnimation(0, 0, 0, endY);
        translateAnimationY.setInterpolator(new AccelerateInterpolator());
        translateAnimationY.setRepeatCount(0);// 动画重复执行的次数
        translateAnimationX.setFillAfter(true);

        AnimationSet set = new AnimationSet(false);
        set.setFillAfter(false);
        set.addAnimation(translateAnimationY);
        set.addAnimation(translateAnimationX);
        set.setDuration(400);// 动画的执行时间
        view.startAnimation(set);
        // 动画监听事件
        set.setAnimationListener(new Animation.AnimationListener() {
            // 动画的开始
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
                ScaleAnimation big = new ScaleAnimation(1.0f, 0.7f, 1.0f, 0.7f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                big.setDuration(500);
                shopCart.startAnimation(big);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            // 动画的结束
            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
            }
        });
    }

    public static void gone(final View v) {
        /*v.animate()
                .alpha(0f)
                .setDuration(Constant.ANIM_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setVisibility(View.GONE);
                    }
                });*/
        v.setVisibility(View.GONE);
    }

    public static void visible(View v) {
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        v.setAlpha(0f);
        v.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        v.animate()
                .alpha(1f)
                .setDuration(KeyConstants.ANIM_DURATION)
                .setListener(null);
    }

    public static void crossfade(View content, final View hide) {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        content.setAlpha(0f);
        content.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        content.animate()
                .alpha(1f)
                .setDuration(KeyConstants.ANIM_DURATION)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        /*hide.animate()
                .alpha(0f)
                .setDuration(Constant.ANIM_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hide.setVisibility(View.GONE);
                    }
                });*/
        hide.setVisibility(View.GONE);
    }

}
