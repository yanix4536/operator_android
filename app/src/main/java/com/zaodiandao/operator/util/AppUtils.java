package com.zaodiandao.operator.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by yanix on 2016/12/6.
 * 应用信息工具类
 */
public class AppUtils {
    private static AppUtils instance = null;

    private AppUtils() {}

    public static AppUtils getInstance() {
        if (instance == null)
            instance = new AppUtils();

        return instance;
    }

    /**
     * 获取版本名称
     */
    public String getVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "未知版本";
        }
    }

    /**
     * 获取版本号
     */
    public String getVersionCode(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String versionCode = info.versionCode + "";
            return versionCode ;
        } catch (Exception e) {
            e.printStackTrace();
            return "未知版本号";
        }
    }
}
