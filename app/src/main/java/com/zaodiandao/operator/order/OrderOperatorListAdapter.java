package com.zaodiandao.operator.order;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.OrderListBean;

import java.util.List;

public class OrderOperatorListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<OrderListBean.OrderOperatorBean> mOrders;

    public OrderOperatorListAdapter(Context context, List<OrderListBean.OrderOperatorBean> orders) {
        mContext = context;
        mOrders = orders;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new OrderOperatorListAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_order_operator_list, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, OrderListBean.OrderOperatorBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            OrderOperatorListAdapter.EmptyViewViewHolder emptyViewViewHolder = (OrderOperatorListAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无订货单");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            OrderListBean.OrderOperatorBean data = mOrders.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvShopName.setText(data.getShop_name());
            myViewHolder.tvOrderNumber.setText("订单号：" + data.getOrdernumber());
            myViewHolder.tvDeliverTime.setText(data.getDelivertime());
            myViewHolder.tvMoney.setText("￥" + data.getMoney());
            if (data.getStatus() == 5) {
                myViewHolder.tvOrderStatus.setText("未支付");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#F3A536"));
            } else if (data.getStatus() == 10) {
                myViewHolder.tvOrderStatus.setText("未审核");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 20) {
                myViewHolder.tvOrderStatus.setText("已审核");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 50) {
                myViewHolder.tvOrderStatus.setText("已配送");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 60) {
                myViewHolder.tvOrderStatus.setText("已完成");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (data.getStatus() == 80) {
                myViewHolder.tvOrderStatus.setText("已取消");
                myViewHolder.tvOrderStatus.setTextColor(Color.parseColor("#13A800"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mOrders != null && mOrders.size()>0) ? mOrders.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrders == null || mOrders.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (OrderListBean.OrderOperatorBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvShopName, tvOrderNumber, tvOrderStatus, tvDeliverTime, tvMoney;

        public MyViewHolder(View view) {
            super(view);
            tvShopName = (TextView) view.findViewById(R.id.tv_shop_name);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvOrderStatus = (TextView) view.findViewById(R.id.tv_order_status);
            tvDeliverTime = (TextView) view.findViewById(R.id.tv_deliver_time);
            tvMoney = (TextView) view.findViewById(R.id.tv_money);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}