package com.zaodiandao.operator.order;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.OrderDetailModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.SPUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/1/13.
 * 订单详情
 */
public class OrderDetailActivity extends BaseActivity {
    public static final String ORDER_ID = "order_id";
    @BindView(R.id.ll_menu)
    LinearLayout llMenu;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.pb_loading)
    View mProgressBar;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @BindView(R.id.tv_linkman)
    TextView tvLinkman;
    @BindView(R.id.tv_mobile)
    TextView tvMobile;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_deliver_time)
    TextView tvDeliverTime;
    @BindView(R.id.tv_vehicle_number)
    TextView tvVehicleNumber;
    @BindView(R.id.tv_driver_name)
    TextView tvDriverName;
    @BindView(R.id.tv_driver_mobile)
    TextView tvDriverMobile;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_product_money)
    TextView tvProductMoney;
    @BindView(R.id.tv_deliver_fee)
    TextView tvDeliverFee;
    @BindView(R.id.tv_discount_money)
    TextView tvDiscountMoney;
    @BindView(R.id.tv_sum)
    TextView tvSum;

    private String orderId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        orderId = getIntent().getStringExtra(ORDER_ID);
        getOrderDetail();
    }

    private void getOrderDetail() {
        mApiService.getOrderDetail(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), orderId,
                new GenericCallback<OrderDetailModel>(getApplicationContext(), OrderDetailModel.class) {

                    @Override
                    public void success(OrderDetailModel data) {
                        tvOrderNumber.setText(data.getOrdernumber());
                        tvLinkman.setText(data.getLinkman());
                        tvMobile.setText(data.getMobile());
                        tvAddress.setText(data.getAddress());
                        tvDeliverTime.setText(data.getDelivertime());
                        tvVehicleNumber.setText(data.getVehiclenumber());
                        tvDriverName.setText(data.getDrivername());
                        tvDriverMobile.setText(data.getDrivermobile());
                        int status = data.getStatus();
                        // 5=未支付 10未审核 20=已审核 50=已配送 60=已完成 80=已取消
                        if (status == 5) {
                            tvStatus.setText("未支付");
                        } else if (status == 10) {
                            tvStatus.setText("未审核");
                        } else if (status == 20) {
                            tvStatus.setText("已审核");
                        } else if (status == 50) {
                            tvStatus.setText("已配送");
                        } else if (status == 60) {
                            tvStatus.setText("已完成");
                        } else if (status == 80) {
                            tvStatus.setText("已取消");
                        } else {
                            tvStatus.setText("未知状态");
                        }
                        tvProductMoney.setText("￥" + data.getProductmoney());
                        tvDeliverFee.setText("￥" + data.getDelivermoney());
                        tvDiscountMoney.setText("-￥" + data.getDiscountmoney());
                        tvSum.setText("￥" + data.getSummoney());
                        llContent.setVisibility(View.VISIBLE);
                        initMenu(data.getProductdata());
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void initMenu(List<OrderDetailModel.BrandBean> brands) {
        llMenu.removeAllViews();

        for (OrderDetailModel.BrandBean brandBean : brands) {
            View v = getLayoutInflater().inflate(R.layout.listitem_brand, null);
            TextView tvBrandName = (TextView) v.findViewById(R.id.tv_brand_name);
            TextView tvDeliverFee = (TextView) v.findViewById(R.id.tv_deliver_fee);
            LinearLayout llProduct = (LinearLayout) v.findViewById(R.id.ll_product);
            tvBrandName.setText(brandBean.getBrandname());
            tvDeliverFee.setText("￥" + brandBean.getDelivermoney());

            for (OrderDetailModel.BrandBean.ProductBean productBean : brandBean.getProducts()) {
                View vProduct = getLayoutInflater().inflate(R.layout.item_product, null);
                TextView tvProductName = (TextView) vProduct.findViewById(R.id.tv_product_name);
                TextView tvCount = (TextView) vProduct.findViewById(R.id.tv_count);
                TextView tvMoney = (TextView) vProduct.findViewById(R.id.tv_money);
                tvProductName.setText(productBean.getName());
                tvCount.setText("x" + productBean.getAmount());
                tvMoney.setText("￥" + productBean.getMoney());
                llProduct.addView(vProduct);
            }
            llMenu.addView(v);
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getOrderDetail();
    }
}
