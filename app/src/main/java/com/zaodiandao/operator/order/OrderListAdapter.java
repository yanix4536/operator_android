package com.zaodiandao.operator.order;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.OrderListBean;
import com.zaodiandao.operator.util.expandablelist.SlideExpandableListAdapter;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<OrderListBean> mOrders;
    private OrderOperatorListAdapter adapter;
    private SlideExpandableListAdapter slideExpandableListAdapter;

    public OrderListAdapter(Context context, List<OrderListBean> orders) {
        mContext = context;
        mOrders = orders;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new OrderListAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_order_list, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, OrderListBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            OrderListAdapter.EmptyViewViewHolder emptyViewViewHolder = (OrderListAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无订货单");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            OrderListBean data = mOrders.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvOperatorName.setText(data.getTitle());
            adapter = new OrderOperatorListAdapter(mContext, data.getOrders());
            adapter.setOnItemClickListener(new OrderOperatorListAdapter.OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(View view, OrderListBean.OrderOperatorBean data) {
                    Intent intent = new Intent(mContext, OrderDetailActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(OrderDetailActivity.ORDER_ID, data.getOrder_id());
                    mContext.startActivity(intent);
                }
            });
            myViewHolder.innerRecycleView.setLayoutManager(new LinearLayoutManager(mContext) {

                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });
            myViewHolder.innerRecycleView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return (mOrders != null && mOrders.size()>0) ? mOrders.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrders == null || mOrders.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (OrderListBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOperatorName;
        RecyclerView innerRecycleView;

        public MyViewHolder(View view) {
            super(view);
            tvOperatorName = (TextView) view.findViewById(R.id.tv_operator_name);
            innerRecycleView = (RecyclerView) view.findViewById(R.id.inner_recyclerView);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}