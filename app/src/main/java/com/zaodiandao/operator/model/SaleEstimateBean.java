package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/2/22.
 * 预估销量
 */
public class SaleEstimateBean {
    private int e_no;
    private List<EstimateItem> e_list;

    public int getE_no() {
        return e_no;
    }

    public void setE_no(int e_no) {
        this.e_no = e_no;
    }

    public List<EstimateItem> getE_list() {
        return e_list;
    }

    public void setE_list(List<EstimateItem> e_list) {
        this.e_list = e_list;
    }

    public static class EstimateItem {
        private String estimate_id;
        private String ordernumber;
        private int status_id;   // 状态值(1=未/待审核 2=已审核 3=已配送 4=已入库 )
        private String status_label;
        private String create_time;
        private String check_time;

        public String getEstimate_id() {
            return estimate_id;
        }

        public void setEstimate_id(String estimate_id) {
            this.estimate_id = estimate_id;
        }

        public String getOrdernumber() {
            return ordernumber;
        }

        public void setOrdernumber(String ordernumber) {
            this.ordernumber = ordernumber;
        }

        public int getStatus_id() {
            return status_id;
        }

        public void setStatus_id(int status_id) {
            this.status_id = status_id;
        }

        public String getStatus_label() {
            return status_label;
        }

        public void setStatus_label(String status_label) {
            this.status_label = status_label;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getCheck_time() {
            return check_time;
        }

        public void setCheck_time(String check_time) {
            this.check_time = check_time;
        }
    }
}
