package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2016/12/21.
 * 订货单详情实体类
 */
public class ReserveOrderDetail {
    private String ordernumber;
    private String delivertime;
    private String summoney;
    private String shopname;
    private String linkman;
    private String mobile;
    private String address;
    private String discountmoney;       // 优惠金额
    private String delivermoney;        // 配送费
    private List<ProductBean> products;

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(String delivertime) {
        this.delivertime = delivertime;
    }

    public String getSummoney() {
        return summoney;
    }

    public void setSummoney(String summoney) {
        this.summoney = summoney;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ProductBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductBean> products) {
        this.products = products;
    }

    public String getDiscountmoney() {
        return discountmoney;
    }

    public void setDiscountmoney(String discountmoney) {
        this.discountmoney = discountmoney;
    }

    public String getDelivermoney() {
        return delivermoney;
    }

    public void setDelivermoney(String delivermoney) {
        this.delivermoney = delivermoney;
    }

    public static class ProductBean {
        private String productname;
        private String sellunit;
        private String amount;
        private String money;

        public String getProductname() {
            return productname;
        }

        public void setProductname(String productname) {
            this.productname = productname;
        }

        public String getSellunit() {
            return sellunit;
        }

        public void setSellunit(String sellunit) {
            this.sellunit = sellunit;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }
}
