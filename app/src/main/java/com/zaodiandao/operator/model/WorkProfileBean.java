package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2017/4/27.
 * 工作资料
 */
public class WorkProfileBean {
    private String title;
    private String date;
    private String art_url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getArt_url() {
        return art_url;
    }

    public void setArt_url(String art_url) {
        this.art_url = art_url;
    }
}
