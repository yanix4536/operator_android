package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2016/11/28.
 * 历史销售数据实体类
 */
public class HistorySale {
    private String brandname;       // 品牌名
    private List<SaledataBean> saledata;

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public List<SaledataBean> getSaledata() {
        return saledata;
    }

    public void setSaledata(List<SaledataBean> saledata) {
        this.saledata = saledata;
    }

    /**
     * 销售数据实体
     */
    public static class SaledataBean {
        private String salesname;       // 销售月份
        private String salesamount;     // 销售总额
        private String payment;            // 销售提成
        private String riseamount;      // 增长量

        public String getSalesname() {
            return salesname;
        }

        public void setSalesname(String salesname) {
            this.salesname = salesname;
        }

        public String getSalesamount() {
            return salesamount;
        }

        public void setSalesamount(String salesamount) {
            this.salesamount = salesamount;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getRiseamount() {
            return riseamount;
        }

        public void setRiseamount(String riseamount) {
            this.riseamount = riseamount;
        }
    }
}
