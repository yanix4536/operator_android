package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2016/12/1.
 * 城市区县等数据实体类
 */
public class CityData {
    private int city_id;
    private List<ShopDetail.CitycountrysBean> citycountrys;
    private List<ShopDetail.GradesBean> grades;
    private List<ShopDetail.ImplantationBoxsBean> implantation_boxs;

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public List<ShopDetail.CitycountrysBean> getCitycountrys() {
        return citycountrys;
    }

    public void setCitycountrys(List<ShopDetail.CitycountrysBean> citycountrys) {
        this.citycountrys = citycountrys;
    }

    public List<ShopDetail.GradesBean> getGrades() {
        return grades;
    }

    public void setGrades(List<ShopDetail.GradesBean> grades) {
        this.grades = grades;
    }

    public List<ShopDetail.ImplantationBoxsBean> getImplantation_boxs() {
        return implantation_boxs;
    }

    public void setImplantation_boxs(List<ShopDetail.ImplantationBoxsBean> implantation_boxs) {
        this.implantation_boxs = implantation_boxs;
    }
}
