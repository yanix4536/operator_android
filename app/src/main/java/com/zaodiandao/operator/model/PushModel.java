package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2017/4/10.
 */
public class PushModel {
    private String title;
    private String body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
