package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/3/31.
 * 商户销售详情
 */
public class DeductModel {
    private String city_id;
    private List<City> citys;
    private String brand_id;
    private List<Brand> brands;
    private String month;
    private List<String> months;
    private String total_ordernum;
    private String total_money;
    private List<OrderBean> orders;

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public List<City> getCitys() {
        return citys;
    }

    public void setCitys(List<City> citys) {
        this.citys = citys;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<String> getMonths() {
        return months;
    }

    public void setMonths(List<String> months) {
        this.months = months;
    }

    public String getTotal_ordernum() {
        return total_ordernum;
    }

    public void setTotal_ordernum(String total_ordernum) {
        this.total_ordernum = total_ordernum;
    }

    public String getTotal_money() {
        return total_money;
    }

    public void setTotal_money(String total_money) {
        this.total_money = total_money;
    }

    public List<OrderBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderBean> orders) {
        this.orders = orders;
    }

    public static class City {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Brand {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class OrderBean {
        private String order_id;
        private String ordernumber;
        private String checkout_time;
        private String salesman_money;

        public OrderBean(){

        }

        public OrderBean(String order_id, String ordernumber, String checkout_time, String salesman_money) {
            this.order_id = order_id;
            this.ordernumber = ordernumber;
            this.checkout_time = checkout_time;
            this.salesman_money = salesman_money;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrdernumber() {
            return ordernumber;
        }

        public void setOrdernumber(String ordernumber) {
            this.ordernumber = ordernumber;
        }

        public String getCheckout_time() {
            return checkout_time;
        }

        public void setCheckout_time(String checkout_time) {
            this.checkout_time = checkout_time;
        }

        public String getSalesman_money() {
            return salesman_money;
        }

        public void setSalesman_money(String salesman_money) {
            this.salesman_money = salesman_money;
        }
    }
}
