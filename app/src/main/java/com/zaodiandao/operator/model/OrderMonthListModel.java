package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/1/11.
 * 历史订单实体类
 */
public class OrderMonthListModel {
    private List<DateFilter> title;
    private List<OrderListBean.OrderOperatorBean> orders;

    public List<DateFilter> getTitle() {
        return title;
    }

    public void setTitle(List<DateFilter> title) {
        this.title = title;
    }

    public List<OrderListBean.OrderOperatorBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderListBean.OrderOperatorBean> orders) {
        this.orders = orders;
    }

    public static class DateFilter {
        private String step;
        private String name;

        public String getStep() {
            return step;
        }

        public void setStep(String step) {
            this.step = step;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
