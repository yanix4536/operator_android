package com.zaodiandao.operator.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2016/11/24.
 * 商户分布实体类
 */
public class ShopDistribute implements Serializable{
    private int clerk_id;   // 业务员ID
    private int city_id;    // 当前城市ID
    private int brand_id;   // 当前品牌ID
    private int brand_radius;   // 门店保护范围 单位：米
    private ShopdataBean shopdata;  // 门店分布
    private List<CityBean> citys;  // 城市列表
    private List<BrandBean> brands;    // 当前品牌列表

    public int getClerk_id() {
        return clerk_id;
    }

    public void setClerk_id(int clerk_id) {
        this.clerk_id = clerk_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getBrand_radius() {
        return brand_radius;
    }

    public void setBrand_radius(int brand_radius) {
        this.brand_radius = brand_radius;
    }

    public ShopdataBean getShopdata() {
        return shopdata;
    }

    public void setShopdata(ShopdataBean shopdata) {
        this.shopdata = shopdata;
    }

    public List<CityBean> getCitys() {
        return citys;
    }

    public void setCitys(List<CityBean> citys) {
        this.citys = citys;
    }

    public List<BrandBean> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandBean> brands) {
        this.brands = brands;
    }

    /**
     * 门店集合
     */
    public static class ShopdataBean {
        private List<PositionBean> partner;      // 已合作门店
        private List<PositionBean> check;          // 审核中门店
        private List<PositionBean> invalid;      // 未审核门店

        public List<PositionBean> getPartner() {
            return partner;
        }

        public void setPartner(List<PositionBean> partner) {
            this.partner = partner;
        }

        public List<PositionBean> getCheck() {
            return check;
        }

        public void setCheck(List<PositionBean> check) {
            this.check = check;
        }

        public List<PositionBean> getInvalid() {
            return invalid;
        }

        public void setInvalid(List<PositionBean> invalid) {
            this.invalid = invalid;
        }

        /**
         * 门店经纬度
         */
        public static class PositionBean {
            private double longitude;
            private double latitude;
            private String name;        // 店名
            private String address;     // 地址
            private int protection;     // 1=保护 2=释放

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public int getProtection() {
                return protection;
            }

            public void setProtection(int protection) {
                this.protection = protection;
            }
        }
    }

    /**
     * 城市
     */
    public static class CityBean {
        private int id;         // 城市id
        private String name;    // 城市名
        private double longitude;
        private double latitude;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
    }

    /**
     * 品牌
     */
    public static class BrandBean implements Serializable{
        private int id;         // 品牌id
        private String name;    // 品牌名

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
