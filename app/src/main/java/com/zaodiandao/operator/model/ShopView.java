package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2016/11/29.
 * 门店查看实体类
 */
public class ShopView {
    private String shop_id;         // 商户ID
    private String shopname;        // 商户名
    private String brandname;       // 品牌名
    private String name;            // 门店名
    private int brand_id;           // 当前品牌ID
    private List<BrandsBean> brands;    // 品牌集合
    private List<ImageBean> img;    // 图片集合

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public List<BrandsBean> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandsBean> brands) {
        this.brands = brands;
    }

    public List<ImageBean> getImg() {
        return img;
    }

    public void setImg(List<ImageBean> img) {
        this.img = img;
    }

    public static class BrandsBean {
        private int id;         // 品牌ID
        private String name;    // 品牌名称

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class ImageBean {
        private String img_id;      // 图片ID
        private String remark;      // 图片说明
        private String imgurl = "";       // 图片地址

        public ImageBean() {}

        public ImageBean(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getImg_id() {
            return img_id;
        }

        public void setImg_id(String img_id) {
            this.img_id = img_id;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (obj instanceof ImageBean) {
                ImageBean other = (ImageBean) obj;
                return imgurl.equals(other.getImgurl());
            }
            return super.equals(obj);
        }
    }

}
