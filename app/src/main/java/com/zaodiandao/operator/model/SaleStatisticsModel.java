package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/2/22.
 */
public class SaleStatisticsModel {
    private List<BrandBean> brands;
    private List<String> months;
    private List<SaleProduct> pdata;

    public List<BrandBean> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandBean> brands) {
        this.brands = brands;
    }

    public List<String> getMonths() {
        return months;
    }

    public void setMonths(List<String> months) {
        this.months = months;
    }

    public List<SaleProduct> getPdata() {
        return pdata;
    }

    public void setPdata(List<SaleProduct> pdata) {
        this.pdata = pdata;
    }

    public static class BrandBean {
        private String brand_id;
        private String brand_name;

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }
    }

    public static class SaleProduct {
        private String product_name;
        private String format;
        private String unit;
        private String sell_amount;
        private String sell_money;

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getSell_amount() {
            return sell_amount;
        }

        public void setSell_amount(String sell_amount) {
            this.sell_amount = sell_amount;
        }

        public String getSell_money() {
            return sell_money;
        }

        public void setSell_money(String sell_money) {
            this.sell_money = sell_money;
        }
    }
}
