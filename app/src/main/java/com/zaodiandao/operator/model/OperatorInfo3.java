package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/3/21.
 * 业务员信息
 */
public class OperatorInfo3 {
    /**
     * 业务员姓名
     */
    private String name;

    /**
     * 业务员头像
     */
    private String avatar;

    /**
     * 业务员手机号
     */
    private String mobile;

    private String show_sale_data;

    private List<DeductBean> city_info;


    public static class DeductBean {
        private String city_id;
        private String city_name;
        /**
         * 已开店数
         */
        private String shop_numed;

        /**
         * 可开店数
         */
        private String shop_numing;

        /**
         * 本月销量
         */
        private String staplefood;

        /**
         * 本月提成系数
         */
        private String coefficiented;

        /**
         * 本月提成预估
         */
        private String salesmanfee;

        /**
         * 下月提成系数
         */
        private String coefficienting;

        /**
         * 可升至的提成系数
         */
        private String coefficientnext;

        /**
         * 可升至的提成系数的差额
         */
        private String staplefoodnext;

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getShop_numed() {
            return shop_numed;
        }

        public void setShop_numed(String shop_numed) {
            this.shop_numed = shop_numed;
        }

        public String getShop_numing() {
            return shop_numing;
        }

        public void setShop_numing(String shop_numing) {
            this.shop_numing = shop_numing;
        }

        public String getStaplefood() {
            return staplefood;
        }

        public void setStaplefood(String staplefood) {
            this.staplefood = staplefood;
        }

        public String getCoefficiented() {
            return coefficiented;
        }

        public void setCoefficiented(String coefficiented) {
            this.coefficiented = coefficiented;
        }

        public String getSalesmanfee() {
            return salesmanfee;
        }

        public void setSalesmanfee(String salesmanfee) {
            this.salesmanfee = salesmanfee;
        }

        public String getCoefficienting() {
            return coefficienting;
        }

        public void setCoefficienting(String coefficienting) {
            this.coefficienting = coefficienting;
        }

        public String getCoefficientnext() {
            return coefficientnext;
        }

        public void setCoefficientnext(String coefficientnext) {
            this.coefficientnext = coefficientnext;
        }

        public String getStaplefoodnext() {
            return staplefoodnext;
        }

        public void setStaplefoodnext(String staplefoodnext) {
            this.staplefoodnext = staplefoodnext;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<DeductBean> getCity_info() {
        return city_info;
    }

    public void setCity_info(List<DeductBean> city_info) {
        this.city_info = city_info;
    }

    public String getShow_sale_data() {
        return show_sale_data;
    }

    public void setShow_sale_data(String show_sale_data) {
        this.show_sale_data = show_sale_data;
    }
}
