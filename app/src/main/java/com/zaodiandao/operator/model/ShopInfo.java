package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2016/11/28.
 * 门店信息实体类
 */
public class ShopInfo {
    private List<ShopBean> restaurants;
    private List<ShopDistribute.BrandBean> brands;
    private List<StatusBean> statuses;

    public List<ShopBean> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<ShopBean> restaurants) {
        this.restaurants = restaurants;
    }

    public List<ShopDistribute.BrandBean> getBrands() {
        return brands;
    }

    public void setBrands(List<ShopDistribute.BrandBean> brands) {
        this.brands = brands;
    }

    public List<StatusBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<StatusBean> statuses) {
        this.statuses = statuses;
    }

    public static class StatusBean {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class ShopBean {
        private int restaurant_id;      // 门店ID
        private String brandname;       // 品牌名称
        private String name;            // 门店名称
        private String shopname;        // 商户名称
        private int status;             // 门店状态

        public int getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(int restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getBrandname() {
            return brandname;
        }

        public void setBrandname(String brandname) {
            this.brandname = brandname;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShopname() {
            return shopname;
        }

        public void setShopname(String shopname) {
            this.shopname = shopname;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
