package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/2/23.
 */
public class ShopAuditModel {
    private String tag;         // 1=商户申请 2=商户加盟
    private String status;
    private List<StatusBean> statuss;
    private List<AuditBean> joininfo;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<StatusBean> getStatuss() {
        return statuss;
    }

    public void setStatuss(List<StatusBean> statuss) {
        this.statuss = statuss;
    }

    public List<AuditBean> getJoininfo() {
        return joininfo;
    }

    public void setJoininfo(List<AuditBean> joininfo) {
        this.joininfo = joininfo;
    }

    public static class StatusBean {
        private String status_id;
        private String status_label;

        public String getStatus_id() {
            return status_id;
        }

        public void setStatus_id(String status_id) {
            this.status_id = status_id;
        }

        public String getStatus_label() {
            return status_label;
        }

        public void setStatus_label(String status_label) {
            this.status_label = status_label;
        }
    }

    public static class AuditBean {
        private String shopapply_id;
        private String restaurant_id;
        private String brand_name;
        private String shop_name;
        private int status_id;
        private String status_label;
        private String boss_name;
        private String boss_mobile;
        private String boss_address;

        public String getShopapply_id() {
            return shopapply_id;
        }

        public void setShopapply_id(String shopapply_id) {
            this.shopapply_id = shopapply_id;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public int getStatus_id() {
            return status_id;
        }

        public void setStatus_id(int status_id) {
            this.status_id = status_id;
        }

        public String getStatus_label() {
            return status_label;
        }

        public void setStatus_label(String status_label) {
            this.status_label = status_label;
        }

        public String getBoss_name() {
            return boss_name;
        }

        public void setBoss_name(String boss_name) {
            this.boss_name = boss_name;
        }

        public String getBoss_mobile() {
            return boss_mobile;
        }

        public void setBoss_mobile(String boss_mobile) {
            this.boss_mobile = boss_mobile;
        }

        public String getBoss_address() {
            return boss_address;
        }

        public void setBoss_address(String boss_address) {
            this.boss_address = boss_address;
        }
    }
}
