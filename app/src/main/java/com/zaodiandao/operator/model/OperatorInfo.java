package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2016/11/25.
 * 业务员信息实体类
 */
public class OperatorInfo {
    private String name;        // 业务员姓名
    private String avatar;      // 业务员头像
    private String mobile;      // 业务员手机号
    private List<SaledataBean> saledata;    // 上月销售数据
    private List<MonthSaleData> currentmonthsaledata;   // 本月销售数据

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<SaledataBean> getSaledata() {
        return saledata;
    }

    public void setSaledata(List<SaledataBean> saledata) {
        this.saledata = saledata;
    }

    public List<MonthSaleData> getCurrentmonthsaledata() {
        return currentmonthsaledata;
    }

    public void setCurrentmonthsaledata(List<MonthSaleData> currentmonthsaledata) {
        this.currentmonthsaledata = currentmonthsaledata;
    }

    /**
     * 本月销售数据
     */
    public static class MonthSaleData {
        private String month;
        private String brand;
        private String cpayment;

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCpayment() {
            return cpayment;
        }

        public void setCpayment(String cpayment) {
            this.cpayment = cpayment;
        }
    }

    /**
     * 上月销售数据
     */
    public static class SaledataBean {
        private String date;        // 日期
        private String month;       // 月份
        private String brand;       // 品牌
        private String brand_id;       // 品牌ID
        private String salesamount; // 销售总额
        private String riseamount;  // 增长量
        private String payment;        // 提成

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getSalesamount() {
            return salesamount;
        }

        public void setSalesamount(String salesamount) {
            this.salesamount = salesamount;
        }

        public String getRiseamount() {
            return riseamount;
        }

        public void setRiseamount(String riseamount) {
            this.riseamount = riseamount;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }
    }
}
