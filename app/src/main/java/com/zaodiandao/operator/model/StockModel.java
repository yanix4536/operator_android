package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/2/22.
 */
public class StockModel {
    private String store_id;
    private List<StoreBean> store_list;
    private List<StockProduct> products;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public List<StoreBean> getStore_list() {
        return store_list;
    }

    public void setStore_list(List<StoreBean> store_list) {
        this.store_list = store_list;
    }

    public List<StockProduct> getProducts() {
        return products;
    }

    public void setProducts(List<StockProduct> products) {
        this.products = products;
    }

    public static class StoreBean {
        private String store_id;
        private String store_name;

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }
    }

    public static class StockProduct {
        private String product_name;
        private String format;
        private String sell_unit;
        private String sell_amount;
        private String hold_amount;

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getSell_unit() {
            return sell_unit;
        }

        public void setSell_unit(String sell_unit) {
            this.sell_unit = sell_unit;
        }

        public String getSell_amount() {
            return sell_amount;
        }

        public void setSell_amount(String sell_amount) {
            this.sell_amount = sell_amount;
        }

        public String getHold_amount() {
            return hold_amount;
        }

        public void setHold_amount(String hold_amount) {
            this.hold_amount = hold_amount;
        }
    }
}
