package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/2/23.
 * 审核详情
 */
public class AuditDetailModel {
    private String status_id;
    private String status_label;
    private String brand_name;
    private String shop_name;
    private String address;
    private String linkman;
    private String mobile;
    private String salesman;
    private List<String> imgs;
    private List<SaleMan> salesmans;

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getStatus_label() {
        return status_label;
    }

    public void setStatus_label(String status_label) {
        this.status_label = status_label;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public List<String> getImgs() {
        return imgs;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }

    public List<SaleMan> getSalesmans() {
        return salesmans;
    }

    public void setSalesmans(List<SaleMan> salesmans) {
        this.salesmans = salesmans;
    }

    public static class SaleMan{
        private int salesman_id;
        private String salesman_name;

        public int getSalesman_id() {
            return salesman_id;
        }

        public void setSalesman_id(int salesman_id) {
            this.salesman_id = salesman_id;
        }

        public String getSalesman_name() {
            return salesman_name;
        }

        public void setSalesman_name(String salesman_name) {
            this.salesman_name = salesman_name;
        }
    }
}
