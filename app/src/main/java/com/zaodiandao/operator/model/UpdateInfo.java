package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2016/12/6.
 * App更新信息
 */
public class UpdateInfo {
    private String update;      // 是否更新
    private String update_mark; // 更新说明
    private String update_is_mandatory;     // 是否强制更新
    private String downloadUrl;     // app下载地址
    private String authority;       // 权限级别(1=普通 24=城市经理)

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getUpdate_mark() {
        return update_mark;
    }

    public void setUpdate_mark(String update_mark) {
        this.update_mark = update_mark;
    }

    public String getUpdate_is_mandatory() {
        return update_is_mandatory;
    }

    public void setUpdate_is_mandatory(String update_is_mandatory) {
        this.update_is_mandatory = update_is_mandatory;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
