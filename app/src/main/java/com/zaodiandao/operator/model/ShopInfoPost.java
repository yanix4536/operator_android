package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2016/11/30.
 * 保存商户信息的参数类
 */
public class ShopInfoPost {
    private int cityId;     // 城市ID
    private int country;    // 区县ID
    private String name;    // 门店名
    private String address; // 门店地址
    private String linkman;         // 联系人
    private String mobile;          // 联系电话
    private double longitude;       // 地图经度
    private double latitude;        // 地图纬度

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

}
