package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/1/11.
 * 历史订单实体类
 */
public class OrderListBean {
    private String title;
    private List<OrderOperatorBean> orders;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<OrderOperatorBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderOperatorBean> orders) {
        this.orders = orders;
    }

    public static class OrderOperatorBean {
        private String order_id;
        private String shop_name;
        private String ordernumber;
        private int status;     // 5=未支付 10=未审核 20=已审核 50=已配送 60=已完成 80=已取消
        private String delivertime;
        private String money;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getOrdernumber() {
            return ordernumber;
        }

        public void setOrdernumber(String ordernumber) {
            this.ordernumber = ordernumber;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getDelivertime() {
            return delivertime;
        }

        public void setDelivertime(String delivertime) {
            this.delivertime = delivertime;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

    }
}
