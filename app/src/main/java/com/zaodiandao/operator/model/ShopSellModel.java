package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/3/31.
 * 商户销售详情
 */
public class ShopSellModel {
    private String city_id;
    private List<City> citys;
    private String brand_id;
    private List<Brand> brands;
    private String month;
    private List<String> months;
    private String total_shopnum;
    private String total_ordernum;
    private String total_money;
    private List<SellBean> sells;

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public List<City> getCitys() {
        return citys;
    }

    public void setCitys(List<City> citys) {
        this.citys = citys;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<String> getMonths() {
        return months;
    }

    public void setMonths(List<String> months) {
        this.months = months;
    }

    public String getTotal_shopnum() {
        return total_shopnum;
    }

    public void setTotal_shopnum(String total_shopnum) {
        this.total_shopnum = total_shopnum;
    }

    public String getTotal_ordernum() {
        return total_ordernum;
    }

    public void setTotal_ordernum(String total_ordernum) {
        this.total_ordernum = total_ordernum;
    }

    public String getTotal_money() {
        return total_money;
    }

    public void setTotal_money(String total_money) {
        this.total_money = total_money;
    }

    public List<SellBean> getSells() {
        return sells;
    }

    public void setSells(List<SellBean> sells) {
        this.sells = sells;
    }

    public static class City {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Brand {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class SellBean {
        private String brand_name;
        private String shop_name;
        private String nums;
        private String food_money;

        public SellBean() {

        }

        public SellBean(String brand_name, String shop_name, String nums, String food_money) {
            this.brand_name = brand_name;
            this.shop_name = shop_name;
            this.nums = nums;
            this.food_money = food_money;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getNums() {
            return nums;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getFood_money() {
            return food_money;
        }

        public void setFood_money(String food_money) {
            this.food_money = food_money;
        }
    }
}
