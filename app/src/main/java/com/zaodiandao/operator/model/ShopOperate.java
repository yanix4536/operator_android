package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2017/3/21.
 * 商户经营实体
 */
public class ShopOperate {
    private String order_id;
    private String order_number;
    private int order_status_id;
    private String order_status_label;
    private String order_created;
    private String order_summoney;
    private String order_salesman_money;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public int getOrder_status_id() {
        return order_status_id;
    }

    public void setOrder_status_id(int order_status_id) {
        this.order_status_id = order_status_id;
    }

    public String getOrder_status_label() {
        return order_status_label;
    }

    public void setOrder_status_label(String order_status_label) {
        this.order_status_label = order_status_label;
    }

    public String getOrder_created() {
        return order_created;
    }

    public void setOrder_created(String order_created) {
        this.order_created = order_created;
    }

    public String getOrder_summoney() {
        return order_summoney;
    }

    public void setOrder_summoney(String order_summoney) {
        this.order_summoney = order_summoney;
    }

    public String getOrder_salesman_money() {
        return order_salesman_money;
    }

    public void setOrder_salesman_money(String order_salesman_money) {
        this.order_salesman_money = order_salesman_money;
    }
}
