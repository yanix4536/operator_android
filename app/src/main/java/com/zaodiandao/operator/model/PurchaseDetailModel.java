package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/2/23.
 */
public class PurchaseDetailModel {
    private String estimate_id;
    private List<PurchaseProductBean> products;

    public String getEstimate_id() {
        return estimate_id;
    }

    public void setEstimate_id(String estimate_id) {
        this.estimate_id = estimate_id;
    }

    public List<PurchaseProductBean> getProducts() {
        return products;
    }

    public void setProducts(List<PurchaseProductBean> products) {
        this.products = products;
    }

    public static class PurchaseProductBean {
        private String pid;
        private String name;
        private String format;
        private String amount;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
}
