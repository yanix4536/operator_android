package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2016/12/1.
 * 商户证件照是实体类
 */
public class ShopPhoto {
    private String poster;

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }
}
