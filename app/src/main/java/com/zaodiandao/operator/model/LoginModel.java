package com.zaodiandao.operator.model;

/**
 * Created by yanix on 2017/2/22.
 */
public class LoginModel {
    private int clerk_id;
    private String authority;

    public int getClerk_id() {
        return clerk_id;
    }

    public void setClerk_id(int clerk_id) {
        this.clerk_id = clerk_id;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}

