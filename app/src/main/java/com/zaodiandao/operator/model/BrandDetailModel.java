package com.zaodiandao.operator.model;

import java.util.List;

/**
 * Created by yanix on 2017/1/17.
 */
public class BrandDetailModel {
    private String brand_name;
    private String description;
    private List<ImageBean> img;

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ImageBean> getImg() {
        return img;
    }

    public void setImg(List<ImageBean> img) {
        this.img = img;
    }

    public static class ImageBean {
        private String ri_id;
        private String img_url = "";
        private String img_mark;

        public ImageBean() {}

        public ImageBean(String imgurl) {
            this.img_url = imgurl;
        }

        public String getRi_id() {
            return ri_id;
        }

        public void setRi_id(String ri_id) {
            this.ri_id = ri_id;
        }

        public String getImg_url() {
            return img_url;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }

        public String getImg_mark() {
            return img_mark;
        }

        public void setImg_mark(String img_mark) {
            this.img_mark = img_mark;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (obj instanceof ImageBean) {
                ImageBean other = (ImageBean) obj;
                return img_url.equals(other.getImg_url());
            }
            return super.equals(obj);
        }
    }
}
