package com.zaodiandao.operator.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2017/1/9.
 * 产品类别
 */
public class ProductClassBean implements Serializable{
    private String estimate_id;
    private String menutype;
    private List<ProductBean> products;

    public String getEstimate_id() {
        return estimate_id;
    }

    public void setEstimate_id(String estimate_id) {
        this.estimate_id = estimate_id;
    }

    public String getMenutype() {
        return menutype;
    }

    public void setMenutype(String menutype) {
        this.menutype = menutype;
    }

    public List<ProductBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductBean> products) {
        this.products = products;
    }

    public static class ProductBean implements Serializable{
        private String pid;     // 商品ID
        private String name;    // 商品名
        private String format;  // 规格
        private String unit;     // 单位
        private String image;           // 产品图片
        private String detail;          // 产品描述
        private int status = 1;         // 1-销售  2-缺货
        private int amount;
        private int position;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}
