package com.zaodiandao.operator.model;

public class ClassItem {
    private String className;       // 类别名称
    private int number;             // 该类别下点菜的数量

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getNumber() {
        return number < 0 ? 0 : number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}