package com.zaodiandao.operator.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yanix on 2016/11/30.
 * 商户信息实体类
 */
public class ShopDetail implements Serializable {
    private ShopBean shop;          // 门店信息
    private int city_id;            // 当前城市ID
    private int country_id;         // 当前区域ID
    private int grade_id;           // 当前门店等级ID
    private int implantation_box_id;    // 是否有冰箱ID
    private List<CitycountrysBean> citycountrys;
    private List<GradesBean> grades;
    private List<ImplantationBoxsBean> implantation_boxs;
    private List<ShopDistribute.BrandBean> brands;

    public ShopBean getShop() {
        return shop;
    }

    public void setShop(ShopBean shop) {
        this.shop = shop;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getGrade_id() {
        return grade_id;
    }

    public void setGrade_id(int grade_id) {
        this.grade_id = grade_id;
    }

    public int getImplantation_box_id() {
        return implantation_box_id;
    }

    public void setImplantation_box_id(int implantation_box_id) {
        this.implantation_box_id = implantation_box_id;
    }

    public List<CitycountrysBean> getCitycountrys() {
        return citycountrys;
    }

    public void setCitycountrys(List<CitycountrysBean> citycountrys) {
        this.citycountrys = citycountrys;
    }

    public List<GradesBean> getGrades() {
        return grades;
    }

    public void setGrades(List<GradesBean> grades) {
        this.grades = grades;
    }

    public List<ImplantationBoxsBean> getImplantation_boxs() {
        return implantation_boxs;
    }

    public void setImplantation_boxs(List<ImplantationBoxsBean> implantation_boxs) {
        this.implantation_boxs = implantation_boxs;
    }

    public List<ShopDistribute.BrandBean> getBrands() {
        return brands;
    }

    public void setBrands(List<ShopDistribute.BrandBean> brands) {
        this.brands = brands;
    }

    public static class ShopBean implements Serializable{
        private int shop_id;        // 商户ID
        private String city;        // 商户所在城市
        private String country;     // 商户所在区域
        private String grade;       // 门店等级
        private String name;        // 门店名
        private String address;     // 门店地址
        private String accountname; // 早点到掌柜账号
        private String linkman;     // 联系人
        private String mobile;      // 联系电话
        private String implantation_box;    // 是否有冰箱
        private double longitude;   // 经度
        private double latitude;    // 纬度
        private String bank_card;   // 银行卡号
        private String bank_account;    // 开户人
        private String bank_name;       // 开户行
        private String bank_namebranch; // 开户行支行

        public int getShop_id() {
            return shop_id;
        }

        public void setShop_id(int shop_id) {
            this.shop_id = shop_id;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAccountname() {
            return accountname;
        }

        public void setAccountname(String accountname) {
            this.accountname = accountname;
        }

        public String getLinkman() {
            return linkman;
        }

        public void setLinkman(String linkman) {
            this.linkman = linkman;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getImplantation_box() {
            return implantation_box;
        }

        public void setImplantation_box(String implantation_box) {
            this.implantation_box = implantation_box;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getBank_card() {
            return bank_card;
        }

        public void setBank_card(String bank_card) {
            this.bank_card = bank_card;
        }

        public String getBank_account() {
            return bank_account;
        }

        public void setBank_account(String bank_account) {
            this.bank_account = bank_account;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getBank_namebranch() {
            return bank_namebranch;
        }

        public void setBank_namebranch(String bank_namebranch) {
            this.bank_namebranch = bank_namebranch;
        }
    }

    public static class CitycountrysBean implements Serializable{
        private int id;         // 城市ID
        private String name;    // 城市名
        private List<CountryBean> country;      // 城市所包含的区域

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<CountryBean> getCountry() {
            return country;
        }

        public void setCountry(List<CountryBean> country) {
            this.country = country;
        }

        public static class CountryBean implements Serializable{
            private int id;             // 区域ID
            private String name;        // 区域名

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

    public static class GradesBean implements Serializable{
        private int id;         // 门店等级ID
        private String name;    // 门店等级名

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class ImplantationBoxsBean implements Serializable{
        private int id;         // 是否有冰箱ID
        private String name;    // 是否有冰箱

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
