package com.zaodiandao.operator.shop.apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.socks.library.KLog;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.model.ShopDetail;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.shop.manage.AddShopActivity;
import com.zaodiandao.operator.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/30.
 * 门店详细信息
 */
public class ShopDetailInfoActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    public static final String KEY_SHOP_DETAIL = "key_shop_detail";
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.tv_shop_city)
    TextView tvShopCity;
    @BindView(R.id.tv_shop_type)
    TextView tvShopType;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_shop_address)
    TextView tvShopAddress;
    @BindView(R.id.tv_linkman)
    TextView tvLinkMan;
    @BindView(R.id.tv_link_mobile)
    TextView tvLinkMobile;

    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.mapView)
    TextureMapView mMapView;
    private BaiduMap map;
    private Marker mMarker;

    private String mShopId;
    private ShopDetail mShopDetail;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_shop_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mShopId = getIntent().getStringExtra(ShopStatusFragment.KEY_SHOP_ID);

        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        map = mMapView.getMap();
        map.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        //解决地图的touch事件和scrollView的touch事件冲突问题
        mMapView.getChildAt(0).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mMapView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mMapView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        getData();
    }

    /**
     * 获取数据
     */
    private void getData() {
        mApiService.shopDetail(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mShopId,
                new GenericCallback<ShopDetail>(getApplicationContext(), ShopDetail.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(ShopDetail data) {
                        mShopDetail = data;
                        ShopDetail.ShopBean shopBean = data.getShop();
                        setTitle(shopBean.getName());
                        tvShopCity.setText(shopBean.getCity() + " " + shopBean.getCountry());
                        tvShopType.setText(shopBean.getGrade());
                        tvShopName.setText(shopBean.getName());
                        tvShopAddress.setText(shopBean.getAddress());
                        tvLinkMan.setText(shopBean.getLinkman());
                        tvLinkMobile.setText(shopBean.getMobile());

                        if (mMarker != null)
                            mMarker.remove();

                        LatLng point = new LatLng(shopBean.getLatitude(), shopBean.getLongitude());
                        BitmapDescriptor bitmapCooperated = BitmapDescriptorFactory.fromResource(R.mipmap.map_cooperated);
                        MarkerOptions option = new MarkerOptions()
                                .position(point)
                                .icon(bitmapCooperated)
                                .draggable(false);
                        option.animateType(MarkerOptions.MarkerAnimateType.none);
                        mMarker = (Marker) map.addOverlay(option);

                        LatLng ll = new LatLng(shopBean.getLatitude(), shopBean.getLongitude());
                        MapStatus.Builder builder = new MapStatus.Builder();
                        builder.target(ll).zoom(17.0f);
                        map.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));

                        mLayoutContent.setVisibility(View.VISIBLE);
                        // invalidateOptionsMenu();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    /*@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.change);
        if (mLayoutContent.getVisibility() == View.VISIBLE) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_shop_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.change:
                Intent intent = new Intent(this, UpdateShopDetailActivity.class);
                intent.putExtra(UpdateShopDetailActivity.KEY_ACTION_SHOP, UpdateShopDetailActivity.ACTION_UPDATE);
                intent.putExtra(KEY_SHOP_DETAIL, mShopDetail);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        KLog.d(event.code);
        if (event.code == EventCode.CHANGE_SHOP_INFO)
            getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        getData();
    }

    @OnClick(R.id.btn_open_shop)
    public void openShop(View view) {
        Intent intent = new Intent(this, AddShopActivity.class);
        intent.putExtra(KEY_SHOP_DETAIL, mShopDetail);
        startActivity(intent);
    }
}
