package com.zaodiandao.operator.shop.manage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yangx on 2015/12/28.
 */
public class OrderManagerFragment extends BaseFragment {
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @BindView(R.id.cursor)
    View cursor;
    @BindView(R.id.rb_today)
    RadioButton rb_today;
    @BindView(R.id.rb_two_month)
    RadioButton rb_two_month;
    private List<Fragment> fragmentList;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private static final int TAB_COUNT = 2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_order_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search:
                startActivity(new Intent(getActivity(), SearchOrderActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View getView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_order_manager, null);
        ButterKnife.bind(this, root);
        initCursor();
        initTab();
        initViewPager();
        return root;
    }

    @Override
    public void getData() {

    }

    private void initTab() {
        rb_today.setOnClickListener(new MyClickListener(0));
        rb_two_month.setOnClickListener(new MyClickListener(1));
        rb_today.setChecked(true);
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        OrderTodayFragment todayFragment = new OrderTodayFragment();
        OrderMonthFragment twoMonthFragment = new OrderMonthFragment();
        fragmentList.add(todayFragment);
        fragmentList.add(twoMonthFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new MyFragmentPagerAdapter(getChildFragmentManager()));
        mPager.setOffscreenPageLimit(1);
        mPager.setCurrentItem(0);//设置当前显示标签页为第一页
        mPager.addOnPageChangeListener(new MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        /**
         * @param position             当前页面，及你点击滑动的页面
         * @param positionOffset       当前页面偏移的百分比
         * @param positionOffsetPixels 当前页面偏移的像素位置
         */
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        /**
         * 进度条移动 :核心的移动算法
         *
         * @param position             当前页位置
         * @param positionOffsetPixels 移动像素值
         */
        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rb_today.setChecked(true);
            } else if (position == 1) {
                rb_two_month.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
