package com.zaodiandao.operator.shop.apply;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.OrderListBean;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.order.OrderOperatorListAdapter;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.commonadapter.CommonAdapter;
import com.zaodiandao.operator.util.commonadapter.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;

import static com.zaodiandao.operator.R.id.listView;

/**
 * Created by yanix on 2016/11/22.
 * 门店管理界面
 */
public class ShopCheckActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mllContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @BindView(R.id.refresh_empty)
    SwipeRefreshLayout emptyRefresh;

    @BindView(listView)
    ListView mListView;

    // private OrderListAdapter mAdapter;
    private CommonAdapter<OrderListBean> mAdapter;;
    private OrderOperatorListAdapter adapter;
    private List<OrderListBean> datas = new ArrayList<>();

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_shop_check);
        mllContent.setColorSchemeResources(R.color.colorPrimary);
        mllContent.setOnRefreshListener(this);
        emptyRefresh.setColorSchemeResources(R.color.colorPrimary);
        emptyRefresh.setOnRefreshListener(this);

        mAdapter = new CommonAdapter<OrderListBean>(this, datas, R.layout.listitem_order_list) {
            @Override
            public void convert(final ViewHolder helper, final OrderListBean item) {

            }
        };
        mListView.setAdapter(mAdapter);
        fetchData();
    }


    public void fetchData() {
        // 获取数据
        mApiService.getOrderList(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), "1",
                new GenericArrayCallback<OrderListBean>(getApplicationContext(), OrderListBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mllContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<OrderListBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        if (data == null || data.size() <= 0) {
                            emptyRefresh.setVisibility(View.VISIBLE);
                            mllContent.setVisibility(View.GONE);
                        } else {
                            emptyRefresh.setVisibility(View.GONE);
                            mllContent.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mllContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mllContent.isRefreshing()) {
                            mllContent.setRefreshing(false);
                        }
                        if (emptyRefresh.isRefreshing()) {
                            emptyRefresh.setRefreshing(false);
                        }
                    }
                });
    }

    private void refresh() {
        // 获取数据
        mApiService.getOrderList(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), "1",
                new GenericArrayCallback<OrderListBean>(getApplicationContext(), OrderListBean.class) {

                    @Override
                    public void success(List<OrderListBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        if (data == null || data.size() <= 0) {
                            emptyRefresh.setVisibility(View.VISIBLE);
                            mllContent.setVisibility(View.GONE);
                        } else {
                            emptyRefresh.setVisibility(View.GONE);
                            mllContent.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mllContent.isRefreshing()) {
                            mllContent.setRefreshing(false);
                        }
                        if (emptyRefresh.isRefreshing()) {
                            emptyRefresh.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        fetchData();
    }

    @Override
    public void onRefresh() {
        refresh();
    }
}
