package com.zaodiandao.operator.shop.apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yanix on 2016/11/22.
 * 商户开店界面
 */
public class ShopApplyFragment extends BaseFragment {
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @BindView(R.id.cursor)
    View cursor;
    @BindView(R.id.rb_opened)
    RadioButton rb_opened;
    @BindView(R.id.rb_open_check)
    RadioButton rb_open_check;
    @BindView(R.id.rb_brand_check)
    RadioButton rb_brand_check;
    private List<Fragment> fragmentList;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private static final int TAB_COUNT = 3;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View getView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_shop_status, null);
        ButterKnife.bind(this, root);
        initCursor();
        initTab();
        initViewPager();
        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.check);
        if ("24".equals(SPUtils.get(getActivity().getApplicationContext(), KeyConstants.LEVEL, ""))) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_check, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.check:
                startActivity(new Intent(getActivity(), ShopAuditActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void getData() {

    }

    private void initTab() {
        rb_opened.setOnClickListener(new MyClickListener(0));
        rb_open_check.setOnClickListener(new MyClickListener(1));
        rb_brand_check.setOnClickListener(new MyClickListener(2));
        rb_opened.setChecked(true);
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        ShopStatusFragment openFragment = new ShopStatusFragment();
        Bundle bundleOpen= new Bundle();
        bundleOpen.putString("type", "1");
        openFragment.setArguments(bundleOpen);

        ShopStatus2Fragment openCheckFragment = new ShopStatus2Fragment();
        Bundle bundleOpenCheck= new Bundle();
        bundleOpenCheck.putString("type", "1");
        openCheckFragment.setArguments(bundleOpenCheck);

        ShopStatus2Fragment brandCheckFragment = new ShopStatus2Fragment();
        Bundle bundleBrandCheck= new Bundle();
        bundleBrandCheck.putString("type", "2");
        brandCheckFragment.setArguments(bundleBrandCheck);

        fragmentList.add(openFragment);
        fragmentList.add(openCheckFragment);
        fragmentList.add(brandCheckFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new MyFragmentPagerAdapter(getChildFragmentManager()));
        mPager.setOffscreenPageLimit(2);
        mPager.setCurrentItem(0);//设置当前显示标签页为第一页
        mPager.addOnPageChangeListener(new MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        /**
         * @param position             当前页面，及你点击滑动的页面
         * @param positionOffset       当前页面偏移的百分比
         * @param positionOffsetPixels 当前页面偏移的像素位置
         */
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        /**
         * 进度条移动 :核心的移动算法
         *
         * @param position             当前页位置
         * @param positionOffsetPixels 移动像素值
         */
        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rb_opened.setChecked(true);
            } else if (position == 1) {
                rb_open_check.setChecked(true);
            } else if (position == 2) {
                rb_brand_check.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
