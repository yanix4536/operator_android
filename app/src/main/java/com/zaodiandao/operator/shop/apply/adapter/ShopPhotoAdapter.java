package com.zaodiandao.operator.shop.apply.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ShopPhoto;
import com.zaodiandao.operator.util.DensityUtils;

import java.util.List;

import static com.squareup.picasso.MemoryPolicy.NO_CACHE;
import static com.squareup.picasso.MemoryPolicy.NO_STORE;

public class ShopPhotoAdapter extends RecyclerView.Adapter<ShopPhotoAdapter.MyViewHolder> implements View.OnClickListener {
    private Context mContext;
    private List<ShopPhoto> mShopApplies;

    public ShopPhotoAdapter(Context context, List<ShopPhoto> shopApplies) {
        mContext = context;
        mShopApplies = shopApplies;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop_photo, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, ShopPhoto data);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ShopPhoto data = mShopApplies.get(position);
        holder.itemView.setTag(data);
        holder.tvPhotoDesc.setText("海报");
        if (!TextUtils.isEmpty(data.getPoster())) {
            Picasso.with(mContext).load(data.getPoster())
                    .resize(DensityUtils.dp2px(mContext, 110), DensityUtils.dp2px(mContext, 100))
                    .memoryPolicy(NO_CACHE, NO_STORE)
                    .placeholder(R.mipmap.avatar_default)
                    .config(Bitmap.Config.RGB_565)
                    .into(holder.ivPhoto);
        } else {
            holder.ivPhoto.setImageResource(R.mipmap.add_photo);
        }
    }

    @Override
    public int getItemCount() {
        return mShopApplies == null ? 0 : mShopApplies.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ShopPhoto) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPhotoDesc;
        ImageView ivPhoto;

        public MyViewHolder(View view) {
            super(view);
            tvPhotoDesc = (TextView) view.findViewById(R.id.tv_photo_desc);
            ivPhoto = (ImageView) view.findViewById(R.id.iv_photo);
        }
    }
}