package com.zaodiandao.operator.shop.manage.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ReserveOrder;
import com.zaodiandao.operator.model.ShopInfo;

import java.util.List;

public class ReserveOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<ReserveOrder> mReserveOrders;

    public ReserveOrderAdapter(Context context, List<ReserveOrder> reserveOrders) {
        mContext = context;
        mReserveOrders = reserveOrders;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new ReserveOrderAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_reserve_order, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, ReserveOrder data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            ReserveOrderAdapter.EmptyViewViewHolder emptyViewViewHolder = (ReserveOrderAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无订货单");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            ReserveOrder data = mReserveOrders.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvOrderNumber.setText(data.getOrdernumber());
            myViewHolder.tvDeliverTime.setText(data.getDelivertime());
            myViewHolder.tvOrderAmount.setText("￥" + data.getSummoney());
        }
    }

    @Override
    public int getItemCount() {
        return (mReserveOrders != null && mReserveOrders.size()>0) ? mReserveOrders.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mReserveOrders == null || mReserveOrders.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ReserveOrder) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOrderNumber, tvDeliverTime, tvOrderAmount;

        public MyViewHolder(View view) {
            super(view);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvDeliverTime = (TextView) view.findViewById(R.id.tv_deliver_time);
            tvOrderAmount = (TextView) view.findViewById(R.id.tv_order_amount);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}