package com.zaodiandao.operator.shop.apply.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ShopOperate;

import java.util.List;

public class ShopOperatorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<ShopOperate> mDatas;

    public ShopOperatorAdapter(Context context, List<ShopOperate> datas) {
        mContext = context;
        mDatas = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop_operate, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, ShopOperate data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            EmptyViewViewHolder emptyViewViewHolder = (EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无数据");
        } else {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            ShopOperate data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvStatus.setText(data.getOrder_status_label());
            int status = data.getOrder_status_id();
            if (status == 5) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#F3A536"));
            } else if (status == 10) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (status == 20) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (status == 50) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (status == 60) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#DA5623"));
            } else if (status == 80) {
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#13A800"));
            }

            myViewHolder.tvOrderNumber.setText(data.getOrder_number());
            myViewHolder.tvOrderTime.setText(data.getOrder_created());
            myViewHolder.tvOrderMoney.setText("￥" + data.getOrder_summoney());
            myViewHolder.tvFoodMoney.setText("￥" + data.getOrder_salesman_money());
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ShopOperate) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOrderNumber, tvStatus, tvOrderTime, tvOrderMoney, tvFoodMoney;

        public MyViewHolder(View view) {
            super(view);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvOrderTime = (TextView) view.findViewById(R.id.tv_order_time);
            tvOrderMoney = (TextView) view.findViewById(R.id.tv_order_money);
            tvFoodMoney = (TextView) view.findViewById(R.id.tv_food_money);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}