package com.zaodiandao.operator.shop.manage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.config.URLConstants;
import com.zaodiandao.operator.model.ShopView;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.shop.apply.ShopPhotoDetailActivity;
import com.zaodiandao.operator.shop.brand.BrandActivity;
import com.zaodiandao.operator.shop.manage.adapter.ShopImageAdapter;
import com.zaodiandao.operator.util.DensityUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.PictureUtil;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;
import com.zaodiandao.operator.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yanix on 2016/11/28.
 * 门店查看/修改界面
 */
public class ShopViewActivity extends BaseActivity {
    private static final int REQUEST_CODE_GALLERY = 100;
    // 查看模式：修改和查看
    private static final int STATUS_CHANGE = 1;
    private static final int STATUS_VIEW = 2;
    // 当前模式：修改
    private int status = STATUS_CHANGE;

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_station_name)
    TextView mTvStationName;
    @BindView(R.id.tv_brand_name)
    TextView mTvBrandName;

    @BindView(R.id.btn_apply)
    Button mBtnApply;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;

    // 门店ID
    private String mRestaurantId;
    private String shopName;
    // 商户ID
    private String mShopId;
    // 当前显示的图片
    private List<ShopView.ImageBean> datas = new ArrayList<>();
    // 门店原有的图片
    private List<ShopView.ImageBean> originDatas = new ArrayList<>();
    // 从本地添加的图片
    private List<ShopView.ImageBean> localDatas = new ArrayList<>();
    // 添加图片
    private ShopView.ImageBean mImageBean = new ShopView.ImageBean();
    private ShopImageAdapter adapter;
    // 保存要删除的图片url
    private JSONArray jsonArray = new JSONArray();
    // 保存临时文件名
    private List<String> tempNames = new ArrayList<>();
    // 保存要上传的文件
    private List<File> uploadFiles = new ArrayList<>();
    // 最多能添加的图片数量
    private int maxSize;
    // 门店之前拥有的图片数量
    private int networkImageCount;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_shop_view);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        // 获取门店ID
        mRestaurantId = getIntent().getStringExtra(BrandActivity.KEY_RESTAURANT_ID);
        getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    /**
     * 获取数据
     */
    private void getData() {
        mApiService.shopView(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mRestaurantId,
                new GenericCallback<ShopView>(getApplicationContext(), ShopView.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(ShopView data) {
                        datas.clear();
                        // 商户ID
                        mShopId = data.getShop_id();
                        mTvStationName.setText(data.getShopname());
                        mTvBrandName.setText(data.getBrandname());
                        shopName = data.getShopname();
                        setTitle(data.getName());
                        // 门店原始照片
                        originDatas.addAll(data.getImg());
                        datas.addAll(data.getImg());
                        // 门店原始照片数量
                        networkImageCount = datas.size();
                        adapter = new ShopImageAdapter(getApplicationContext(), datas);
                        mRecyclerView.setAdapter(adapter);
                        // 照片点击事件
                        adapter.setOnItemClickListener(new ShopImageAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(final View view, final ShopView.ImageBean data) {
                                // 判断图片Url是否为空，为空说明是添加图片，只有在修改模式下才会显示
                                if (TextUtils.isEmpty(data.getImgurl())) {
                                    MenuSheetView menuSheetView =
                                            new MenuSheetView(ShopViewActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                                                @Override
                                                public boolean onMenuItemClick(MenuItem item) {
                                                    switch (item.getItemId()) {
                                                        case R.id.take_photo:
                                                            // 拍照
                                                            takePhoto();
                                                            break;
                                                        case R.id.take_from_album:
                                                            // 从相册选择
                                                            takeFromAlbum();
                                                            break;
                                                    }
                                                    if (mSheetLayout.isSheetShowing()) {
                                                        mSheetLayout.dismissSheet();
                                                    }
                                                    return true;
                                                }
                                            });
                                    menuSheetView.inflateMenu(R.menu.menu_choose_image);
                                    mSheetLayout.showWithSheetView(menuSheetView);
                                } else {
                                    // 图片URL地址不为空，那么就是编辑这张图片，只有在修改模式下才能编辑
                                    if (status == STATUS_VIEW) {
                                        MenuSheetView menuSheetView =
                                                new MenuSheetView(ShopViewActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                                                    @Override
                                                    public boolean onMenuItemClick(MenuItem item) {
                                                        if (mSheetLayout.isSheetShowing()) {
                                                            mSheetLayout.dismissSheet();
                                                        }
                                                        switch (item.getItemId()) {
                                                            case R.id.big_image:
                                                                // 查看大图
                                                                open(((RelativeLayout) view).getChildAt(0), data.getImgurl());
                                                                break;
                                                            case R.id.delete:
                                                                // 删除图片，如果是本地添加的图片，直接从本地图片集合删除
                                                                // 如果是原始图片，则需要把这个图片id从保存的id集合中移除，同时将原始的图片数量减一
                                                                if (!data.getImgurl().startsWith("/storage")) {
                                                                    jsonArray.add(data.getImg_id());
                                                                    networkImageCount--;
                                                                } else {
                                                                    localDatas.remove(data);
                                                                }
                                                                datas.remove(data);
                                                                // 判断是否添加+号图片，图片如果已经存在，就不再重复添加
                                                                if (!datas.contains(mImageBean))
                                                                    datas.add(mImageBean);
                                                                adapter.notifyDataSetChanged();
                                                                break;
                                                        }
                                                        return true;
                                                    }
                                                });
                                        menuSheetView.inflateMenu(R.menu.menu_delete_image);
                                        mSheetLayout.showWithSheetView(menuSheetView);
                                    } else {
                                        // 如果是查看模式，只能查看大图，不能编辑
                                        open(((RelativeLayout) view).getChildAt(0), data.getImgurl());
                                    }
                                }
                            }
                        });
                        mLayoutContent.setVisibility(View.VISIBLE);
                        // 更新一个菜单栏
                        invalidateOptionsMenu();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    /**
     * 图片放大
     *
     * @param view
     * @param url
     */
    private void open(View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), ShopPhotoDetailActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            ShopViewActivity.this.overridePendingTransition(0, 0);
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.change);
        if (mLayoutContent.getVisibility() == View.VISIBLE) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_shop_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // 如果bottom sheet正在显示，则隐藏，其他什么都不做
        if (mSheetLayout.isSheetShowing()) {
            mSheetLayout.dismissSheet();
            return super.onOptionsItemSelected(item);
        }

        switch (item.getItemId()) {
            case R.id.change:
                if (status == STATUS_CHANGE) {
                    status = STATUS_VIEW;
                    item.setTitle("取消");
                    // 判断是都需要添加+图片
                    if (datas.size() < 6) {
                        datas.add(mImageBean);
                    }
                    adapter.notifyDataSetChanged();
                    mBtnApply.setVisibility(View.VISIBLE);
                } else {
                    status = STATUS_CHANGE;
                    item.setTitle("修改");
                    // 恢复原始数据
                    datas.clear();
                    datas.addAll(originDatas);
                    networkImageCount = datas.size();
                    localDatas.clear();
                    adapter.notifyDataSetChanged();
                    mBtnApply.setVisibility(View.INVISIBLE);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * 从相册选择
     */
    private void takeFromAlbum() {
        ArrayList<String> selectedImages = new ArrayList<>();
        // 计算已经选择的本地图片
        for (ShopView.ImageBean imageBean : localDatas) {
            selectedImages.add(imageBean.getImgurl());
        }

        //带配置
        FunctionConfig config = new FunctionConfig.Builder()
                .setMutiSelectMaxSize(6 - networkImageCount)    // 设置还能选中的图片数量
                .setSelected(selectedImages)  // 设置选中的图片
                .build();

        GalleryFinal.openGalleryMuti(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                updateGridView(resultList);
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 拍照
     */
    private void takePhoto() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                datas.remove(mImageBean);
                datas.removeAll(localDatas);
                ShopView.ImageBean imageBean = new ShopView.ImageBean(resultList.get(0).getPhotoPath());
                localDatas.add(imageBean);

                datas.addAll(localDatas);
                maxSize = 6 - datas.size();
                if (maxSize > 0) {
                    datas.add(mImageBean);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 更新图片列表
     *
     * @param resultList 本地图片集合
     */
    private void updateGridView(List<PhotoInfo> resultList) {
        datas.remove(mImageBean);
        datas.removeAll(localDatas);
        localDatas.clear();
        for (PhotoInfo photoInfo : resultList) {
            ShopView.ImageBean imageBean = new ShopView.ImageBean(photoInfo.getPhotoPath());
            localDatas.add(imageBean);
        }
        datas.addAll(localDatas);
        maxSize = 6 - datas.size();
        if (maxSize > 0) {
            datas.add(mImageBean);
        }
        adapter.notifyDataSetChanged();
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //开启裁剪功能
                .setEnableRotate(false)     //开启旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 100))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }

    @OnClick(R.id.btn_apply)
    public void submit(View view) {
        DialogHelper.showDimDialog(ShopViewActivity.this, "正在上传...");

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                // 添加待上传文件
                for (ShopView.ImageBean imageBean : localDatas) {
                    try {
                        String tempName = PictureUtil.bitmapToPath(imageBean.getImgurl());
                        tempNames.add(tempName);
                        uploadFiles.add(new File(tempName));
                    } catch (final IOException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogHelper.dismissDialog();
                                ToastUtils.showMessage(getApplicationContext(), e.toString());
                            }
                        });
                        return;
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        uploadImage();
                    }
                });
            }
        });
    }

    /**
     * 修改门店信息
     */
    private void uploadImage() {
        /* form的分割线,自己定义 */
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody.Builder builder = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                /* 上传一个普通的String参数 */
                .addFormDataPart("version", URLConstants.API_VERSION)
                .addFormDataPart("clerk_id", (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1) + "")
                .addFormDataPart("shop_id", mShopId)
                .addFormDataPart("name", shopName)
                .addFormDataPart("restaurant_id", mRestaurantId)
                .addFormDataPart("img_id_arr", jsonArray.toJSONString());

        for (int i = 0; i < uploadFiles.size(); i++) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), uploadFiles.get(i));
            String fileName = uploadFiles.get(i).getName();
            builder = builder.addFormDataPart("file[]", fileName, fileBody);
        }
        MultipartBody mBody = builder.build();

        Request request = new Request.Builder().url(URLConstants.URL_UPLOAD_SHOP_PHOTO).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            public void onResponse(Call call, Response response) throws IOException {
                String bodyStr = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(bodyStr);
                    final String error = jsonObject.getString("error");
                    int code = jsonObject.getInt("code");
                    final boolean ok = code == 200;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            DialogHelper.dismissDialog();
                            if (ok) {
                                PictureUtil.deleteImgTmp(tempNames);
                                tempNames.clear();
                                ToastUtils.showMessage(getApplicationContext(), "信息提交成功");
                                finish();
                            } else {
                                ToastUtils.showMessage(getApplicationContext(), "error: " + error);
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        DialogHelper.dismissDialog();
                        ToastUtils.showMessage(getApplicationContext(), e.toString());
                    }
                });
            }
        });
    }
}
