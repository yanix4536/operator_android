package com.zaodiandao.operator.shop.apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.ReserveOrder;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.shop.manage.adapter.ReserveOrderAdapter;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/12/21.
 * 商户订货单列表
 */
public class ReserveOrderListActivity extends BaseActivity {
    public static final String KER_ORDER_ID = "order_id";

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private ReserveOrderAdapter mAdapter;
    private List<ReserveOrder> datas = new ArrayList<>();

    private String mShopId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_reserve_order);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ReserveOrderAdapter(getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new ReserveOrderAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, ReserveOrder data) {
                Intent intent = new Intent(ReserveOrderListActivity.this, ReserveOrderDetailActivity.class);
                intent.putExtra(KER_ORDER_ID, data.getOrder_id());
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        mShopId = getIntent().getStringExtra(ShopStatusFragment.KEY_SHOP_ID);

        getData();
    }

    public void getData() {
        // 获取数据
        mApiService.getReserveOrders(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mShopId,
                new GenericArrayCallback<ReserveOrder>(getApplicationContext(), ReserveOrder.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mRecyclerView.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mRecyclerView.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<ReserveOrder> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }
}
