package com.zaodiandao.operator.shop.manage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.OrderListBean;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.order.OrderDetailActivity;
import com.zaodiandao.operator.order.OrderOperatorListAdapter;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static android.R.attr.type;

/**
 * Created by yanix on 2016/11/22.
 * 今日同城订单
 */
public class OrderTodayFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mllContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @BindView(R.id.spinner_status)
    Spinner mSpinner;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private OrderOperatorListAdapter mAdapter;
    private List<OrderListBean> datas = new ArrayList<>();
    private List<OrderListBean.OrderOperatorBean> realDatas = new ArrayList<>();
    private List<String> filterNames = new ArrayList<>();;
    private int selectIndex = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestTag = "OrderTodayFragment_" + type;
    }

    @Override
    public View getView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_shop_manage, null);
        ButterKnife.bind(this, root);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new OrderOperatorListAdapter(getActivity().getApplicationContext(), realDatas);
        mAdapter.setOnItemClickListener(new OrderOperatorListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, OrderListBean.OrderOperatorBean data) {
                Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
                // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(OrderDetailActivity.ORDER_ID, data.getOrder_id());
                getActivity().startActivity(intent);
            }
        });

        mRecyclerView.setAdapter(mAdapter);

        // 切换状态
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                realDatas.clear();
                realDatas.addAll(datas.get(position).getOrders());
                selectIndex = position;
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        fetchData();
        return root;
    }

    public void fetchData() {
        // 获取数据
        mApiService.getOrderTodayList(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericArrayCallback<OrderListBean>(getActivity().getApplicationContext(), OrderListBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mllContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<OrderListBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();

                        filterNames.clear();
                        for (OrderListBean bean : data) {
                            filterNames.add(bean.getTitle());
                        }
                        mSpinner.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.tv_name, filterNames));
                        mSpinner.setSelection(selectIndex);
                        mllContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mllContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    @Override
    public void getData() {

    }

    private void refresh() {
        // 获取数据
        mApiService.getOrderTodayList(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericArrayCallback<OrderListBean>(getActivity().getApplicationContext(), OrderListBean.class) {

                    @Override
                    public void success(List<OrderListBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();

                        mllContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        fetchData();
    }

    @Override
    public void onRefresh() {
        refresh();
    }
}
