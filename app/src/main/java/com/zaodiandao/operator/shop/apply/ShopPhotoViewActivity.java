package com.zaodiandao.operator.shop.apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.config.URLConstants;
import com.zaodiandao.operator.model.ShopPhoto;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.net.ZddStringCallback;
import com.zaodiandao.operator.shop.apply.adapter.ShopPhotoAdapter;
import com.zaodiandao.operator.util.DensityUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.PictureUtil;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;
import com.zaodiandao.operator.util.Util;
import com.zaodiandao.operator.view.CustemConfirmDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yanix on 2016/12/1.
 * 查看商户证件照
 */
public class ShopPhotoViewActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    public static final int REQUEST_CODE_GALLERY = 100;
    // 提示是否删除照片
    CustemConfirmDialog dialog;

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;

    // 保存照片信息
    private List<ShopPhoto> datas = new ArrayList<>();
    private ShopPhotoAdapter adapter;
    // 商户ID
    private String shopId;
    // 图片压缩保存的临时文件名
    private String tmepName;
    // 指示图片是否正在放大，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_shop_photo);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // 下拉刷新
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new ShopPhotoAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new ShopPhotoAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(final View view, final ShopPhoto data) {
                // 图片URL为空说明是添加图片，否则是编辑图片
                if (TextUtils.isEmpty(data.getPoster())) {
                    MenuSheetView menuSheetView =
                            new MenuSheetView(ShopPhotoViewActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.take_photo:
                                            // 拍照
                                            takePhoto();
                                            break;
                                        case R.id.take_from_album:
                                            // 从相册选取
                                            takeFromAlbum();
                                            break;
                                    }
                                    if (mSheetLayout.isSheetShowing()) {
                                        mSheetLayout.dismissSheet();
                                    }
                                    return true;
                                }
                            });
                    menuSheetView.inflateMenu(R.menu.menu_choose_image);
                    mSheetLayout.showWithSheetView(menuSheetView);
                } else {
                    MenuSheetView menuSheetView =
                            new MenuSheetView(ShopPhotoViewActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    if (mSheetLayout.isSheetShowing()) {
                                        mSheetLayout.dismissSheet();
                                    }
                                    switch (item.getItemId()) {
                                        case R.id.big_photo:
                                            open(((LinearLayout)view).getChildAt(0), data.getPoster());
                                            break;
                                        case R.id.photo_replace:
                                            takeFromAlbum();
                                            break;
                                        /*case R.id.photo_delete:
                                            dialog = new CustemConfirmDialog(ShopPhotoViewActivity.this, R.style.QQStyle, "确定删除吗？", "删除",
                                                    new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            dialog.dismiss();
                                                            // 删除图片
                                                            deletePhoto();
                                                        }
                                                    });
                                            dialog.show();
                                            break;*/
                                    }
                                    return true;
                                }
                            });
                    menuSheetView.inflateMenu(R.menu.menu_delete_or_update_photo);
                    mSheetLayout.showWithSheetView(menuSheetView);
                }
            }
        });

        // 获取传过来的商户ID
        shopId = getIntent().getStringExtra(ShopStatusFragment.KEY_SHOP_ID);
        // 获取证照信息
        getData();
    }

    private void open(View view, String url) {
        if (!isOpening) {
            // 防止重复点击
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), ShopPhotoDetailActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            ShopPhotoViewActivity.this.overridePendingTransition(0, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    /**
     * 删除图片
     */
    private void deletePhoto() {
        mApiService.deleteShopPhoto(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopId,
                new ZddStringCallback(getApplicationContext(),"result") {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(ShopPhotoViewActivity.this, "正在删除...");
                    }

                    @Override
                    public void success(String data) {
                        ToastUtils.showMessage(getApplicationContext(), data);
                        // 删除成功，重新获取数据
                        getData();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }
                });
    }

    /**
     * 获取证照数据
     */
    private void getData() {
        mApiService.getShopPhoto(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopId,
                new GenericArrayCallback<ShopPhoto>(getApplicationContext(), ShopPhoto.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        // 网络错误
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        // 数据解析错误
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<ShopPhoto> data) {
                        // 成功
                        datas.clear();
                        datas.addAll(data);
                        adapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        // 请求结束
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        // 请求重试
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        // 刷新
        getData();
    }

    /**
     * 从相册选择
     */
    private void takeFromAlbum() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openGallerySingle(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                try {
                    // 上传压缩后的图片
                    tmepName = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    uploadByOkHttp(new File(tmepName));
                } catch (IOException e) {
                    e.printStackTrace();
                    ToastUtils.showMessage(getApplicationContext(), e.toString());
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 拍一张
     */
    private void takePhoto() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                try {
                    tmepName = PictureUtil.bitmapToPath(resultList.get(0).getPhotoPath());
                    uploadByOkHttp(new File(tmepName));
                } catch (IOException e) {
                    e.printStackTrace();
                    ToastUtils.showMessage(getApplicationContext(), e.toString());
                }
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                    .setEnableEdit(true)        //开启编辑功能
                    .setEnableCrop(false)       //禁止裁剪功能
                    .setEnableRotate(false)     //禁止旋转功能
                    .setEnableCamera(true)      //开启相机功能
                    .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 110))     //裁剪宽度
                    .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                    .setCropSquare(false)       //裁剪正方形
                    .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                    .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                    .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                    .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                    .setEnablePreview(true)     //是否开启预览功能
                    .build();
    }

    /**
     * 上传文件
     * @param file 要上传的文件
     */
    private void uploadByOkHttp(File file) {
        DialogHelper.showDimDialog(ShopPhotoViewActivity.this, "正在上传...");
        // 要上传的file
        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
        String fileName = file.getName();

        // form的分割线, 自己定义
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody mBody = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                /* 上传一个普通的String参数 */
                .addFormDataPart("version", URLConstants.API_VERSION)
                .addFormDataPart("clerk_id", (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1) + "")
                .addFormDataPart("shop_id", shopId)
                /* 上传文件 */
                .addFormDataPart("file", fileName, fileBody)
                .build();

        Request request = new Request.Builder().url(URLConstants.URL_UPLOAD_FILE).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            public void onResponse(Call call, Response response) throws IOException {
                String bodyStr = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(bodyStr);
                    final String error = jsonObject.getString("error");
                    int code = jsonObject.getInt("code");
                    final boolean ok = code == 200;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            DialogHelper.dismissDialog();
                            if (ok) {
                                // 删除临时文件
                                PictureUtil.deleteImgTmp(tmepName);
                                ToastUtils.showMessage(getApplicationContext(), "图片上传成功");
                                // 重新获取数据
                                getData();
                            } else {
                                ToastUtils.showMessage(getApplicationContext(), "error: " + error);
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        DialogHelper.dismissDialog();
                        ToastUtils.showMessage(getApplicationContext(), e.toString());
                    }
                });
            }
        });
    }
}
