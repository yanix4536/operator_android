package com.zaodiandao.operator.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.ShopOperate;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.order.OrderDetailActivity;
import com.zaodiandao.operator.shop.apply.adapter.ShopOperatorAdapter;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/22.
 * 商户开店界面
 */
public class ShopOperateActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    public static final String KEY_SHOP_ID = "shop_id";
    public static final String KEY_SHOP_NAME = "shop_name";

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<ShopOperate> datas = new ArrayList<>();
    private ShopOperatorAdapter adapter;

    private String shopId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.fragment_shop_apply);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);

        adapter = new ShopOperatorAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new ShopOperatorAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, ShopOperate data) {
                Intent intent = new Intent(ShopOperateActivity.this, OrderDetailActivity.class);
                intent.putExtra(OrderDetailActivity.ORDER_ID, data.getOrder_id());
                startActivity(intent);
            }
        });

        shopId = getIntent().getStringExtra(KEY_SHOP_ID);
        String shopName = getIntent().getStringExtra(KEY_SHOP_NAME);
        setTitle(shopName);
        getData();
    }

    public void getData() {
        // 获取数据
        mApiService.actionShopOperate(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopId,
                new GenericArrayCallback<ShopOperate>(getApplicationContext(), ShopOperate.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<ShopOperate> data) {
                        datas.clear();
                        datas.addAll(data);
                        adapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
