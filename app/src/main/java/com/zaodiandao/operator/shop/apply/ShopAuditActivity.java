package com.zaodiandao.operator.shop.apply;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yangx on 2015/12/28.
 * 商户审核
 */
public class ShopAuditActivity extends BaseActivity {
    @BindView(R.id.viewpager)
    ViewPager mPager;
    @BindView(R.id.cursor)
    View cursor;
    @BindView(R.id.rb_shop)
    RadioButton rbShop;
    @BindView(R.id.rb_brand)
    RadioButton rbBrand;
    private List<Fragment> fragmentList;
    private RelativeLayout.LayoutParams layoutParams;
    private int bmpW;
    private int screenW;
    private int wholeWidth;
    private static final int TAB_COUNT = 2;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_shop_audit);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initCursor();
        initTab();
        initViewPager();
    }

    private void initTab() {
        rbShop.setOnClickListener(new MyClickListener(0));
        rbBrand.setOnClickListener(new MyClickListener(1));
        rbShop.setChecked(true);
    }

    public class MyClickListener implements View.OnClickListener {
        private int index = 0;

        public MyClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index, true);
        }
    }

    private void initCursor() {
        layoutParams = (RelativeLayout.LayoutParams) cursor.getLayoutParams();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        wholeWidth = dm.widthPixels;
        screenW = dm.widthPixels;
        bmpW = screenW / TAB_COUNT;
        layoutParams.width = bmpW;
        cursor.setLayoutParams(layoutParams);
        cursor.setVisibility(View.VISIBLE);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        ShopAuditStatusFragment shopFragment = new ShopAuditStatusFragment();
        Bundle bundleShop = new Bundle();
        bundleShop.putString("type", "1");
        shopFragment.setArguments(bundleShop);

        ShopAuditStatusFragment brandFragment = new ShopAuditStatusFragment();
        Bundle bundleBrand = new Bundle();
        bundleBrand.putString("type", "2");
        brandFragment.setArguments(bundleBrand);

        fragmentList.add(shopFragment);
        fragmentList.add(brandFragment);

        //给ViewPager设置适配器
        mPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        mPager.setOffscreenPageLimit(1);
        mPager.setCurrentItem(0);//设置当前显示标签页为第一页
        mPager.addOnPageChangeListener(new MyOnPageChangeListener());//页面变化时的监听器
    }

    /**
     * 视图加载完成之后执行：值得注意的是PopupWindow每次显示和销毁都会执行一次
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        /**
         * @param position             当前页面，及你点击滑动的页面
         * @param positionOffset       当前页面偏移的百分比
         * @param positionOffsetPixels 当前页面偏移的像素位置
         */
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tabMove(position, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageSelected(int position) {
            tabChange(position);
        }

        /**
         * 进度条移动 :核心的移动算法
         *
         * @param position             当前页位置
         * @param positionOffsetPixels 移动像素值
         */
        private void tabMove(int position, int positionOffsetPixels) {
            int moveI = (int) (bmpW * position + (((double) positionOffsetPixels / wholeWidth) * bmpW));
            layoutParams.leftMargin = moveI;
            cursor.setLayoutParams(layoutParams);
        }

        private void tabChange(int position) {
            if (position == 0) {
                rbShop.setChecked(true);
            } else if (position == 1) {
                rbBrand.setChecked(true);
            }
        }
    }

    class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
