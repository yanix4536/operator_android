package com.zaodiandao.operator.shop.distribute;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Stroke;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.ShopDistribute;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.shop.apply.adapter.AddressAdapter;
import com.zaodiandao.operator.util.DensityUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/22.
 * 门店分布界面
 */
public class ShopDistributeFragment extends BaseFragment {
    @BindView(R.id.iv_cooperated)
    ImageView mIvCooperated;
    @BindView(R.id.iv_auditing)
    ImageView mIvAuditing;
    @BindView(R.id.iv_noaudit)
    ImageView mIvNoaudit;
    @BindView(R.id.tv_cooperated)
    TextView mTvCooperated;
    @BindView(R.id.tv_auditing)
    TextView mTvAuditing;
    @BindView(R.id.tv_noaudit)
    TextView mTvNoaudit;

    @BindView(R.id.spinner_city)
    Spinner mSpinnerCity;
    @BindView(R.id.spinner_brand)
    Spinner mSpinnerBrand;

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    boolean isCooperatedCheck = true;
    boolean isAuditingCheck = true;
    boolean isNoauditCheck = false;

    @BindView(R.id.mapView)
    TextureMapView mMapView;
    private BaiduMap mBaiduMap;
    private LocationClient mLocClient;
    private MyBDLocationListener mMyBDLocationListener;
    private View popupView;
    private TextView tvName, tvAddress;

    // 保存地图上的标记
    private List<Overlay> mMarkerCooperatedList = new ArrayList<>();
    private List<Overlay> mMarkerAuditingList = new ArrayList<>();
    private List<Overlay> mMarkerUnauditList = new ArrayList<>();

    private List<ShopDistribute.ShopdataBean.PositionBean> mCooperatedData;
    private List<ShopDistribute.ShopdataBean.PositionBean> mAuditingData;
    private List<ShopDistribute.ShopdataBean.PositionBean> mUnauditData;

    // 保存门店数据
    private ShopDistribute mShopDistribute;
    // 保存当前城市id和品牌id
    private String mCityId, mBrandId;
    // 花圈半径
    private int mRadius;
    private double mLongitude, mLatitude;
    private Dialog dialog;

    private SuggestionSearch mSuggestionSearch;
    private PopupWindow popupWindow;
    private AddressAdapter addressAdapter;
    private List<SuggestionResult.SuggestionInfo> mSuggestionInfos = new ArrayList<>();
    private List<String> addresses = new ArrayList<>();
    private EditText etAddress;
    private double latitude, longitude;
    private Marker mMarker;

    GeoCoder mSearch = null; // 搜索模块，也可去掉地图模块独立使用
    private String city;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View getView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_shop_distribute, null);
        ButterKnife.bind(this, root);
        // 解决与ViewPager的滑动冲突
        mMapView.getChildAt(0).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mMapView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mMapView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        mBaiduMap = mMapView.getMap();
        mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        mMyBDLocationListener = new MyBDLocationListener();
        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (!marker.isVisible())
                    return true;

                if (popupView == null) {
                    popupView = getActivity().getLayoutInflater().inflate(R.layout.map_popup_info, null);
                    tvName = (TextView) popupView.findViewById(R.id.tv_name);
                    tvAddress = (TextView) popupView.findViewById(R.id.tv_address);
                }
                tvName.setText(marker.getExtraInfo().getString("name"));
                tvAddress.setText(marker.getExtraInfo().getString("address"));
                //创建InfoWindow , 传入 view， 地理坐标， y 轴偏移量
                InfoWindow mInfoWindow = new InfoWindow(popupView, marker.getPosition(), -DensityUtils.dp2px(getActivity().getApplicationContext(), 16));
                //显示InfoWindow
                mBaiduMap.showInfoWindow(mInfoWindow);
                return true;
            }
        });

        // 切换城市
        mSpinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int cityId = getCityIds(mShopDistribute.getCitys()).get(position);
                if (!mCityId.equals(cityId + "")) {
                    mCityId = cityId + "";
                    requestData(true);
                }
                // 如果切换的城市的当前选择的城市一样，则什么都不做，避免重复请求
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // 切换品牌
        mSpinnerBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int brandId = getBrandIds(mShopDistribute.getBrands()).get(position);
                if (!mBrandId.equals(brandId + "")) {
                    mBrandId = brandId + "";
                    requestData(true);
                }
                // 如果切换的品牌的当前选择的品牌一样，则什么都不做，避免重复请求
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // 初始化搜索模块，注册事件监听
        mSearch = GeoCoder.newInstance();
        mSearch.setOnGetGeoCodeResultListener(new OnGetGeoCoderResultListener() {
            @Override
            public void onGetGeoCodeResult(GeoCodeResult result) {
                DialogHelper.dismissDialog();
                if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    ToastUtils.showMessage(getActivity().getApplicationContext(), "抱歉，找不到地址");
                    return;
                }

                addMarker(result.getLocation().latitude, result.getLocation().longitude);
            }

            @Override
            public void onGetReverseGeoCodeResult(ReverseGeoCodeResult reverseGeoCodeResult) {

            }
        });

        initPopupWindow();
        mSuggestionSearch = SuggestionSearch.newInstance();
        mSuggestionSearch.setOnGetSuggestionResultListener(listener);

        // 默认城市和品牌
        mCityId = "0";
        mBrandId = "0";
        // 默认半径2千米
        mRadius = 2000;
        // 获取数据
        requestData(false);

        return root;
    }

    /**
     * 搜索建议监听器
     */
    OnGetSuggestionResultListener listener = new OnGetSuggestionResultListener() {
        public void onGetSuggestionResult(SuggestionResult res) {
            if (res == null || res.getAllSuggestions() == null || !dialog.isShowing()) {
                return;
                //未找到相关结果
            }
            //获取在线建议检索结果
            mSuggestionInfos.clear();
            mSuggestionInfos.addAll(res.getAllSuggestions());
            updateSuggestedAddress();
        }
    };

    /**
     * 更新搜索建议地址
     */
    private void updateSuggestedAddress() {
        addresses.clear();
        for (SuggestionResult.SuggestionInfo info : mSuggestionInfos) {
            addresses.add(info.district + info.key);
        }
        showPopupWindow(etAddress);
    }

    /**
     * 初始化popup window
     * 用于显示搜索建议
     */
    private void initPopupWindow() {
        View contentView = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.pop_window, null);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        addressAdapter = new AddressAdapter(getActivity().getApplicationContext(), addresses);
        addressAdapter.setOnItemClickListener(new AddressAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SuggestionResult.SuggestionInfo info = mSuggestionInfos.get(position);
                etAddress.setText(info.district + info.key);
                latitude = info.pt.latitude;
                longitude = info.pt.longitude;
                addMarker(latitude, longitude);
                popupWindow.dismiss();
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(addressAdapter);

        popupWindow = new PopupWindow(contentView, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
                // 这里如果返回true的话，touch事件将被拦截
                // 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
            }
        });
        // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
        // 我觉得这里是API的一个bug
        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_shape));
    }

    /**
     * 添加当前位置标记
     */
    private void addMarker(double tempLatitude, double tempLongitude) {
        if (mMarker != null)
            mMarker.remove();

        LatLng point = new LatLng(tempLatitude, tempLongitude);
        BitmapDescriptor bitmapCooperated = BitmapDescriptorFactory.fromResource(R.mipmap.map_cooperated);
        MarkerOptions option = new MarkerOptions()
                .position(point)
                .icon(bitmapCooperated)
                .draggable(true);
        option.animateType(MarkerOptions.MarkerAnimateType.none);
        mMarker = (Marker) mBaiduMap.addOverlay(option);

        LatLng ll = new LatLng(tempLatitude, tempLongitude);
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(ll).zoom(17.0f);
        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    /**
     * 显示搜索建议
     * @param view 显示在哪个视图的下方
     */
    private void showPopupWindow(View view) {
        hideSoftKeyBoard();
        addressAdapter.notifyDataSetChanged();
        // 设置好参数之后再show
        popupWindow.showAsDropDown(view);
    }

    /**
     * 强制隐藏软键盘
     */
    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etAddress.getWindowToken(), 0);
    }

    /**
     * 初始化定位信息
     */
    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        //可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        //可选，默认gcj02，设置返回的定位结果坐标系
        option.setCoorType("bd09ll");
        //可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setScanSpan(0);
        //可选，设置是否需要地址信息，默认不需要
        option.setIsNeedAddress(false);
        //可选，默认false,设置是否使用gps
        option.setOpenGps(true);
        //可选，默认false，设置是否当GPS有效时按照1S/1次频率输出GPS结果
        option.setLocationNotify(false);
        //可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationDescribe(true);
        //可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIsNeedLocationPoiList(true);
        //可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setIgnoreKillProcess(false);
        //可选，默认false，设置是否收集CRASH信息，默认收集
        option.SetIgnoreCacheException(false);
        //可选，默认false，设置是否需要过滤GPS仿真结果，默认需要
        option.setEnableSimulateGps(false);
        mLocClient.setLocOption(option);
    }

    /**
     * 定位城市坐标点
     */
    private void locateCityPosition() {
        LatLng ll = new LatLng(mLatitude, mLongitude);
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(ll).zoom(12.0f);
        mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    /**
     * 定位当前位置
     */
    private void startLocation() {
        // 开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        // 定位初始化
        mLocClient = new LocationClient(getActivity().getApplicationContext());
        mLocClient.registerLocationListener(mMyBDLocationListener);
        // 初始化定位信息
        initLocation();
        // 开始定位
        mLocClient.start();
    }

    /**
     * 定位监听器
     */
    class MyBDLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // mapView销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }

            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    // 此处设置开发者获取到的方向信息，顺时针0-360
                    .direction(100).latitude(location.getLatitude())
                    .longitude(location.getLongitude()).build();
            mBaiduMap.setMyLocationData(locData);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLocClient.stop();
        mLocClient.unRegisterLocationListener(mMyBDLocationListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mSearch.destroy();
        mMapView.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        if (mLayoutContent.getVisibility() == View.VISIBLE) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search:
                dialog = new Dialog(getActivity(), R.style.QQStyle);
                View contentView = getActivity().getLayoutInflater().inflate(R.layout.dialog_search_address, null);
                etAddress = (EditText) contentView.findViewById(R.id.et_address);
                /*etAddress.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (TextUtils.isEmpty(s.toString()) *//*|| !getActivity().hasWindowFocus()*//*) {
                            popupWindow.dismiss();
                            return;
                        }
                        // 使用建议搜索服务获取建议列表，结果在onSuggestionResult()中更新
                        mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())
                                .keyword(s.toString()).city(city));
                    }
                });*/
                contentView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                contentView.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Geo搜索
                        mSearch.geocode(new GeoCodeOption().city(city).address(etAddress.getText().toString()));
                        dialog.dismiss();
                        DialogHelper.showDimDialog(getActivity(), "正在搜索...");
                    }
                });

                dialog.setContentView(contentView);
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * 获取接口数据
     */
    private void requestData(final boolean isShowDialog) {
        mBaiduMap.hideInfoWindow();
        // 获取数据
        mApiService.shopDistribute(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mCityId, mBrandId,
                new GenericCallback<ShopDistribute>(getActivity().getApplicationContext(), ShopDistribute.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        if (isShowDialog) {
                            DialogHelper.showDimDialog(getActivity(), getString(R.string.dialog_loading_text));
                        }
                    }

                    @Override
                    public void success(ShopDistribute data) {
                        mShopDistribute = data;
                        mRadius = data.getBrand_radius();
                        mCityId = data.getCity_id() + "";
                        mBrandId = data.getBrand_id() + "";
                        // 初始化spinner
                        final List<ShopDistribute.CityBean> cities = data.getCitys();
                        city = cities.get(getCityIds(cities).indexOf(Integer.parseInt(mCityId))).getName();
                        List<ShopDistribute.BrandBean> brands = data.getBrands();
                        mSpinnerCity.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.tv_name, getCityNames(cities)));
                        mSpinnerBrand.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.tv_name, getBrandNames(brands)));
                        int cityIndex = getCityIds(cities).indexOf(data.getCity_id());
                        mLongitude = cities.get(cityIndex).getLongitude();
                        mLatitude = cities.get(cityIndex).getLatitude();
                        mSpinnerCity.setSelection(cityIndex);
                        mSpinnerBrand.setSelection(getBrandIds(brands).indexOf(data.getBrand_id()));
                        // 初始化地图标记
                        mIvCooperated.setBackgroundResource(R.mipmap.map_cooperated_checked);
                        mIvAuditing.setBackgroundResource(R.mipmap.map_auditing_checked);
                        mIvNoaudit.setBackgroundResource(R.mipmap.map_unaudit);
                        isCooperatedCheck = true;
                        isAuditingCheck = true;
                        isNoauditCheck = false;
                        mTvCooperated.setTextColor(getResources().getColor(R.color.text_dark));
                        mTvAuditing.setTextColor(getResources().getColor(R.color.text_dark));
                        mTvNoaudit.setTextColor(getResources().getColor(R.color.text_grey));
                        // 地图标注
                        //0.清除所有标记
                        removeMarkerCooperated();
                        removeMarkerAuditing();
                        removeMarkerUnaudit();
                        mCooperatedData = data.getShopdata().getPartner();
                        mAuditingData = data.getShopdata().getCheck();
                        mUnauditData = data.getShopdata().getInvalid();
                        //1.标注已合作
                        addMarkerCooperated(mCooperatedData);
                        //2.标注审核中
                        addMarkerAuditing(mAuditingData);
                        //3.隐藏未审核门店标记
                        addMarkerUnaudit(mUnauditData);
                        // hideMarkerUnaudit();
                        removeMarkerUnaudit();

                        locateCityPosition();
                        // 让地图显示当前位置
                        startLocation();
                        mLayoutContent.setVisibility(View.VISIBLE);
                        getActivity().invalidateOptionsMenu();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (isShowDialog) {
                            DialogHelper.dismissDialog();
                        }
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    /**
     * 获取城市名字集合
     */
    private List<String> getCityNames(List<ShopDistribute.CityBean> cities) {
        List<String> cityNames = new ArrayList<>();
        for (ShopDistribute.CityBean cityBean : cities) {
            cityNames.add(cityBean.getName());
        }
        return cityNames;
    }

    /**
     * 获取城市id集合
     */
    private List<Integer> getCityIds(List<ShopDistribute.CityBean> cities) {
        List<Integer> cityIds = new ArrayList<>();
        for (ShopDistribute.CityBean cityBean : cities) {
            cityIds.add(cityBean.getId());
        }
        return cityIds;
    }

    /**
     * 获取品牌名称集合
     */
    private List<String> getBrandNames(List<ShopDistribute.BrandBean> brands) {
        List<String> brandNames = new ArrayList<>();
        for (ShopDistribute.BrandBean brandBean : brands) {
            brandNames.add(brandBean.getName());
        }
        return brandNames;
    }

    /**
     * 获取品牌id集合
     */
    private List<Integer> getBrandIds(List<ShopDistribute.BrandBean> brands) {
        List<Integer> brandIds = new ArrayList<>();
        for (ShopDistribute.BrandBean brandBean : brands) {
            brandIds.add(brandBean.getId());
        }
        return brandIds;
    }

    @Override
    public void getData() {
        // 坑，这里getActivity()的返回值为null，因为这是第一个fragment
    }

    @OnClick(R.id.ll_cooperated)
    public void chooseCooperated(View view) {
        // 如果当前是选中状态，则隐藏标记，否则显示标记
        if (isCooperatedCheck) {
            mIvCooperated.setBackgroundResource(R.mipmap.map_cooperated);
            mTvCooperated.setTextColor(getResources().getColor(R.color.text_grey));
            // 隐藏标记
            // hideMarkerCooperated();
            removeMarkerCooperated();
        } else {
            mIvCooperated.setBackgroundResource(R.mipmap.map_cooperated_checked);
            mTvCooperated.setTextColor(getResources().getColor(R.color.text_dark));
            // 显示标记
            // showMarkerCooperated();
            addMarkerCooperated(mCooperatedData);
        }
        isCooperatedCheck = !isCooperatedCheck;
    }

    @OnClick(R.id.ll_auditing)
    public void chooseAuditing(View view) {
        if (isAuditingCheck) {
            mIvAuditing.setBackgroundResource(R.mipmap.map_auditing);
            mTvAuditing.setTextColor(getResources().getColor(R.color.text_grey));
            // 隐藏标记
            // hideMarkerAuditing();
            removeMarkerAuditing();
        } else {
            mIvAuditing.setBackgroundResource(R.mipmap.map_auditing_checked);
            mTvAuditing.setTextColor(getResources().getColor(R.color.text_dark));
            // 显示标记
            // showMarkerAuditing();
            addMarkerAuditing(mAuditingData);
        }
        isAuditingCheck = !isAuditingCheck;
    }

    @OnClick(R.id.ll_noaudit)
    public void chooseNoaudit(View view) {
        if (isNoauditCheck) {
            mIvNoaudit.setBackgroundResource(R.mipmap.map_unaudit);
            mTvNoaudit.setTextColor(getResources().getColor(R.color.text_grey));
            // 隐藏标记
            // hideMarkerUnaudit();
            removeMarkerUnaudit();
        } else {
            mIvNoaudit.setBackgroundResource(R.mipmap.map_unaudit_checked);
            mTvNoaudit.setTextColor(getResources().getColor(R.color.text_dark));
            // 显示标记
            // showMarkerUnaudit();
            addMarkerUnaudit(mUnauditData);
        }
        isNoauditCheck = !isNoauditCheck;
    }

    @OnClick(R.id.iv_locate)
    public void reLocate(View view) {
        locateCityPosition();
        startLocation();
    }

    /**
     * 添加地图标记：已合作门店
     */
    private void addMarkerCooperated(List<ShopDistribute.ShopdataBean.PositionBean> positionBeanList) {
        mMarkerCooperatedList.clear();
        for (ShopDistribute.ShopdataBean.PositionBean positionBean : positionBeanList) {
            LatLng point = new LatLng(positionBean.getLatitude(), positionBean.getLongitude());
            BitmapDescriptor bitmapCooperated = BitmapDescriptorFactory.fromResource(R.mipmap.map_cooperated_marker);
            MarkerOptions option = new MarkerOptions()
                    .position(point)
                    .icon(bitmapCooperated);
            option.animateType(MarkerOptions.MarkerAnimateType.none);
            Marker marker = (Marker) mBaiduMap.addOverlay(option);
            Bundle bundle = new Bundle();
            bundle.putString("name", positionBean.getName());
            bundle.putString("address", positionBean.getAddress());
            marker.setExtraInfo(bundle);
            mMarkerCooperatedList.add(marker);
            if (positionBean.getProtection() == 1) {
                // 画圆圈
                OverlayOptions circleOption = new CircleOptions()
                        .center(point)
                        .radius(mRadius)
                        .stroke(new Stroke(5, 0xFF7C9ADA))
                        .fillColor(0x7FFFFFFF);
                Overlay markerCircle = mBaiduMap.addOverlay(circleOption);
                mMarkerCooperatedList.add(markerCircle);
            }
        }
    }

    /**
     * 清除地图标记：已合作门店
     */
    private void removeMarkerCooperated() {
        mBaiduMap.hideInfoWindow();
        for (Overlay marker : mMarkerCooperatedList) {
            marker.setVisible(false);
            marker.remove();
        }
    }

    /**
     * 隐藏地图标记：已合作门店
     */
    private void hideMarkerCooperated() {
        mBaiduMap.hideInfoWindow();
        for (Overlay marker : mMarkerCooperatedList) {
            marker.setVisible(false);
        }
    }

    /**
     * 显示地图标记：已合作门店
     */
    private void showMarkerCooperated() {
        for (Overlay marker : mMarkerCooperatedList) {
            marker.setVisible(true);
        }
    }

    /**
     * 添加地图标记：审核中门店
     */
    private void addMarkerAuditing(List<ShopDistribute.ShopdataBean.PositionBean> positionBeanList) {
        mMarkerAuditingList.clear();
        for (ShopDistribute.ShopdataBean.PositionBean positionBean : positionBeanList) {
            LatLng point = new LatLng(positionBean.getLatitude(), positionBean.getLongitude());
            BitmapDescriptor bitmapCooperated = BitmapDescriptorFactory.fromResource(R.mipmap.map_auditing_marker);
            MarkerOptions option = new MarkerOptions()
                    .position(point)
                    .icon(bitmapCooperated);
            option.animateType(MarkerOptions.MarkerAnimateType.none);
            Marker marker = (Marker) mBaiduMap.addOverlay(option);
            Bundle bundle = new Bundle();
            bundle.putString("name", positionBean.getName());
            bundle.putString("address", positionBean.getAddress());
            marker.setExtraInfo(bundle);
            mMarkerAuditingList.add(marker);
            if (positionBean.getProtection() == 1) {
                // 画圆圈
                OverlayOptions circleOption = new CircleOptions()
                        .center(point)
                        .radius(mRadius)
                        .stroke(new Stroke(5, 0xFF7C9ADA))
                        .fillColor(0x7FFFFFFF);
                Overlay markerCircle = mBaiduMap.addOverlay(circleOption);
                mMarkerAuditingList.add(markerCircle);
            }
        }
    }

    /**
     * 清除地图标记：审核中门店
     */
    private void removeMarkerAuditing() {
        mBaiduMap.hideInfoWindow();
        for (Overlay marker : mMarkerAuditingList) {
            marker.setVisible(false);
            marker.remove();
        }
    }

    /**
     * 隐藏地图标记：审核中门店
     */
    private void hideMarkerAuditing() {
        mBaiduMap.hideInfoWindow();
        for (Overlay marker : mMarkerAuditingList) {
            marker.setVisible(false);
        }
    }

    /**
     * 显示地图标记：审核中门店
     */
    private void showMarkerAuditing() {
        for (Overlay marker : mMarkerAuditingList) {
            marker.setVisible(true);
        }
    }

    /**
     * 添加地图标记：未审核门店
     */
    private void addMarkerUnaudit(List<ShopDistribute.ShopdataBean.PositionBean> positionBeanList) {
        mMarkerUnauditList.clear();
        for (ShopDistribute.ShopdataBean.PositionBean positionBean : positionBeanList) {
            LatLng point = new LatLng(positionBean.getLatitude(), positionBean.getLongitude());
            BitmapDescriptor bitmapCooperated = BitmapDescriptorFactory.fromResource(R.mipmap.map_noaudit_marker);
            MarkerOptions option = new MarkerOptions()
                    .position(point)
                    .icon(bitmapCooperated);
            option.animateType(MarkerOptions.MarkerAnimateType.none);
            Marker marker = (Marker) mBaiduMap.addOverlay(option);
            Bundle bundle = new Bundle();
            bundle.putString("name", positionBean.getName());
            bundle.putString("address", positionBean.getAddress());
            marker.setExtraInfo(bundle);
            mMarkerUnauditList.add(marker);
        }
    }

    /**
     * 清除地图标记：未审核门店
     */
    private void removeMarkerUnaudit() {
        mBaiduMap.hideInfoWindow();
        for (Overlay marker : mMarkerUnauditList) {
            marker.setVisible(false);
            marker.remove();
        }
    }

    /**
     * 隐藏地图标记：未审核门店
     */
    /*private void hideMarkerUnaudit() {
        mBaiduMap.hideInfoWindow();
        for (Overlay marker : mMarkerUnauditList) {
            marker.setVisible(false);
        }
    }*/

    /**
     * 显示地图标记：未审核门店
     */
    private void showMarkerUnaudit() {
        for (Overlay marker : mMarkerUnauditList) {
            marker.setVisible(true);
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view){
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        requestData(false);
    }
}
