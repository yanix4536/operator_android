package com.zaodiandao.operator.shop.manage.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ShopBrandModel;

import java.util.List;

public class ShopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<ShopBrandModel.BrandBean> mShopInfos;

    public ShopAdapter(Context context, List<ShopBrandModel.BrandBean> shopInfos) {
        mContext = context;
        mShopInfos = shopInfos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new ShopAdapter.EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public interface OnRecyclerViewItemClickListener {
        // 为RecycleView设置点击事件
        void onItemClick(View view, ShopBrandModel.BrandBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            ShopAdapter.EmptyViewViewHolder emptyViewViewHolder = (ShopAdapter.EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无品牌");
        } else {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            ShopBrandModel.BrandBean data = mShopInfos.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvBrandName.setText(data.getBrand_name());
            // // 0=未过审 1=已加盟 2=未过审
            int status = data.getStatus();
            if (status == 0) {
                myViewHolder.tvStatus.setText("未过审");
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#FF0000"));
                myViewHolder.tvTime.setText("申请时间：" + data.getCreate());
            } else if (status == 1) {
                myViewHolder.tvStatus.setText("已加盟");
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#2E9200"));
                myViewHolder.tvTime.setText("加盟时间：" + data.getCreate());
            } else if (status == 2) {
                myViewHolder.tvStatus.setText("审核中");
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#F2B43B"));
                myViewHolder.tvTime.setText("申请时间：" + data.getCreate());
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mShopInfos != null && mShopInfos.size()>0) ? mShopInfos.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mShopInfos == null || mShopInfos.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ShopBrandModel.BrandBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvBrandName, tvStatus, tvTime;

        public MyViewHolder(View view) {
            super(view);
            tvBrandName = (TextView) view.findViewById(R.id.tv_brand_name);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}