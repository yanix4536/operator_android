package com.zaodiandao.operator.shop.apply;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.ReserveOrderDetail;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.SPUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/12/21.
 * 订货单详情
 */
public class ReserveOrderDetailActivity extends BaseActivity {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @BindView(R.id.ll_content)
    ScrollView mScrollView;
    @BindView(R.id.tv_reserve_order_number)
    TextView tvOrderNumber;
    @BindView(R.id.tv_linkman)
    TextView tvLinkman;
    @BindView(R.id.tv_link_mobile)
    TextView tvLinkMobile;
    @BindView(R.id.tv_deliver_address)
    TextView tvDeliverAddress;
    @BindView(R.id.tv_deliver_time)
    TextView tvDeliverTime;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_order_money)
    TextView tvOrderMoney;
    @BindView(R.id.tv_deliver_money)
    TextView tvDeliverMoney;
    @BindView(R.id.tv_discount_money)
    TextView tvDiscountMoney;
    @BindView(R.id.ll_menu)
    LinearLayout llMenu;

    private LayoutInflater mLayoutInflater;
    private ReserveOrderDetail mReserveOrderDetail;
    private String orderId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_reserve_order_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mLayoutInflater = getLayoutInflater();
        orderId = getIntent().getStringExtra(ReserveOrderListActivity.KER_ORDER_ID);
        getData();
    }

    public void getData() {
        // 获取数据
        mApiService.getReserveOrderDetail(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), orderId,
                new GenericCallback<ReserveOrderDetail>(getApplicationContext(), ReserveOrderDetail.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mScrollView.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mScrollView.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(ReserveOrderDetail data) {
                        mReserveOrderDetail = data;
                        tvOrderNumber.setText(data.getOrdernumber());
                        tvLinkman.setText(data.getLinkman());
                        tvLinkMobile.setText(data.getMobile());
                        tvDeliverAddress.setText(data.getAddress());
                        tvDeliverTime.setText(data.getDelivertime());
                        tvShopName.setText(data.getShopname());
                        tvOrderMoney.setText("￥" + data.getSummoney());
                        tvDeliverMoney.setText("￥" + data.getDelivermoney());
                        tvDiscountMoney.setText("￥" + data.getDiscountmoney());
                        mScrollView.setVisibility(View.VISIBLE);
                        fillMenu();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void fillMenu() {
        llMenu.removeAllViews();
        for (ReserveOrderDetail.ProductBean productBean : mReserveOrderDetail.getProducts()) {
            View view = mLayoutInflater.inflate(R.layout.item_menu, null);
            TextView tvMenuName = (TextView) view.findViewById(R.id.tv_menu_name);
            TextView tvMenuCount = (TextView) view.findViewById(R.id.tv_menu_count);
            TextView tvMenuAmount = (TextView) view.findViewById(R.id.tv_menu_amount);
            tvMenuName.setText(productBean.getProductname());
            tvMenuCount.setText(productBean.getAmount() + "(" + productBean.getSellunit() + ")");
            tvMenuAmount.setText("￥" + productBean.getMoney());
            llMenu.addView(view);
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }
}
