package com.zaodiandao.operator.shop.apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.socks.library.KLog;
import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.me.adapter.ShopStatus2Adapter;
import com.zaodiandao.operator.model.ShopApply;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/22.
 * 商户开店界面
 */
public class ShopStatus2Fragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<ShopApply> datas = new ArrayList<>();
    private ShopStatus2Adapter adapter;

    private String type;

    View root;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
        requestTag = "ShopStatus2Fragment" + type;
    }

    @Override
    public View getView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_shop_apply, null);
        ButterKnife.bind(this, root);
        EventBus.getDefault().register(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);

        adapter = new ShopStatus2Adapter(getActivity().getApplicationContext(), datas, type);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new ShopStatus2Adapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, ShopApply data) {
                Intent intent = new Intent(getActivity(), ShopAuditDetailActivity.class);
                intent.putExtra(ShopAuditDetailActivity.KEY_TAG, type);
                if ("1".equals(type)) {
                    intent.putExtra(ShopAuditDetailActivity.KEY_SHOP_ID, data.getShop_id() + "");
                } else {
                    intent.putExtra(ShopAuditDetailActivity.KEY_BRAND_ID, data.getShop_id() + "");
                }
                intent.putExtra(ShopAuditDetailActivity.KEY_FROM_NORMAL, true);
                startActivity(intent);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void getData() {
        // 获取数据
        mApiService.shopApply(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1), (Integer.parseInt(type) + 1) + "",
                new GenericArrayCallback<ShopApply>(getActivity().getApplicationContext(), ShopApply.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<ShopApply> data) {
                        datas.clear();
                        datas.addAll(data);
                        adapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                        getActivity().invalidateOptionsMenu();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        KLog.d(event.code);
        if (event.code == EventCode.CHANGE_SHOP_INFO || event.code == EventCode.CREATE_SHOP_INFO)
            getData();
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
