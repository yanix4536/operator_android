package com.zaodiandao.operator.shop.apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.socks.library.KLog;
import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.me.adapter.ShopAuditAdapter;
import com.zaodiandao.operator.model.ShopAuditModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/22.
 * 门店管理界面
 */
public class ShopAuditStatusFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.spinner_status)
    Spinner spinnerStatus;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<ShopAuditModel.AuditBean> datas = new ArrayList<>();
    private ShopAuditAdapter adapter;

    private ShopAuditModel mShopAuditModel;

    // 类型
    private String tag;
    private String status;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = getArguments().getString("type");
        requestTag = "ShopAuditStatusFragment_" + tag;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public View getView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_shop_audit, null);
        ButterKnife.bind(this, root);
        EventBus.getDefault().register(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        adapter = new ShopAuditAdapter(getActivity().getApplicationContext(), datas, tag);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new ShopAuditAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, ShopAuditModel.AuditBean data) {
                Intent intent = new Intent(getActivity(), ShopAuditDetailActivity.class);
                intent.putExtra(ShopAuditDetailActivity.KEY_TAG, tag);
                if ("1".equals(tag)) {
                    intent.putExtra(ShopAuditDetailActivity.KEY_SHOP_ID, data.getShopapply_id());
                } else {
                    intent.putExtra(ShopAuditDetailActivity.KEY_BRAND_ID, data.getRestaurant_id());
                }
                startActivity(intent);
            }
        });

        // 切换状态
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String statusId = getStatusIds(mShopAuditModel.getStatuss()).get(position);
                KLog.d("statusId = " + statusId);
                KLog.d("status = " + status);
                if (!status.equals(statusId)) {
                    status = statusId;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if ("1".equals(tag)) {
            fetchData();
        }
        return root;
    }

    /**
     * 获取仓库名称集合
     */
    private List<String> getStatusNames(List<ShopAuditModel.StatusBean> statuss) {
        List<String> statusNames = new ArrayList<>();
        for (ShopAuditModel.StatusBean statusBean : statuss) {
            statusNames.add(statusBean.getStatus_label());
        }
        return statusNames;
    }

    /**
     * 获取品牌id集合
     */
    private List<String> getStatusIds(List<ShopAuditModel.StatusBean> statuss) {
        List<String> statusIds = new ArrayList<>();
        for (ShopAuditModel.StatusBean statusBean : statuss) {
            statusIds.add(statusBean.getStatus_id());
        }
        return statusIds;
    }

    public void fetchData() {
        // 获取数据
        mApiService.getAuditList(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1), tag,
                new GenericCallback<ShopAuditModel>(getActivity().getApplicationContext(), ShopAuditModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(ShopAuditModel data) {
                        mShopAuditModel = data;
                        datas.clear();
                        datas.addAll(data.getJoininfo());
                        adapter.notifyDataSetChanged();

                        status = data.getStatus();
                        List<ShopAuditModel.StatusBean> statuss = data.getStatuss();
                        spinnerStatus.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_item, R.id.tv_name, getStatusNames(statuss)));
                        spinnerStatus.setSelection(getStatusIds(statuss).indexOf(status));
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    @Override
    public void getData() {
        if ("2".equals(tag)) {
            // 获取数据
            mApiService.getAuditList(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1), tag,
                    new GenericCallback<ShopAuditModel>(getActivity().getApplicationContext(), ShopAuditModel.class) {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            super.onError(call, e, id);
                            if (llContent.getVisibility() != View.VISIBLE)
                                mTvRetry.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void failure() {
                            super.failure();
                            if (llContent.getVisibility() != View.VISIBLE)
                                mTvRetry.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void success(ShopAuditModel data) {
                            mShopAuditModel = data;
                            datas.clear();
                            datas.addAll(data.getJoininfo());
                            adapter.notifyDataSetChanged();

                            status = data.getStatus();
                            List<ShopAuditModel.StatusBean> statuss = data.getStatuss();
                            spinnerStatus.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_item, R.id.tv_name, getStatusNames(statuss)));
                            spinnerStatus.setSelection(getStatusIds(statuss).indexOf(status));
                            llContent.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            mProgressBar.setVisibility(View.GONE);
                            if (mRefreshLayout.isRefreshing()) {
                                mRefreshLayout.setRefreshing(false);
                            }
                        }
                    });
        }
    }

    public void requestData(final boolean isShowDialog) {
        // 获取数据
        mApiService.getAuditList(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1), tag, status,
                new GenericCallback<ShopAuditModel>(getActivity().getApplicationContext(), ShopAuditModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(ShopAuditModel data) {
                        mShopAuditModel = data;
                        datas.clear();
                        datas.addAll(data.getJoininfo());
                        adapter.notifyDataSetChanged();

                        status = data.getStatus();
                        List<ShopAuditModel.StatusBean> statuss = data.getStatuss();
                        spinnerStatus.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_item, R.id.tv_name, getStatusNames(statuss)));
                        spinnerStatus.setSelection(getStatusIds(statuss).indexOf(status));
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        if (isShowDialog) {
                            DialogHelper.showDimDialog(getActivity(), getString(R.string.dialog_loading_text));
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }

                        if (isShowDialog) {
                            DialogHelper.dismissDialog();
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        fetchData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.code == EventCode.REFRESH)
            fetchData();
    }

    @Override
    public void onRefresh() {
        requestData(false);
    }
}
