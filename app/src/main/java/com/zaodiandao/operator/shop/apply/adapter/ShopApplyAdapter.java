package com.zaodiandao.operator.shop.apply.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ShopApply;
import com.zaodiandao.operator.shop.ShopOperateActivity;
import com.zaodiandao.operator.shop.apply.ShopPhotoViewActivity;
import com.zaodiandao.operator.shop.apply.ShopStatusFragment;
import com.zaodiandao.operator.shop.brand.BrandActivity;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ShopApplyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<ShopApply> mShopApplies;

    public ShopApplyAdapter(Context context, List<ShopApply> shopApplies) {
        mContext = context;
        mShopApplies = shopApplies;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop_apply, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, ShopApply data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            EmptyViewViewHolder emptyViewViewHolder = (EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无商户");
        } else {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            final ShopApply data = mShopApplies.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvStationName.setText(data.getName());
            myViewHolder.tvStationMobile.setText(data.getMobile());

            Paint paint = myViewHolder.tvStationMobile.getPaint();
            paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);
            paint.setAntiAlias(true);
            myViewHolder.tvStationMobile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mobile = myViewHolder.tvStationMobile.getText().toString();
                    if (TextUtils.isEmpty(mobile)) {
                        return;
                    }
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_DIAL);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.setData(Uri.parse("tel:" + mobile));
                    mContext.startActivity(intent);
                }
            });

            myViewHolder.tvBuyStatus.setText(data.getOrder_status_label());

            myViewHolder.tvStationAddress.setText(data.getAddress());
            myViewHolder.llPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ShopPhotoViewActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(ShopStatusFragment.KEY_SHOP_ID, data.getShop_id() + "");
                    mContext.startActivity(intent);
                }
            });
            myViewHolder.llOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, BrandActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(BrandActivity.KEY_SHOP_ID, data.getShop_id() + "");
                    mContext.startActivity(intent);
                }
            });
            myViewHolder.llStatistics.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ShopOperateActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(ShopOperateActivity.KEY_SHOP_ID, data.getShop_id() + "");
                    intent.putExtra(ShopOperateActivity.KEY_SHOP_NAME, data.getName() + "");
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (mShopApplies != null && mShopApplies.size()>0) ? mShopApplies.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mShopApplies == null || mShopApplies.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ShopApply) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvStationName, tvStationMobile, tvStationAddress, tvBuyStatus;
        LinearLayout llPhoto, llOrder, llStatistics;

        public MyViewHolder(View view) {
            super(view);
            tvStationName = (TextView) view.findViewById(R.id.tv_station_name);
            tvStationMobile = (TextView) view.findViewById(R.id.tv_station_mobile);
            tvStationAddress = (TextView) view.findViewById(R.id.tv_station_address);
            llPhoto = (LinearLayout) view.findViewById(R.id.ll_photo);
            llOrder = (LinearLayout) view.findViewById(R.id.ll_order);
            llStatistics = (LinearLayout) view.findViewById(R.id.ll_statistics);
            tvBuyStatus = (TextView) view.findViewById(R.id.tv_buy_status);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}