package com.zaodiandao.operator.shop.manage.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ShopView;
import com.zaodiandao.operator.util.BitmapUtils;

import java.util.List;

public class ShopImageAdapter extends RecyclerView.Adapter<ShopImageAdapter.MyViewHolder> implements View.OnClickListener {
    private Context mContext;
    private List<ShopView.ImageBean> mImages;

    public ShopImageAdapter(Context context, List<ShopView.ImageBean> images) {
        mContext = context;
        mImages = images;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop_image, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, ShopView.ImageBean data);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ShopView.ImageBean data = mImages.get(position);
        String imgUrl = data.getImgurl();
        if (!TextUtils.isEmpty(imgUrl)) {
            if (imgUrl.startsWith("/storage")) {
                // 显示本地图片
                Bitmap bitmap = BitmapUtils.createImageThumbnail(mContext, imgUrl);
                holder.ivShop.setImageBitmap(bitmap);
            } else {
                // 显示网络图片
                Picasso.with(mContext).load(imgUrl).placeholder(R.mipmap.avatar_default)
                        .config(Bitmap.Config.RGB_565)
                        .into(holder.ivShop);
            }
        } else {
            holder.ivShop.setImageResource(R.mipmap.add_image);
        }
        holder.itemView.setTag(data);
    }

    @Override
    public int getItemCount() {
        return mImages == null ? 0 : mImages.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ShopView.ImageBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivShop;

        public MyViewHolder(View view) {
            super(view);
            ivShop = (ImageView) view.findViewById(R.id.iv_shop);
        }
    }
}