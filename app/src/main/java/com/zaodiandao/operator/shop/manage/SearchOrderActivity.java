package com.zaodiandao.operator.shop.manage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.OrderListBean;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.order.OrderDetailActivity;
import com.zaodiandao.operator.order.OrderOperatorListAdapter;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class SearchOrderActivity extends BaseActivity {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar pbLoading;
    @BindView(R.id.tv_retry)
    TextView tvFail;
    @BindView(R.id.recyclerView)
    RecyclerView lvOrder;
    @BindView(R.id.et_search)
    AutoCompleteTextView etSearch;

    private List<OrderListBean.OrderOperatorBean> datas = new ArrayList<>();
    private OrderOperatorListAdapter mAdapter;

    private String keyword;

    @Override
    protected void getView(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        setContentView(R.layout.activity_order_search);
        ButterKnife.bind(this);

        lvOrder.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new OrderOperatorListAdapter(getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new OrderOperatorListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, OrderListBean.OrderOperatorBean data) {
                Intent intent = new Intent(SearchOrderActivity.this, OrderDetailActivity.class);
                intent.putExtra(OrderDetailActivity.ORDER_ID, data.getOrder_id());
                startActivity(intent);
            }
        });

        lvOrder.setAdapter(mAdapter);

        pbLoading.setVisibility(View.GONE);

        etSearch.requestFocus();
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                keyword = s.toString();
                if (TextUtils.isEmpty(keyword)) {
                    pbLoading.setVisibility(View.GONE);
                    lvOrder.setVisibility(View.GONE);
                    tvFail.setVisibility(View.GONE);
                } else {
                    pbLoading.setVisibility(View.VISIBLE);
                    tvFail.setVisibility(View.GONE);
                    lvOrder.setVisibility(View.GONE);
                    getOrder();
                }
            }
        });
    }

    private void getOrder() {
        // 获取数据
        mApiService.searchOrder(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), keyword,
                new GenericArrayCallback<OrderListBean.OrderOperatorBean>(getApplicationContext(), OrderListBean.OrderOperatorBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (!call.isCanceled()) {
                            lvOrder.setVisibility(View.GONE);
                            tvFail.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void success(List<OrderListBean.OrderOperatorBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        mAdapter.notifyDataSetChanged();
                        tvFail.setVisibility(View.GONE);
                        lvOrder.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        pbLoading.setVisibility(View.GONE);
                    }
                });
    }


    @OnClick(R.id.rl_cancel)
    public void cancel(View v) {
        finish();
        overridePendingTransition(0, 0);
    }

    @OnClick(R.id.tv_retry)
    public void fail(View v) {
        pbLoading.setVisibility(View.VISIBLE);
        tvFail.setVisibility(View.GONE);
        getOrder();
    }

}