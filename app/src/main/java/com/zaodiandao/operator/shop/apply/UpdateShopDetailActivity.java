package com.zaodiandao.operator.shop.apply;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.model.CityData;
import com.zaodiandao.operator.model.ShopDetail;
import com.zaodiandao.operator.model.ShopInfoPost;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.net.ZddStringCallback;
import com.zaodiandao.operator.shop.apply.adapter.AddressAdapter;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/30.
 * 添加/修改商户信息界面
 */
public class UpdateShopDetailActivity extends BaseActivity {
    public static final String KEY_ACTION_SHOP = "key_action_shop";
    // 添加或修改模式
    public static final int ACTION_ADD = 1;
    public static final int ACTION_UPDATE = 2;

    @BindView(R.id.spinner_city)
    Spinner spinnerCity;
    @BindView(R.id.spinner_country)
    Spinner spinnerCountry;
    @BindView(R.id.et_shop_name)
    EditText etShopName;
    @BindView(R.id.et_shop_address)
    EditText etShopAddress;
    @BindView(R.id.et_linkman)
    EditText etLinkMan;
    @BindView(R.id.et_link_mobile)
    EditText etLinkMobile;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.mapView)
    TextureMapView mMapView;
    private BaiduMap map;
    private MyBDLocationListener mMyBDLocationListener;
    private LocationClient mLocClient;
    private SuggestionSearch mSuggestionSearch;

    private ShopDetail mShopDetail;
    private ArrayAdapter<String> countryAdapter;
    private List<String> countryNames = new ArrayList<>();
    private List<String> addresses = new ArrayList<>();
    private List<SuggestionResult.SuggestionInfo> mSuggestionInfos = new ArrayList<>();
    private int cityId, countryId;
    private double latitude, longitude;
    private int action;
    private String cityName = "上海";

    private PopupWindow popupWindow;
    private AddressAdapter addressAdapter;
    private Marker mMarker;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_update_shop_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        map = mMapView.getMap();
        map.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        // marker设置拖拽监听器
        map.setOnMarkerDragListener(new BaiduMap.OnMarkerDragListener() {
            public void onMarkerDrag(Marker marker) {
                //拖拽中
            }

            public void onMarkerDragEnd(Marker marker) {
                //拖拽结束，保存位置
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;
            }

            public void onMarkerDragStart(Marker marker) {
                //开始拖拽
            }
        });
        mMyBDLocationListener = new MyBDLocationListener();

        //解决地图的touch事件和scrollView的touch事件冲突问题
        mMapView.getChildAt(0).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mMapView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mMapView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        // 判断是添加还是修改
        action = getIntent().getIntExtra(KEY_ACTION_SHOP, 0);

        initPopupWindow();
        mSuggestionSearch = SuggestionSearch.newInstance();
        mSuggestionSearch.setOnGetSuggestionResultListener(listener);

        etShopAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString()) || !hasWindowFocus()) {
                    popupWindow.dismiss();
                    return;
                }
                // 使用建议搜索服务获取建议列表，结果在onSuggestionResult()中更新
                mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())
                        .keyword(s.toString()).city(cityName));
            }
        });

        if (action == ACTION_UPDATE) {
            // 如果是修改，则获取传过来的商户信息
            mShopDetail = (ShopDetail) getIntent().getSerializableExtra(ShopDetailInfoActivity.KEY_SHOP_DETAIL);
            ShopDetail.ShopBean shopBean = mShopDetail.getShop();
            // 设置标题为商户名称
            setTitle(shopBean.getName());
            cityId = mShopDetail.getCity_id();
            countryId = mShopDetail.getCountry_id();
            latitude = shopBean.getLatitude();
            longitude = shopBean.getLongitude();
            etShopName.setText(shopBean.getName());
            etShopAddress.setText(shopBean.getAddress());
            etLinkMan.setText(shopBean.getLinkman());
            etLinkMobile.setText(shopBean.getMobile());
            etShopName.requestFocus();

            countryAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, R.id.tv_name, getCountryNames(mShopDetail.getCitycountrys(), cityId));
            spinnerCity.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, R.id.tv_name, getCityNames(mShopDetail.getCitycountrys())));
            spinnerCountry.setAdapter(countryAdapter);
            cityName = getCityNames(mShopDetail.getCitycountrys()).get(getCityIds(mShopDetail.getCitycountrys()).indexOf(cityId));
            spinnerCity.setSelection(getCityIds(mShopDetail.getCitycountrys()).indexOf(cityId));
            spinnerCountry.setSelection(getCountryIds(mShopDetail.getCitycountrys(), cityId).indexOf(countryId));
            // 切换城市
            spinnerCity.setOnItemSelectedListener(new CityAdapter(mShopDetail.getCitycountrys()));
            // 切换区县
            spinnerCountry.setOnItemSelectedListener(new CountryAdapter(mShopDetail.getCitycountrys()));

            // 添加标记
            addMarker(latitude, longitude);
        } else {
            setTitle("添加商户");
            startLocation();
            getCityData();
        }
    }

    class CityAdapter implements AdapterView.OnItemSelectedListener {
        private List<ShopDetail.CitycountrysBean> data;

        public CityAdapter(List<ShopDetail.CitycountrysBean> data) {
            this.data = data;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            int localCityId = getCityIds(data).get(position);
            if (localCityId == cityId)
                return;
            cityId = localCityId;
            cityName = getCityNames(data).get(getCityIds(data).indexOf(cityId));
            getCountryNames(data, cityId);
            countryAdapter.notifyDataSetChanged();
            spinnerCountry.setSelection(0);
            countryId = getCountryIds(data, cityId).get(0);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class CountryAdapter implements AdapterView.OnItemSelectedListener {
        private List<ShopDetail.CitycountrysBean> data;

        public CountryAdapter(List<ShopDetail.CitycountrysBean> data) {
            this.data = data;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            countryId = getCountryIds(data, cityId).get(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    /**
     * 搜索建议监听器
     */
    OnGetSuggestionResultListener listener = new OnGetSuggestionResultListener() {
        public void onGetSuggestionResult(SuggestionResult res) {
            if (res == null || res.getAllSuggestions() == null) {
                return;
                //未找到相关结果
            }
            //获取在线建议检索结果
            mSuggestionInfos.clear();
            mSuggestionInfos.addAll(res.getAllSuggestions());
            updateSuggestedAddress();
        }
    };

    /**
     * 更新搜索建议地址
     */
    private void updateSuggestedAddress() {
        addresses.clear();
        for (SuggestionResult.SuggestionInfo info : mSuggestionInfos) {
            addresses.add(info.district + info.key);
        }
        showPopupWindow(etShopAddress);
    }

    /**
     * 初始化popup window
     * 用于显示搜索建议
     */
    private void initPopupWindow() {
        View contentView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.pop_window, null);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        addressAdapter = new AddressAdapter(getApplicationContext(), addresses);
        addressAdapter.setOnItemClickListener(new AddressAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SuggestionResult.SuggestionInfo info = mSuggestionInfos.get(position);
                etShopAddress.setText(info.district + info.key);
                latitude = info.pt.latitude;
                longitude = info.pt.longitude;
                addMarker(latitude, longitude);
                popupWindow.dismiss();
            }
        });
        recyclerView.setAdapter(addressAdapter);

        popupWindow = new PopupWindow(contentView, RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
                // 这里如果返回true的话，touch事件将被拦截
                // 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
            }
        });
        // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
        // 我觉得这里是API的一个bug
        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_shape));
    }

    /**
     * 显示搜索建议
     * @param view 显示在哪个视图的下方
     */
    private void showPopupWindow(View view) {
        hideSoftKeyBoard();
        addressAdapter.notifyDataSetChanged();
        // 设置好参数之后再show
        popupWindow.showAsDropDown(view);
    }

    private void getCityData() {
        mApiService.getCityData(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericCallback<CityData>(getApplicationContext(), CityData.class) {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(UpdateShopDetailActivity.this, getString(R.string.dialog_loading_text));
                    }

                    @Override
                    public void success(final CityData data) {
                        cityId = data.getCity_id();
                        countryAdapter = new ArrayAdapter<String>(UpdateShopDetailActivity.this, R.layout.spinner_item, R.id.tv_name, getCountryNames(data.getCitycountrys(), cityId));
                        spinnerCity.setAdapter(new ArrayAdapter<String>(UpdateShopDetailActivity.this, R.layout.spinner_item, R.id.tv_name, getCityNames(data.getCitycountrys())));
                        spinnerCountry.setAdapter(countryAdapter);
                        cityName = getCityNames(data.getCitycountrys()).get(getCityIds(data.getCitycountrys()).indexOf(cityId));
                        spinnerCity.setSelection(getCityIds(data.getCitycountrys()).indexOf(cityId));
                        spinnerCountry.setSelection(0);
                        // 切换城市
                        spinnerCity.setOnItemSelectedListener(new CityAdapter(data.getCitycountrys()));
                        // 切换区县
                        spinnerCountry.setOnItemSelectedListener(new CountryAdapter(data.getCitycountrys()));

                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }
                });
    }

    /*
    * 定位监听器
    */
    class MyBDLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // mapView销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }

            latitude = location.getLatitude();
            longitude = location.getLongitude();
            addMarker(latitude, longitude);
        }
    }

    /**
     * 定位当前位置
     */
    private void startLocation() {
        // 开启定位图层
        map.setMyLocationEnabled(true);
        // 定位初始化
        mLocClient = new LocationClient(getApplicationContext());
        mLocClient.registerLocationListener(mMyBDLocationListener);
        // 初始化定位信息
        initLocation();
        // 开始定位
        mLocClient.start();
    }

    /**
     * 添加当前位置标记
     */
    private void addMarker(double tempLatitude, double tempLongitude) {
        if (mMarker != null)
            mMarker.remove();

        LatLng point = new LatLng(tempLatitude, tempLongitude);
        BitmapDescriptor bitmapCooperated = BitmapDescriptorFactory.fromResource(R.mipmap.map_cooperated);
        MarkerOptions option = new MarkerOptions()
                .position(point)
                .icon(bitmapCooperated)
                .draggable(true);
        option.animateType(MarkerOptions.MarkerAnimateType.none);
        mMarker = (Marker) map.addOverlay(option);

        LatLng ll = new LatLng(tempLatitude, tempLongitude);
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(ll).zoom(17.0f);
        map.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    /**
     * 初始化定位信息
     */
    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        //可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        //可选，默认gcj02，设置返回的定位结果坐标系
        option.setCoorType("bd09ll");
        //可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setScanSpan(0);
        //可选，设置是否需要地址信息，默认不需要
        option.setIsNeedAddress(false);
        //可选，默认false,设置是否使用gps
        option.setOpenGps(true);
        //可选，默认false，设置是否当GPS有效时按照1S/1次频率输出GPS结果
        option.setLocationNotify(true);
        //可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationDescribe(true);
        //可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIsNeedLocationPoiList(true);
        //可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setIgnoreKillProcess(false);
        //可选，默认false，设置是否收集CRASH信息，默认收集
        option.SetIgnoreCacheException(false);
        //可选，默认false，设置是否需要过滤GPS仿真结果，默认需要
        option.setEnableSimulateGps(false);
        mLocClient.setLocOption(option);
    }

    private List<String> getCityNames(List<ShopDetail.CitycountrysBean> cities) {
        List<String> cityNames = new ArrayList<>();
        for (ShopDetail.CitycountrysBean city : cities) {
            cityNames.add(city.getName());
        }
        return cityNames;
    }

    private List<Integer> getCityIds(List<ShopDetail.CitycountrysBean> cities) {
        List<Integer> cityIds = new ArrayList<>();
        for (ShopDetail.CitycountrysBean city : cities) {
            cityIds.add(city.getId());
        }
        return cityIds;
    }

    private List<String> getCountryNames(List<ShopDetail.CitycountrysBean> cities, int cityId) {
        countryNames.clear();
        for (ShopDetail.CitycountrysBean city : cities) {
            if (city.getId() == cityId) {
                for (ShopDetail.CitycountrysBean.CountryBean country : city.getCountry()) {
                    countryNames.add(country.getName());
                }
                break;
            }
        }
        return countryNames;
    }

    private List<Integer> getCountryIds(List<ShopDetail.CitycountrysBean> cities, int cityId) {
        List<Integer> countryIds = new ArrayList<>();
        for (ShopDetail.CitycountrysBean city : cities) {
            if (city.getId() == cityId) {
                for (ShopDetail.CitycountrysBean.CountryBean country : city.getCountry()) {
                    countryIds.add(country.getId());
                }
                break;
            }
        }
        return countryIds;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        if (mLocClient != null) {
            mLocClient.stop();
            mLocClient.unRegisterLocationListener(mMyBDLocationListener);
        }
        mMapView.onDestroy();
        mSuggestionSearch.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

    @OnClick( )
    public void changeInfo(View view) {
        String name = etShopName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_station_name_not_empty));
            return;
        }
        String address = etShopAddress.getText().toString().trim();
        if (TextUtils.isEmpty(address)) {
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_station_address_not_empty));
            return;
        }
        String linkman = etLinkMan.getText().toString().trim();
        if (TextUtils.isEmpty(linkman)) {
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_station_link_man_not_empty));
            return;
        }
        String mobile = etLinkMobile.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_station_link_mobile_not_empty));
            return;
        }

        ShopInfoPost shopInfoPost = new ShopInfoPost();
        shopInfoPost.setCityId(cityId);
        shopInfoPost.setCountry(countryId);
        shopInfoPost.setLongitude(longitude);
        shopInfoPost.setLatitude(latitude);
        shopInfoPost.setName(name);
        shopInfoPost.setAddress(address);
        shopInfoPost.setLinkman(linkman);
        shopInfoPost.setMobile(mobile);

        if (action == ACTION_UPDATE) {
            updateShop(shopInfoPost);
        } else {
            createShop(shopInfoPost);
        }
    }

    /**
     * 修改商户
     * @param shopInfoPost
     */
    private void updateShop(ShopInfoPost shopInfoPost) {
        mApiService.updateShopInfo(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mShopDetail.getShop().getShop_id() + "", shopInfoPost,
                new ZddStringCallback(getApplicationContext(), "shop_id") {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(UpdateShopDetailActivity.this, "修改中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(String response) {
                        ToastUtils.showMessage(getApplicationContext(), "修改成功");
                        // 修改成功
                        EventBus.getDefault().post(new MessageEvent(EventCode.CHANGE_SHOP_INFO, "商户信息修改成功!"));
                        finish();
                    }
                });
    }

    /**
     * 创建商户
     * @param shopInfoPost
     */
    private void createShop(ShopInfoPost shopInfoPost) {
        mApiService.createShopInfo(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopInfoPost,
                new ZddStringCallback(getApplicationContext(), "shop_id") {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(UpdateShopDetailActivity.this, "商户创建中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(String response) {
                        ToastUtils.showMessage(getApplicationContext(), "创建成功");
                        // 修改成功
                        EventBus.getDefault().post(new MessageEvent(EventCode.CREATE_SHOP_INFO, "商户信息创建成功!"));
                        finish();
                    }
                });
    }

    /**
     * 强制隐藏软键盘
     */
    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etShopAddress.getWindowToken(), 0);
    }
}
