package com.zaodiandao.operator.shop.manage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.config.URLConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.model.ShopDetail;
import com.zaodiandao.operator.model.ShopDistribute;
import com.zaodiandao.operator.model.ShopView;
import com.zaodiandao.operator.shop.apply.ShopDetailInfoActivity;
import com.zaodiandao.operator.shop.apply.ShopPhotoDetailActivity;
import com.zaodiandao.operator.shop.manage.adapter.ShopImageAdapter;
import com.zaodiandao.operator.util.DensityUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.PictureUtil;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;
import com.zaodiandao.operator.util.Util;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yanix on 2016/11/28.
 * 添加门店界面
 */
public class AddShopActivity extends BaseActivity {
    public static final int REQUEST_CODE_GALLERY = 100;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_station_name)
    TextView mTvStationName;
    @BindView(R.id.spinner_brand)
    Spinner mSpinnerBrand;
    @BindView(R.id.et_shop_name)
    EditText mEtShopName;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout mSheetLayout;
    // 接收传过来的门店信息
    private ShopDetail mShopDetail;
    // 用于显示的数据列表
    private List<ShopView.ImageBean> datas = new ArrayList<>();
    // 保存本地图片列表
    private List<ShopView.ImageBean> localDatas = new ArrayList<>();
    // 添加图片按钮
    private ShopView.ImageBean mImageBean = new ShopView.ImageBean();
    private ShopImageAdapter adapter;
    // 保存临时文件名
    private List<String> tempNames = new ArrayList<>();
    // 保存要上传的文件列表
    private List<File> uploadFiles = new ArrayList<>();
    private int maxSize;
    // 保存用户选择的品牌ID
    private int selectedBrandId;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        setContentView(R.layout.activity_add_shop);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        // 获取门店信息
        mShopDetail = (ShopDetail) getIntent().getSerializableExtra(ShopDetailInfoActivity.KEY_SHOP_DETAIL);
        mTvStationName.setText(mShopDetail.getShop().getName());
        // 设置品牌
        mSpinnerBrand.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_item, R.id.tv_name, getBrandNames(mShopDetail.getBrands())));
        mSpinnerBrand.setSelection(0);
        selectedBrandId = 0;
        // 切换品牌
        mSpinnerBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedBrandId = getBrandIds(mShopDetail.getBrands()).get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // 添加+图片
        datas.add(mImageBean);
        adapter = new ShopImageAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        // 点击图片事件
        adapter.setOnItemClickListener(new ShopImageAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(final View view, final ShopView.ImageBean data) {
                // 判断图片Url是否为空，为空说明是添加图片
                if (TextUtils.isEmpty(data.getImgurl())) {
                    MenuSheetView menuSheetView =
                            new MenuSheetView(AddShopActivity.this, MenuSheetView.MenuType.LIST, "选择照片...", new MenuSheetView.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.take_photo:
                                            // 拍一张
                                            takePhoto();
                                            break;
                                        case R.id.take_from_album:
                                            // 从相册选择
                                            takeFromAlbum();
                                            break;
                                    }
                                    if (mSheetLayout.isSheetShowing()) {
                                        mSheetLayout.dismissSheet();
                                    }
                                    return true;
                                }
                            });
                    menuSheetView.inflateMenu(R.menu.menu_choose_image);
                    mSheetLayout.showWithSheetView(menuSheetView);
                } else {
                    // 图片URL地址不为空，那么就是编辑这张图片
                    MenuSheetView menuSheetView =
                            new MenuSheetView(AddShopActivity.this, MenuSheetView.MenuType.LIST, "编辑照片...", new MenuSheetView.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.big_image:  // 查看大图
                                            // 查看大图
                                            open(((RelativeLayout)view).getChildAt(0), data.getImgurl());
                                            break;
                                        case R.id.delete:  // 删除这张图片
                                            localDatas.remove(data);
                                            datas.remove(data);
                                            if (!datas.contains(mImageBean))
                                                datas.add(mImageBean);
                                            adapter.notifyDataSetChanged();
                                            break;
                                    }
                                    if (mSheetLayout.isSheetShowing()) {
                                        mSheetLayout.dismissSheet();
                                    }
                                    return true;
                                }
                            });
                    menuSheetView.inflateMenu(R.menu.menu_delete_image);
                    mSheetLayout.showWithSheetView(menuSheetView);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    /**
     * 图片放大
     * @param view
     * @param url
     */
    private void open(View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), ShopPhotoDetailActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            AddShopActivity.this.overridePendingTransition(0, 0);
        }
    }

    /**
     * 获取品牌名称集合
     */
    private List<String> getBrandNames(List<ShopDistribute.BrandBean> brands) {
        List<String> brandNames = new ArrayList<>();
        for (ShopDistribute.BrandBean brand : brands) {
            brandNames.add(brand.getName());
        }
        return brandNames;
    }

    /**
     * 获取品牌ID集合
     */
    private List<Integer> getBrandIds(List<ShopDistribute.BrandBean> brands) {
        List<Integer> brandIds = new ArrayList<>();
        for (ShopDistribute.BrandBean brand : brands) {
            brandIds.add(brand.getId());
        }
        return brandIds;
    }

    /**
     * 从相册选择
     */
    private void takeFromAlbum() {
        ArrayList<String> selectedImages = new ArrayList<>();
        for (ShopView.ImageBean imageBean : localDatas) {
            selectedImages.add(imageBean.getImgurl());
        }
        FunctionConfig config = new FunctionConfig.Builder()
                .setMutiSelectMaxSize(6)        // 最多选择6张图片
                .setSelected(selectedImages)    // 设置选中的图片
                .build();
        GalleryFinal.openGalleryMuti(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                updateGridView(resultList);
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    /**
     * 拍照
     */
    private void takePhoto() {
        FunctionConfig config = getFunctionConfig();
        GalleryFinal.openCamera(REQUEST_CODE_GALLERY, config, new GalleryFinal.OnHanlderResultCallback() {
            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                datas.remove(mImageBean);
                datas.removeAll(localDatas);
                ShopView.ImageBean imageBean = new ShopView.ImageBean(resultList.get(0).getPhotoPath());
                localDatas.add(imageBean);

                datas.addAll(localDatas);
                maxSize = 6 - datas.size();
                if (maxSize > 0) {
                    datas.add(mImageBean);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {
                ToastUtils.showMessage(getApplicationContext(), errorMsg);
            }
        });
    }

    private void updateGridView(List<PhotoInfo> resultList) {
        datas.remove(mImageBean);
        datas.removeAll(localDatas);
        localDatas.clear();
        for (PhotoInfo photoInfo : resultList) {
            ShopView.ImageBean imageBean = new ShopView.ImageBean(photoInfo.getPhotoPath());
            localDatas.add(imageBean);
        }
        datas.addAll(localDatas);
        maxSize = 6 - datas.size();
        if (maxSize > 0) {
            datas.add(mImageBean);
        }
        adapter.notifyDataSetChanged();
    }

    private FunctionConfig getFunctionConfig() {
        return new FunctionConfig.Builder()
                .setEnableEdit(true)        //开启编辑功能
                .setEnableCrop(false)       //开启裁剪功能
                .setEnableRotate(false)     //开启旋转功能
                .setEnableCamera(true)      //开启相机功能
                .setCropWidth(DensityUtils.dp2px(getApplicationContext(), 100))     //裁剪宽度
                .setCropHeight(DensityUtils.dp2px(getApplicationContext(), 100))    //裁剪高度
                .setCropSquare(false)       //裁剪正方形
                .setForceCrop(false)        //启动强制裁剪功能,一进入编辑页面就开启图片裁剪，不需要用户手动点击裁剪，此功能只针对单选操作
                .setForceCropEdit(false)    //在开启强制裁剪功能时是否可以对图片进行编辑（也就是是否显示旋转图标和拍照图标）
                .setRotateReplaceSource(false)   //配置选择图片时是否替换原始图片，默认不替换
                .setCropReplaceSource(false)     //配置裁剪图片时是否替换原始图片，默认不替换
                .setEnablePreview(true)     //是否开启预览功能
                .build();
    }

    @OnClick(R.id.btn_add_shop)
    public void submit(View view) {
        final String shopName = mEtShopName.getText().toString().trim();
        if (TextUtils.isEmpty(shopName)) {
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_shop_name_not_empty));
            return;
        }

        DialogHelper.showDimDialog(AddShopActivity.this, "正在上传...");

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                // 添加待上传文件
                for (ShopView.ImageBean imageBean : localDatas) {
                    try {
                        String tempName = PictureUtil.bitmapToPath(imageBean.getImgurl());
                        tempNames.add(tempName);
                        uploadFiles.add(new File(tempName));
                    } catch (final IOException e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogHelper.dismissDialog();
                                ToastUtils.showMessage(getApplicationContext(), e.toString());
                            }
                        });
                        return;
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        uploadImage(shopName);
                    }
                });
            }
        });
    }

    /**
     * 添加门店
     * @param shopName 门店名称
     */
    private void uploadImage(String shopName) {
        /* form的分割线,自己定义 */
        String boundary = "xx--------------------------------------------------------------xx";

        MultipartBody.Builder builder = new MultipartBody.Builder(boundary).setType(MultipartBody.FORM)
                /* 上传一个普通的String参数 */
                .addFormDataPart("version", URLConstants.API_VERSION)
                .addFormDataPart("clerk_id", (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1) + "")
                .addFormDataPart("shop_id", mShopDetail.getShop().getShop_id() + "")
                .addFormDataPart("name", shopName)
                .addFormDataPart("brand_id", selectedBrandId + "");

        for (int i = 0; i < uploadFiles.size(); i++) {
            RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), uploadFiles.get(i));
            String fileName = uploadFiles.get(i).getName();
            builder = builder.addFormDataPart("file[]", fileName, fileBody);
        }
        MultipartBody mBody = builder.build();

        Request request = new Request.Builder().url(URLConstants.URL_UPLOAD_SHOP_PHOTO).post(mBody).build();
        new OkHttpClient().newCall(request).enqueue(new okhttp3.Callback() {
            public void onResponse(Call call, Response response) throws IOException {
                String bodyStr = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(bodyStr);
                    final String error = jsonObject.getString("error");
                    int code = jsonObject.getInt("code");
                    final boolean ok = code == 200;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            DialogHelper.dismissDialog();
                            if (ok) {
                                PictureUtil.deleteImgTmp(tempNames);
                                tempNames.clear();
                                ToastUtils.showMessage(getApplicationContext(), "信息提交成功");
                                EventBus.getDefault().post(new MessageEvent(EventCode.CREATE_RESTAURANT, "门店添加成功!"));
                                finish();
                            } else {
                                ToastUtils.showMessage(getApplicationContext(), "error: " + error);
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        DialogHelper.dismissDialog();
                        ToastUtils.showMessage(getApplicationContext(), e.toString());
                    }
                });
            }
        });
    }
}
