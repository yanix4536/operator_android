package com.zaodiandao.operator.shop.brand;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.ShopBrandModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.shop.manage.ShopViewActivity;
import com.zaodiandao.operator.shop.manage.adapter.ShopAdapter;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/22.
 * 门店管理界面
 */
public class BrandActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    public static final String KEY_RESTAURANT_ID = "key_restaurant_id";
    public static final String KEY_SHOP_ID = "key_shop_id";

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mllContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<ShopBrandModel.BrandBean> datas = new ArrayList<>();
    private ShopAdapter mAdapter;
    private String shopId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_brand);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mllContent.setColorSchemeResources(R.color.colorPrimary);
        mllContent.setOnRefreshListener(this);
        mAdapter = new ShopAdapter(getApplicationContext(), datas);
        mAdapter.setOnItemClickListener(new ShopAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, ShopBrandModel.BrandBean data) {
                Intent intent = new Intent(BrandActivity.this, ShopViewActivity.class);
                intent.putExtra(KEY_RESTAURANT_ID, data.getRestaurant_id()+"");
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        shopId = getIntent().getStringExtra(KEY_SHOP_ID);
        getData();
    }

    private void getData() {
        // 获取数据
        mApiService.getShopBrand(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopId,
                new GenericCallback<ShopBrandModel>(getApplicationContext(), ShopBrandModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mllContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mllContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(ShopBrandModel data) {
                        setTitle(data.getShop_name());
                        datas.clear();
                        datas.addAll(data.getBrand());
                        mAdapter.notifyDataSetChanged();
                        mllContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mllContent.isRefreshing()) {
                            mllContent.setRefreshing(false);
                        }
                    }
                });
    }


    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
