package com.zaodiandao.operator.shop.apply;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.model.AuditDetailModel;
import com.zaodiandao.operator.model.BrandDetailModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.net.ZddStringCallback;
import com.zaodiandao.operator.shop.apply.adapter.BrandImageAdapter;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;
import com.zaodiandao.operator.util.Util;
import com.zaodiandao.operator.view.ScrollGridLayoutManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2017/2/20.
 * 质量反馈详情
 */
public class ShopAuditDetailActivity extends BaseActivity {
    public static final String KEY_TAG = "tag";
    public static final String KEY_SHOP_ID = "shop_id";
    public static final String KEY_BRAND_ID = "brand_id";
    public static final String KEY_FROM_NORMAL = "from_normal";

    @BindView(R.id.tv_name_label)
    TextView tvNameLabel;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_linkman)
    TextView tvLinkman;
    @BindView(R.id.tv_mobile)
    TextView tvMobile;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_saleman)
    TextView tvSaleman;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.ll_action_one)
    LinearLayout llActionOne;
    @BindView(R.id.btn_action)
    Button btnAction;
    @BindView(R.id.ll_action_two)
    LinearLayout llActionTwo;
    @BindView(R.id.tv_refuse)
    TextView tvRefuse;
    @BindView(R.id.tv_pass)
    TextView tvPass;

    private List<BrandDetailModel.ImageBean> datas = new ArrayList<>();
    private BrandImageAdapter adapter;
    // 指示当前图片放大动画是否打开，防止重复点击
    boolean isOpening;

    private String tag;
    private String shopId;
    private String brandId;
    private boolean fromNormal;
    private Dialog dialog;

    private AuditDetailModel mAuditDetailModel;
    private String salemanId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 这里的作用是让状态栏从隐藏到显示的过程中,布局不受影响
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );

        setContentView(R.layout.activity_feedback_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ScrollGridLayoutManager recyclerViews = new ScrollGridLayoutManager(this, 3);
        recyclerViews.setScrollEnabled(false);
        mRecyclerView.setLayoutManager(recyclerViews);

        adapter = new BrandImageAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        // 照片点击事件
        adapter.setOnItemClickListener(new BrandImageAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(final View view, final BrandDetailModel.ImageBean data) {
                open(((RelativeLayout) view).getChildAt(0), data.getImg_url());
            }
        });

        Intent intent = getIntent();
        tag = intent.getStringExtra(KEY_TAG);
        fromNormal = intent.getBooleanExtra(KEY_FROM_NORMAL, false);
        if ("1".equals(tag)) {
            shopId = intent.getStringExtra(KEY_SHOP_ID);
            getAuditDetailWithShop();
        } else {
            brandId = intent.getStringExtra(KEY_BRAND_ID);
            getAuditDetailWithBrand();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpening = false;
    }

    private void getAuditDetailWithShop() {
        mApiService.getAuditDetailWithShop(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopId,
                new GenericCallback<AuditDetailModel>(getApplicationContext(), AuditDetailModel.class) {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(ShopAuditDetailActivity.this, "努力加载中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(AuditDetailModel data) {
                        mAuditDetailModel = data;
                        tvNameLabel.setText("商户名");
                        tvName.setText(data.getShop_name());
                        tvStatus.setText(data.getStatus_label());
                        tvLinkman.setText(data.getLinkman());
                        tvMobile.setText(data.getMobile());
                        tvAddress.setText(data.getAddress());
                        tvSaleman.setText(data.getSalesman());

                        String statusId = data.getStatus_id();
                        if ("0".equals(statusId)) {   // 审核失败
                            llActionOne.setVisibility(View.GONE);
                            llActionTwo.setVisibility(View.GONE);
                        } else if ("1".equals(statusId) && !fromNormal) {  // 已申请
                            llActionOne.setVisibility(View.GONE);
                            llActionTwo.setVisibility(View.VISIBLE);
                        } else if ("2".equals(statusId) && !fromNormal) {  // 已审核
                            llActionOne.setVisibility(View.VISIBLE);
                            llActionTwo.setVisibility(View.GONE);
                            btnAction.setText("指 派");
                        } else if ("3".equals(statusId) && !fromNormal) {  // 已指派
                            llActionOne.setVisibility(View.VISIBLE);
                            llActionTwo.setVisibility(View.GONE);
                            btnAction.setText("开 店");
                        }

                        datas.clear();
                        for (String img : data.getImgs()) {
                            datas.add(new BrandDetailModel.ImageBean(img));
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
        );
    }

    private void getAuditDetailWithBrand() {
        mApiService.getAuditDetailWithBrand(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), brandId,
                new GenericCallback<AuditDetailModel>(getApplicationContext(), AuditDetailModel.class) {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(ShopAuditDetailActivity.this, "努力加载中...");
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(AuditDetailModel data) {
                        mAuditDetailModel = data;
                        tvNameLabel.setText("品牌名");
                        tvName.setText(data.getBrand_name());
                        tvStatus.setText(data.getStatus_label());
                        tvLinkman.setText(data.getLinkman());
                        tvMobile.setText(data.getMobile());
                        tvAddress.setText(data.getAddress());
                        tvSaleman.setText(data.getSalesman());

                        String statusId = data.getStatus_id();
                        if ("0".equals(statusId)) {   // 失效
                            llActionOne.setVisibility(View.GONE);
                            llActionTwo.setVisibility(View.GONE);
                        } else if ("1".equals(statusId)) {  // 申请通过
                            llActionOne.setVisibility(View.GONE);
                            llActionTwo.setVisibility(View.GONE);
                        } else if ("2".equals(statusId) && !fromNormal) {  // 未审核
                            llActionOne.setVisibility(View.GONE);
                            llActionTwo.setVisibility(View.VISIBLE);
                        }

                        datas.clear();
                        for (String img : data.getImgs()) {
                            datas.add(new BrandDetailModel.ImageBean(img));
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
        );
    }

    private void open(View view, String url) {
        if (!isOpening) {
            isOpening = true;
            Intent i = new Intent(getApplicationContext(), ShopPhotoDetailActivity.class);
            i.putExtra(KeyConstants.ORIG_X, Util.getXY(view)[0]);
            i.putExtra(KeyConstants.ORIG_Y, Util.getXY(view)[1]);
            i.putExtra(KeyConstants.ORIG_WIDTH, view.getWidth());
            i.putExtra(KeyConstants.ORIG_HEIGHT, view.getHeight());
            i.putExtra(KeyConstants.IMG_URL, url);

            // Start activity disable animations
            startActivity(i);
            ShopAuditDetailActivity.this.overridePendingTransition(0, 0);
        }
    }

    private void submit(String shopId, String brandId, String type, String remark, String salesmanId) {
        // 获取数据
        mApiService.actionAudit(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), shopId, brandId, type, remark, salesmanId,
                new ZddStringCallback(getApplicationContext(), "message") {

                    @Override
                    public void success(String data) {
                        ToastUtils.showMessage(getApplicationContext(), data);
                        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH, "刷新!"));
                        finish();
                    }

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(ShopAuditDetailActivity.this, "正在提交...");
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }
                });
    }

    @OnClick(R.id.btn_action)
    public void action(View v) {
        String statusId = mAuditDetailModel.getStatus_id();
        if ("2".equals(statusId)) {     // 指派业务员
            dialog = new Dialog(this, R.style.QQStyle);
            View contentView = getLayoutInflater().inflate(R.layout.dialog_salesman_list, null);

            RadioGroup radioGroup = (RadioGroup) contentView.findViewById(R.id.radioGroup);
            radioGroup.removeAllViews();

            for (AuditDetailModel.SaleMan saleMan : mAuditDetailModel.getSalesmans()) {
                View itemView = getLayoutInflater().inflate(R.layout.listitem_saleman, null);
                RadioButton radioButton = (RadioButton) itemView;
                radioButton.setId(saleMan.getSalesman_id());
                radioButton.setText(saleMan.getSalesman_name());
                radioGroup.addView(itemView);
            }

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    salemanId = checkedId + "";
                }
            });

            contentView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            contentView.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (TextUtils.isEmpty(salemanId)) {
                        ToastUtils.showMessage(getApplicationContext(), "请选择业务员");
                        return;
                    }

                    dialog.dismiss();
                    if ("1".equals(tag)) {
                        submit(shopId, "0", "3", "", salemanId);
                    } else {
                        submit("0", brandId, "3", "", salemanId);
                    }
                }
            });

            dialog.setContentView(contentView);
            dialog.show();
        } else if ("3".equals(statusId)) {  // 开店
            if ("1".equals(tag)) {
                submit(shopId, "0", "4", "", "0");
            } else {
                submit("0", brandId, "4", "", "0");
            }
        }
    }

    @OnClick(R.id.tv_refuse)
    public void refuse(View v) {
        dialog = new Dialog(this, R.style.QQStyle);
        View contentView = getLayoutInflater().inflate(R.layout.dialog_audit_refuse, null);
        final EditText etRefuseReason = (EditText) contentView.findViewById(R.id.et_refuse_reason);
        contentView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        contentView.findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String reason = etRefuseReason.getText().toString().trim();
                if ("1".equals(tag)) {
                    submit(shopId, "0", "2", reason, "0");
                } else {
                    submit("0", brandId, "2", reason, "0");
                }
            }
        });

        dialog.setContentView(contentView);
        dialog.show();
    }

    @OnClick(R.id.tv_pass)
    public void pass(View v) {
        if ("1".equals(tag)) {
            submit(shopId, "0", "1", "", "0");
        } else {
            submit("0", brandId, "1", "", "0");
        }
    }
}
