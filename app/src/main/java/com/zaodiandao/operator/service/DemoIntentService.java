package com.zaodiandao.operator.service;

import android.content.Context;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTTransmitMessage;
import com.zaodiandao.operator.OperatorApplication;
import com.zaodiandao.operator.model.PushModel;

/**
 * 继承 GTIntentService 接收来自个推的消息, 所有消息在线程中回调, 如果注册了该服务, 则务必要在 AndroidManifest中声明, 否则无法接受消息<br>
 * onReceiveMessageData 处理透传消息<br>
 * onReceiveClientId 接收 cid <br>
 * onReceiveOnlineState cid 离线上线通知 <br>
 * onReceiveCommandResult 各种事件处理回执 <br>
 */
public class DemoIntentService extends GTIntentService {

    public DemoIntentService() {

    }

    @Override
    public void onReceiveServicePid(Context context, int pid) {
    }

    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage msg) {
        byte[] payload = msg.getPayload();
        if (payload != null) {
            String jsonData = new String(payload);
            try {
                PushModel data = JSON.parseObject(jsonData, PushModel.class);
                sendMessage(data, 0);
            } catch (Exception e) {
                return;
            }
        }
    }

    @Override
    public void onReceiveClientId(Context context, String clientid) {
    }

    @Override
    public void onReceiveOnlineState(Context context, boolean online) {
    }

    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage cmdMessage) {
    }

    private void sendMessage(PushModel data, int what) {
        Message msg = Message.obtain();
        msg.what = what;
        msg.obj = data;
        OperatorApplication.sendMessage(msg);
    }
}
