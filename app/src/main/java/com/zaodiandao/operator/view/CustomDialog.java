package com.zaodiandao.operator.view;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.zaodiandao.operator.R;

/**
 * 自定义Loading对话框
 */
public class CustomDialog extends Dialog {
    private static int default_width = 240; //默认宽度
    private static int default_height = 80;//默认高度
    private TextView tvText;

    public CustomDialog(Context context, int style, String text) {
        this(context, default_width, default_height, style, text);
    }

    public CustomDialog(Context context, int width, int height, int style, String text) {
        super(context, style);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_loading, null);
        tvText = (TextView) view.findViewById(R.id.loading_text);
        tvText.setText(text);
        setContentView(view);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        float density = getDensity(context);
        params.width = (int) (width * density);
        params.height = (int) (height * density);
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
    }

    public void setText(String text){
        if (tvText != null)
            tvText.setText(text);
    }

    private float getDensity(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        return dm.density;
    }
}
