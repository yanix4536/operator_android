package com.zaodiandao.operator.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.zaodiandao.operator.R;

/**
 * Created by yangx on 2015/12/30.
 * 自定义提示对话框
 */
public class CustemConfirmDialog extends Dialog {
    Context context;
    private TextView tvMsg;
    // 确认和取事件
    private View.OnClickListener confirmListener;
    private View.OnClickListener cancelListener;
    // 要显示的内容
    private String msg;
    // 确认按钮的文字
    private String confirmStr;
    // 是否显示取消按钮
    private boolean isShowCancel = true;

    public CustemConfirmDialog(Context context, int theme, String msg, View.OnClickListener confirmListener) {
        super(context, theme);
        this.context = context;
        this.msg = msg;
        this.confirmListener = confirmListener;
    }

    public CustemConfirmDialog(Context context, int theme, String msg, View.OnClickListener confirmListener, View.OnClickListener cancelListener) {
        this(context, theme, msg, confirmListener);
        this.cancelListener = cancelListener;
    }

    public CustemConfirmDialog(Context context, int theme, String msg, String confirmStr, View.OnClickListener confirmListener, View.OnClickListener cancelListener) {
        this(context, theme, msg, confirmListener, cancelListener);
        this.confirmStr = confirmStr;
    }

    public CustemConfirmDialog(Context context, int theme, String msg, String confirmStr, boolean isShowCancel, View.OnClickListener confirmListener, View.OnClickListener cancelListener) {
        this(context, theme, msg, confirmStr, confirmListener, cancelListener);
        this.isShowCancel = isShowCancel;
    }

    public CustemConfirmDialog(Context context, int theme, String msg, String confirmStr, View.OnClickListener confirmListener) {
        this(context, theme, msg, confirmListener);
        this.confirmStr = confirmStr;
    }

    public CustemConfirmDialog(Context context, int theme, String msg, String confirmStr, boolean isShowCancel, View.OnClickListener confirmListener) {
        this(context, theme, msg, confirmStr, confirmListener);
        this.isShowCancel = isShowCancel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.confirm_dialog);
        TextView tvOk = (TextView) findViewById(R.id.tv_confirm);
        TextView tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvOk.setOnClickListener(confirmListener);
        // 设置确认按钮的文字
        if (!TextUtils.isEmpty(confirmStr)) {
            tvOk.setText(confirmStr);
        }
        // 显示或隐藏取消按钮
        if (!isShowCancel) {
            tvCancel.setVisibility(View.GONE);
        } else {
            if (cancelListener != null) {
                tvCancel.setOnClickListener(cancelListener);
            } else {
                tvCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
            }
        }
        tvMsg = (TextView) findViewById(R.id.tv_msg);
        tvMsg.setText(msg);
    }
}
