package com.zaodiandao.operator.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 林冠宏 on 2016/7/11.
 */
public class SlowlyProgressBar {
    private static final int StartAnimation = 0x12;

    private Handler handler;
    private View view;

    /**
     * 当前的位移距离和速度
     */
    private int thisWidth = 0;
    private int thisSpeed = 0;

    /**
     * 当前的进度长度
     */
    private int progress = 0;

    /**
     * 移动单位
     */
    private int record = 0;

    /**
     * 10dp each time
     */
    private int width = 10;

    /**
     * 3dp
     */
    private int height = 3;

    private boolean isStart = false;

    /**
     * 屏幕宽度
     */
    private int phoneWidth = 0;

    private int i = 0;

    /**
     * 每次的移动记录容器，位移对应每帧时间
     */
    private List<Integer> progressQuery = new ArrayList<>();
    private List<Integer> speedQuery = new ArrayList<>();

    private void initData() {
        thisWidth = 0;
        thisSpeed = 0;
        progress = 0;
        record = 0;
        width = 10;
        height = 3;
        isStart = false;
        i = 0;
    }

    public SlowlyProgressBar(View view, int phoneWidth) {
        initHandler();
        this.phoneWidth = phoneWidth;
        this.view = view;
    }

    /**
     * 善后工作，释放引用的持有，方能gc生效
     */
    public void destroy() {
        if (progressQuery != null) {
            progressQuery.clear();
            progressQuery = null;
        }
        if (speedQuery != null) {
            speedQuery.clear();
            speedQuery = null;
        }
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }

    public void setProgress(int progress) {
        if (progress > 100 || progress <= 0) {
            return;
        }
        /** 每次传入的 width 应该是包含之前的数值,所以下面要减去 */
        /** 下面记得转化比例，公式 （屏幕宽度 * progress / 100） */
        this.width = (progress * phoneWidth) / 100;

        /** lp.width 总是获取前一次的 大小 */
        /** 移动 100px 时的速度一次倍率 是 2 */
        int size = progressQuery.size();
        if (size != 0) {
            size = progressQuery.get(size - 1);
        }
        /** 计算倍率，2/100 = x/width */
        int distance = width - size;
        int speedTime;
        if (distance <= 100) {
            speedTime = 2;
        } else {
            speedTime = (int) ((2 * distance) / 100.0);
        }
        /** 添加 */
        progressQuery.add(this.width);
        speedQuery.add(speedTime);
        /** 开始 */
        if (!isStart) {
            isStart = true;
            handler.sendEmptyMessage(StartAnimation);
        }
    }

    public SlowlyProgressBar setViewHeight(int height) {
        this.height = height;
        return this;
    }

    private void initHandler() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case StartAnimation:
                        /** 提取队列信息 */
                        if (progress >= thisWidth) { /** 如果已经跑完，那么移出 */
                            if (progressQuery.size() == i) {
                                if (progress >= 100) {
                                    startDismissAnimation();
                                }
                                isStart = false;
                                break;
                            }
                            thisWidth = progressQuery.get(i);
                            thisSpeed = speedQuery.get(i);
                            i++;
                        }
                        move(thisSpeed, view.getLayoutParams().width);
                        /** 发信息的延时长度并不会影响速度 */
                        handler.sendEmptyMessageDelayed(StartAnimation, 1);
                        break;
                }
            }
        };
    }

    /**
     * 移动
     */
    private void move(int speedTime, int lastWidth) {
        if (speedTime > 9) {
            speedTime = 9; /** 控制最大倍率 */
        }
        /** 乘3是纠正误差 */
        progress = (record * speedTime);
        /** 纠正 */
        if (progress >= lastWidth) {
            view.setLayoutParams(new RelativeLayout.LayoutParams(progress, dip2px(view.getContext(), height)));
        }
        record++;
    }

    public int dip2px(Context context, int dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dip, context.getResources().getDisplayMetrics());
    }

    private void startDismissAnimation() {
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "alpha", 1.0f, 0.0f);
        anim.setDuration(500);
        anim.setInterpolator(new DecelerateInterpolator());     // 减速

        anim.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                initData();
                view.setLayoutParams(new RelativeLayout.LayoutParams(0, dip2px(view.getContext(), height)));
                view.setAlpha(1.0f);
            }
        });
        anim.start();
    }
}



