package com.zaodiandao.operator.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.igexin.sdk.PushManager;
import com.umeng.analytics.MobclickAgent;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.BuildConfig;
import com.zaodiandao.operator.MainActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.LoginModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/21.
 * 运营登录界面
 */
public class LoginActivity extends BaseActivity {
    @BindView(R.id.et_account)
    TextInputEditText etAccount;
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    @Override
    protected void getView(Bundle savedInstanceState) {
        // 设置全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        // 监听输入框内容变化
        addTextChangeListener(etAccount);
        addTextChangeListener(etPassword);
    }

    @OnClick(R.id.btn_login)
    public void login(View view){
        // 按钮可以点击，说明输入内容不为空，所以这里不用再判断内容是否为空
        String account = etAccount.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        // 去登录
        mApiService.login(requestTag, account, password,
                new GenericCallback<LoginModel>(getApplicationContext(), LoginModel.class) {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(LoginActivity.this, "登录中...");
            }

            @Override
            public void onAfter(int id) {
                DialogHelper.dismissDialog();
                super.onAfter(id);
            }

            @Override
            public void success(LoginModel data) {
                // 登录成功，保存业务员ID的值，下次就不用登录了
                SPUtils.put(getApplicationContext(), KeyConstants.OPERATOR_ID, data.getClerk_id());
                SPUtils.put(getApplicationContext(), KeyConstants.LEVEL, data.getAuthority());
                PushManager.getInstance().bindAlias(getApplicationContext(), "client_id_" + data.getClerk_id());
                if (!BuildConfig.LOG_DEBUG) {
                    // 统计账户信息
                    MobclickAgent.onProfileSignIn(data.getClerk_id() + "");
                }
                // 进入首页
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                // 销毁界面
                finish();
            }
        });
    }

    /**
     * 监听输入框变化，改变按钮状态
     * @param editText 要监听的输入框
     */
    private void addTextChangeListener(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            boolean isOtherEmpty = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editText == etAccount) {
                    isOtherEmpty = TextUtils.isEmpty(etPassword.getText().toString().trim());
                } else {
                    isOtherEmpty = TextUtils.isEmpty(etAccount.getText().toString().trim());
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // 如果账号密码都不为空，登录按钮状态设置为正常；否则，设置为禁用
                if (!TextUtils.isEmpty(s.toString().trim()) && !isOtherEmpty) {
                    btnLogin.setEnabled(true);
                } else {
                    btnLogin.setEnabled(false);
                }
            }
        });
    }
}
