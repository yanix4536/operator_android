package com.zaodiandao.operator;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioButton;

import com.igexin.sdk.PushManager;
import com.socks.library.KLog;
import com.zaodiandao.operator.me.MeFragment;
import com.zaodiandao.operator.service.DemoIntentService;
import com.zaodiandao.operator.service.DemoPushService;
import com.zaodiandao.operator.shop.apply.ShopApplyFragment;
import com.zaodiandao.operator.shop.distribute.ShopDistributeFragment;
import com.zaodiandao.operator.shop.manage.OrderManagerFragment;
import com.zaodiandao.operator.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yanix on 2016/11/21.
 * 主Activity：包含4个TAB
 */
public class MainActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener{
    private static final int REQUEST_PERMISSION = 0;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    private static final int CACHED_FRAGMENT_COUNT = 3;     // 缓存的fragment数量
    // 记录第一次按后退键的时间，实现按两次退出app的功能
    private long mExitTime = 0;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    // 保存Fragment的集合
    private List<Fragment> mTabs = new ArrayList<Fragment>();
    private FragmentPagerAdapter mAdapter;

    @BindView(R.id.rb_shop_distribute)
    RadioButton rbShopDistribute;
    @BindView(R.id.rb_shop_apply)
    RadioButton rbShopApply;
    @BindView(R.id.rb_shop_manage)
    RadioButton rbOrderManage;
    @BindView(R.id.rb_me)
    RadioButton rbMe;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);

        PackageManager pkgManager = getPackageManager();

        // 读写 sd card 权限非常重要, android6.0默认禁止的, 建议初始化之前就弹窗让用户赋予该权限
        boolean sdCardWritePermission =
                pkgManager.checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName()) == PackageManager.PERMISSION_GRANTED;

        // read phone state用于获取 imei 设备信息
        boolean phoneSatePermission =
                pkgManager.checkPermission(android.Manifest.permission.READ_PHONE_STATE, getPackageName()) == PackageManager.PERMISSION_GRANTED;

        if (Build.VERSION.SDK_INT >= 23 && !sdCardWritePermission || !phoneSatePermission) {
            requestPermission2();
        } else {
            PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
        }

        // 注册 intentService 后 PushDemoReceiver 无效, sdk 会使用 DemoIntentService 传递数据,
        // AndroidManifest 对应保留一个即可(如果注册 DemoIntentService, 可以去掉 PushDemoReceiver, 如果注册了
        // IntentService, 必须在 AndroidManifest 中声明)
        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), DemoIntentService.class);

        ButterKnife.bind(this);
        // 申请定位权限
        requestPermission();
        // 初始化视图和事件
        initViewAndEvent();
        initAdapter();
    }

    private void requestPermission2() {
        ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE},
                REQUEST_PERMISSION);
    }

    private void requestPermission() {
        // 获取定位权限
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // 判断是否需要向用户解释，为什么要申请该权限
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ToastUtils.showMessage(getApplicationContext(), "获取定位权限是为了得到你当前的位置");
            } else {
                // 请求权限
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
            }
        }
    }

    private void initViewAndEvent() {
        mViewPager.setOffscreenPageLimit(CACHED_FRAGMENT_COUNT);
        mViewPager.addOnPageChangeListener(this);
        findViewById(R.id.rl_shop_distribute).setOnClickListener(this);
        findViewById(R.id.rl_shop_apply).setOnClickListener(this);
        findViewById(R.id.rl_shop_manage).setOnClickListener(this);
        findViewById(R.id.rl_me).setOnClickListener(this);
    }

    private void initAdapter() {
        ShopDistributeFragment shopDistributeFragment = new ShopDistributeFragment();
        ShopApplyFragment shopApplyFragment = new ShopApplyFragment();
        OrderManagerFragment orderManagerFragment = new OrderManagerFragment();
        MeFragment meFragment = new MeFragment();

        mTabs.add(shopDistributeFragment);
        mTabs.add(shopApplyFragment);
        mTabs.add(orderManagerFragment);
        mTabs.add(meFragment);

        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount() {
                return mTabs.size();
            }

            @Override
            public Fragment getItem(int position) {
                return mTabs.get(position);
            }
        };

        mViewPager.setAdapter(mAdapter);
        changeColor(0);
        mViewPager.setCurrentItem(0, false);
    }

    /**
     * 修改图标状态和标题
     * @param position 要高亮的图标位置
     */
    private void changeColor(int position) {
        if (position == 0) {
            rbShopDistribute.setChecked(true);
            rbShopApply.setChecked(false);
            rbOrderManage.setChecked(false);
            rbMe.setChecked(false);
            setTitle(getString(R.string.title_shop_distribute));
        } else if (position == 1) {
            rbShopDistribute.setChecked(false);
            rbShopApply.setChecked(true);
            rbOrderManage.setChecked(false);
            rbMe.setChecked(false);
            setTitle(getString(R.string.title_shop_apply));
        } else if (position == 2) {
            rbShopDistribute.setChecked(false);
            rbShopApply.setChecked(false);
            rbOrderManage.setChecked(true);
            rbMe.setChecked(false);
            setTitle(getString(R.string.title_shop_manage));
        } else if (position == 3) {
            rbShopDistribute.setChecked(false);
            rbShopApply.setChecked(false);
            rbOrderManage.setChecked(false);
            rbMe.setChecked(true);
            setTitle(getString(R.string.title_me));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_shop_distribute:
                changeColor(0);
                mViewPager.setCurrentItem(0, false);
                break;
            case R.id.rl_shop_apply:
                changeColor(1);
                mViewPager.setCurrentItem(1, false);
                break;
            case R.id.rl_shop_manage:
                changeColor(2);
                mViewPager.setCurrentItem(2, false);
                break;
            case R.id.rl_me:
                changeColor(3);
                mViewPager.setCurrentItem(3, false);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        changeColor(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                ToastUtils.showMessage(getApplicationContext(), "再按一次退出" + getString(R.string.app_name));
                mExitTime = System.currentTimeMillis();
            } else {
                finish();
                overridePendingTransition(R.anim.close_zoom_enter, R.anim.close_zoom_exit);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 动态权限请求回调
     * @param requestCode       请求码
     * @param permissions       请求的权限集合
     * @param grantResults      授权结果
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_location_permission_deny));
                }
                break;
            }
            case REQUEST_PERMISSION: {
                if ((grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
                } else {
                    KLog.e("We highly recommend that you need to grant the special permissions before initializing the SDK, otherwise some "
                            + "functions will not work");
                    PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
                }
            }
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
