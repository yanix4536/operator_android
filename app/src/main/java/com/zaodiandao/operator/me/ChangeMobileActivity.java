package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.net.ZddStringCallback;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/28.
 * 修改手机号界面
 */
public class ChangeMobileActivity extends BaseActivity {
    @BindView(R.id.et_new_mobile)
    TextInputEditText etNewMobile;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_change_mobile);
        // bind view
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addTextChangeListener(etNewMobile);
    }

    /**
     * 监听输入框变化，改变按钮状态
     * @param editText 要监听的输入框
     */
    private void addTextChangeListener(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // 如果账号密码都不为空，登录按钮状态设置为正常；否则，设置为禁用
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    btnSubmit.setEnabled(true);
                } else {
                    btnSubmit.setEnabled(false);
                }
            }
        });
    }

    @OnClick(R.id.btn_submit)
    public void submit(View view) {
        String newMobile = etNewMobile.getText().toString().trim();
        mApiService.changeMobile(requestTag, (int)SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), newMobile, new ZddStringCallback(getApplicationContext(), "message") {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                DialogHelper.showDimDialog(ChangeMobileActivity.this, "正在提交...");
            }

            @Override
            public void onAfter(int id) {
                DialogHelper.dismissDialog();
                super.onAfter(id);
            }

            @Override
            public void success(String response) {
                // 修改成功
                EventBus.getDefault().post(new MessageEvent(EventCode.CHANGE_MOBILE, "手机号修改成功!"));
                ToastUtils.showMessage(getApplicationContext(), response);
                finish();
            }
        });
    }
}
