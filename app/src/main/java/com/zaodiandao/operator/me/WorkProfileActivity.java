package com.zaodiandao.operator.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.ZDDWebViewActivity;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.me.adapter.WorkProfileAdapter;
import com.zaodiandao.operator.model.WorkProfileBean;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/22.
 * 商户开店界面
 */
public class WorkProfileActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    SwipeRefreshLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<WorkProfileBean> datas = new ArrayList<>();
    private WorkProfileAdapter adapter;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_work_profile);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mLayoutContent.setColorSchemeResources(R.color.colorPrimary);
        mLayoutContent.setOnRefreshListener(this);

        adapter = new WorkProfileAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new WorkProfileAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, WorkProfileBean data) {
                Intent intent = new Intent(WorkProfileActivity.this, ZDDWebViewActivity.class);
                intent.putExtra(ZDDWebViewActivity.TITLE, data.getTitle());
                intent.putExtra(ZDDWebViewActivity.URL, data.getArt_url());
                startActivity(intent);
            }
        });

        getData();
    }

    public void getData() {
        // 获取数据
        mApiService.actionWorkProfile(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericArrayCallback<WorkProfileBean>(getApplicationContext(), WorkProfileBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(List<WorkProfileBean> data) {
                        datas.clear();
                        datas.addAll(data);
                        adapter.notifyDataSetChanged();
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mLayoutContent.isRefreshing()) {
                            mLayoutContent.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
