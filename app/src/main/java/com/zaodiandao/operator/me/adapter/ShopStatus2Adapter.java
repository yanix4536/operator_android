package com.zaodiandao.operator.me.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.ShopApply;

import java.util.List;

public class ShopStatus2Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<ShopApply> mDatas;
    private String mTag;

    public ShopStatus2Adapter(Context context, List<ShopApply> datas, String tag) {
        mContext = context;
        mDatas = datas;
        mTag = tag;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_shop_audit, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, ShopApply data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            EmptyViewViewHolder emptyViewViewHolder = (EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无数据");
        } else {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            ShopApply data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvStatus.setText(data.getStatus_label());
            int status = data.getStatus_id();
            if ("1".equals(mTag)) {
                myViewHolder.tvName.setText("店名：" + data.getName());
                if (status == 0) {      // 未过审
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#FDB53B"));
                } else if (status == 1) {  // 已申请
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#00A02C"));
                } else if (status == 2) {  // 已审核
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#4AB1FC"));
                } else if (status == 3) {   // 已指派
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#EA2E33"));
                }
            } else {
                myViewHolder.tvName.setText("品牌名：" + data.getBrand_name());
                if (status == 0) {      // 未过审
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#FDB53B"));
                } else if (status == 1) {  // 已审核
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#4AB1FC"));
                } else if (status == 2) {  // 已申请
                    myViewHolder.tvStatus.setTextColor(Color.parseColor("#00A02C"));
                }
            }
            myViewHolder.tvBoss.setText(data.getBoss_name());
            myViewHolder.tvMobile.setText(data.getMobile());
            myViewHolder.tvAddress.setText(data.getAddress());
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (ShopApply) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvStatus, tvBoss, tvMobile, tvAddress;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvBoss = (TextView) view.findViewById(R.id.tv_boss);
            tvMobile = (TextView) view.findViewById(R.id.tv_mobile);
            tvAddress = (TextView) view.findViewById(R.id.tv_address);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}