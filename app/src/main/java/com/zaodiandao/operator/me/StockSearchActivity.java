package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.me.adapter.StockSearchAdapter;
import com.zaodiandao.operator.model.StockModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2017/2/22.
 */
public class StockSearchActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.spinner_store)
    Spinner spinnerStore;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<StockModel.StockProduct> datas = new ArrayList<>();
    private StockSearchAdapter adapter;

    private StockModel mStockModel;
    private String mStoreId = "";

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_stock_search);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        adapter = new StockSearchAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);

        // 切换仓库
        spinnerStore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String storeId = getStoreIds(mStockModel.getStore_list()).get(position);
                if (!mStoreId.equals(storeId)) {
                    mStoreId = storeId;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getData();
    }

    /**
     * 获取仓库名称集合
     */
    private List<String> getStoreNames(List<StockModel.StoreBean> stores) {
        List<String> storeNames = new ArrayList<>();
        for (StockModel.StoreBean storeBean : stores) {
            storeNames.add(storeBean.getStore_name());
        }
        return storeNames;
    }

    /**
     * 获取品牌id集合
     */
    private List<String> getStoreIds(List<StockModel.StoreBean> stores) {
        List<String> storeIds = new ArrayList<>();
        for (StockModel.StoreBean storeBean : stores) {
            storeIds.add(storeBean.getStore_id());
        }
        return storeIds;
    }

    public void getData() {
        // 获取数据
        mApiService.getStockSearch(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericCallback<StockModel>(getApplicationContext(), StockModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(StockModel data) {
                        mStockModel = data;
                        datas.clear();
                        datas.addAll(data.getProducts());
                        adapter.notifyDataSetChanged();

                        mStoreId = data.getStore_id();
                        spinnerStore.setAdapter(new ArrayAdapter<>(StockSearchActivity.this, R.layout.spinner_item, R.id.tv_name, getStoreNames(data.getStore_list())));
                        spinnerStore.setSelection(0);
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    public void requestData(final boolean isShowDialog) {
        // 获取数据
        mApiService.getStockSearch(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mStoreId,
                new GenericCallback<StockModel>(getApplicationContext(), StockModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(StockModel data) {
                        mStockModel = data;
                        datas.clear();
                        datas.addAll(data.getProducts());
                        adapter.notifyDataSetChanged();

                        List<StockModel.StoreBean> stores = data.getStore_list();
                        spinnerStore.setAdapter(new ArrayAdapter<>(StockSearchActivity.this, R.layout.spinner_item, R.id.tv_name, getStoreNames(stores)));
                        spinnerStore.setSelection(getStoreIds(stores).indexOf(mStoreId));
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        if (isShowDialog) {
                            DialogHelper.showDimDialog(StockSearchActivity.this, getString(R.string.dialog_loading_text));
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }

                        if (isShowDialog) {
                            DialogHelper.dismissDialog();
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        requestData(false);
    }
}
