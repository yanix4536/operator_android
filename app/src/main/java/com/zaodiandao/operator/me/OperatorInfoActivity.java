package com.zaodiandao.operator.me;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.igexin.sdk.PushManager;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.umeng.analytics.MobclickAgent;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.BuildConfig;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.login.LoginActivity;
import com.zaodiandao.operator.model.UpdateInfo;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.AppUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;
import com.zaodiandao.operator.view.CustemConfirmDialog;
import com.zx.uploadlibrary.listener.impl.UIProgressListener;
import com.zx.uploadlibrary.utils.OKHttpUtils;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/25.
 * 业务员信息界面
 */
public class OperatorInfoActivity extends BaseActivity {
    CustemConfirmDialog dialog;

    @BindView(R.id.tv_version)
    TextView tvVersion;

    private AppUtils mAppUtils;
    private CustemConfirmDialog mDialog;
    private ProgressDialog progressDialog;
    private static final String SAVE = "sdcard/operator.apk";

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_operator_info);
        ButterKnife.bind(this);

        mAppUtils = AppUtils.getInstance();
        tvVersion.setText("v" + mAppUtils.getVersion(getApplicationContext()));

        Intent intent = getIntent();
        setTitle(intent.getStringExtra(MeFragment.KEY_OPERATOR_NAME));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.tv_change_password)
    public void changePassword(View view) {
        startActivity(new Intent(this, ChangePasswordActivity.class));
    }

    @OnClick(R.id.tv_change_mobile)
    public void changeMobile(View view) {
        startActivity(new Intent(this, ChangeMobileActivity.class));
    }

    @OnClick(R.id.tv_feedback)
    public void feedBack(View v) {
        startActivity(new Intent(this, FeedBackActivity.class));
    }

    @OnClick(R.id.ll_check_update)
    public void checkUpdate(View view) {
        checkUpdate();
    }

    @OnClick(R.id.tv_exit_login)
    public void exitLogin(View view) {
        showConfirmDialog();
    }

    private void showConfirmDialog() {
        dialog = new CustemConfirmDialog(OperatorInfoActivity.this, R.style.QQStyle, "确定退出吗？", "退出",
        new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PushManager.getInstance().unBindAlias(getApplicationContext(), "client_id_" + (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), true);
                SPUtils.remove(getApplicationContext(), KeyConstants.OPERATOR_ID);

                if (!BuildConfig.LOG_DEBUG) {
                    // 停止统计账户信息
                    MobclickAgent.onProfileSignOff();
                }

                Intent intent = new Intent(OperatorInfoActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.reduce_in, R.anim.reduce_out);
            }
        });
        dialog.show();
    }

    private void checkUpdate() {
        DialogHelper.showDimDialog(OperatorInfoActivity.this, "正在检查更新...");
        // 获取数据
        mApiService.getUpdateInfo(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                mAppUtils.getVersion(getApplicationContext()), mAppUtils.getVersionCode(getApplicationContext()),
                new GenericCallback<UpdateInfo>(getApplicationContext(), UpdateInfo.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        ToastUtils.showMessage(getApplicationContext(), "出错了");
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        ToastUtils.showMessage(getApplicationContext(), "出错了");
                    }

                    @Override
                    public void success(final UpdateInfo data) {
                        SPUtils.put(getApplicationContext(), KeyConstants.LEVEL, data.getAuthority());
                        if ("0".equals(data.getUpdate())) {  // 没有更新
                            ToastUtils.showMessage(getApplicationContext(), "你的版本已经是最新的了");
                        } else {        // 有更新
                            mDialog = new CustemConfirmDialog(OperatorInfoActivity.this, R.style.QQStyle, data.getUpdate_mark(), "下载",
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.dismiss();
                                            download(data.getDownloadUrl());
                                        }
                                    }
                            );
                            mDialog.setCancelable(true);
                            mDialog.setCanceledOnTouchOutside(true);
                            mDialog.show();
                        }

                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }
                });
    }

    protected void download2(String url) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            showProgress();
            HttpUtils httpUtils = new HttpUtils();
            httpUtils.download(url, SAVE, new RequestCallBack<File>() {
                @Override
                public void onSuccess(ResponseInfo<File> responseInfo) {
                    progressDialog.dismiss();
                    try {
                        install();
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(HttpException e, String s) {
                    progressDialog.dismiss();
                    ToastUtils.showMessage(getApplicationContext(), "新版本下载失败");
                }

                public void onLoading(long total, long current, boolean isUploding) {
                    try {
                        super.onLoading(total, current, isUploding);
                        progressDialog.setMax((int) total);
                        progressDialog.setProgress((int) current);
                        float all = total / 1024.0f / 1024.0f;
                        float percent = current / 1024.0f / 1024.0f;
                        progressDialog.setProgressNumberFormat(String.format("%.2fM/%.2fM", percent, all));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            ToastUtils.showMessage(getApplicationContext(), "没有发现可用的SD卡");
        }
    }

    private void download(String url) {
        //这个是ui线程回调，可直接操作UI
        final UIProgressListener uiProgressResponseListener = new UIProgressListener() {
            @Override
            public void onUIProgress(long bytesRead, long contentLength, boolean done) {
                Log.i("TAG", "bytesRead:" + bytesRead);
                Log.i("TAG", "contentLength:" + contentLength);
                Log.i("TAG", "done:" + done);
                if (contentLength != -1) {
                    //长度未知的情况下回返回-1
                    Log.i("TAG", (100 * bytesRead) / contentLength + "% done");
                }
                Log.i("TAG", "================================");
                progressDialog.setMax((int) contentLength);
                progressDialog.setProgress((int) bytesRead);
                // 格式化下载进度，以MB为单位
                float all = contentLength / 1024.0f / 1024.0f;
                float percent = bytesRead / 1024.0f / 1024.0f;
                progressDialog.setProgressNumberFormat(String.format("%.2fM/%.2fM", percent, all));
            }

            @Override
            public void onUIStart(long bytesRead, long contentLength, boolean done) {
                super.onUIStart(bytesRead, contentLength, done);
                ToastUtils.showMessage(getApplicationContext(), "开始下载");
            }

            @Override
            public void onUIFinish(long bytesRead, long contentLength, boolean done) {
                super.onUIFinish(bytesRead, contentLength, done);
                progressDialog.dismiss();
                if (!done) {
                    // 下载失败
                    ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_download_fail));
                } else {
                    // 下载完成
                    try {
                        // 安装新版本
                        install();
                        // 退出界面
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // 显示进度框
            showProgress();
            //开启文件下载
            OKHttpUtils.downloadandSaveFile(url, SAVE, uiProgressResponseListener);
        } else {
            // 没有SDCard
            ToastUtils.showMessage(getApplicationContext(), getString(R.string.error_no_sdcard));
        }
    }

    private void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("下载进度");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected void install() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        // 添加flag
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.fromFile(new File(SAVE)), "application/vnd.android.package-archive");
        startActivity(intent);
    }
}
