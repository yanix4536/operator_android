package com.zaodiandao.operator.me;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.model.ClassItem;
import com.zaodiandao.operator.model.ProductClassBean;
import com.zaodiandao.operator.net.GenericArrayCallback;
import com.zaodiandao.operator.net.ZddStringCallback;
import com.zaodiandao.operator.util.AnimUtils;
import com.zaodiandao.operator.util.DensityUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;
import com.zaodiandao.operator.util.commonadapter.CommonAdapter;
import com.zaodiandao.operator.util.commonadapter.ViewHolder;
import com.zaodiandao.operator.view.CustemConfirmDialog;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2017/1/6.
 * 订货页面
 */
public class PurchaseActivity extends BaseActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public static final String KEY_ESTIMATE_ID = "estimate_id";

    @BindView(R.id.ll_content)
    RelativeLayout llContent;
    @BindView(R.id.pb_loading)
    View pbloading;
    @BindView(R.id.tv_retry)
    TextView tvFail;
    @BindView(R.id.lv_class)
    ListView lvClass;
    @BindView(R.id.lv_product)
    ListView lvProduct;
    @BindView(R.id.tv_ok)
    TextView tvOk;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.ll_cart)
    LinearLayout llCart;
    @BindView(R.id.iv_cart)
    ImageView ivCart;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;
    // 辅助购物车显示的view
    @BindView(R.id.black_view)
    View blackView;

    ImageView imageView;

    // 类别、菜品、类别菜品、临时购物车、最终购物车集合
    public List<ClassItem> classes = new ArrayList<>();
    public List<ProductClassBean.ProductBean> productInfos = new ArrayList<>();
    public List<ProductClassBean> productClassInfos = new ArrayList<>();
    public List<ProductClassBean.ProductBean> cart = new ArrayList<>();
    public List<ProductClassBean.ProductBean> realCart = new ArrayList<>();

    // 类别列表、菜品列表、购物车列表适配器
    public static CommonAdapter<ClassItem> classAdapter;
    private CommonAdapter<ProductClassBean.ProductBean> productAdapter;
    public static CommonAdapter<ProductClassBean.ProductBean> cartAdapter;

    private String selectedClassName = "";
    private boolean isShowCart = true;
    private PopupWindow popWindow;
    private CustemConfirmDialog dialog;

    private NumberReceiver numberReceiver;
    private AnimUtils animUtils;

    private String estimateId = "";

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_purchase);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(this);

        tvFail.setOnClickListener(this);
        tvOk.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llCart.setOnClickListener(this);

        blackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        animUtils = new AnimUtils(this, ivCart);

        classAdapter = new CommonAdapter<ClassItem>(this, classes, R.layout.listitem_class) {

            @Override
            public void convert(ViewHolder helper, final ClassItem item) {
                TextView tvClassName = helper.getView(R.id.tv_class_name);
                tvClassName.setText(item.getClassName());

                View indicator = helper.getView(R.id.indicator);

                if (helper.getPosition() == selectPosition) {
                    indicator.setVisibility(View.VISIBLE);
                    tvClassName.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    indicator.setVisibility(View.INVISIBLE);
                    tvClassName.setTextColor(getResources().getColor(R.color.text_middle));
                }

                TextView tvCount = helper.getView(R.id.tv_count);
                int totalCount = item.getNumber();
                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.GONE);
                }
            }
        };
        lvClass.setAdapter(classAdapter);
        lvClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedClassName = classes.get(position).getClassName();

                classAdapter.setSelectPosition(position);
                classAdapter.notifyDataSetInvalidated();

                productInfos.clear();
                productInfos.addAll(productClassInfos.get(position).getProducts());
                productAdapter.notifyDataSetChanged();
            }
        });

        productAdapter = new CommonAdapter<ProductClassBean.ProductBean>(this, productInfos, R.layout.listitem_product) {

            @Override
            public void convert(final ViewHolder helper, final ProductClassBean.ProductBean item) {
                ImageView ivProduct = helper.getView(R.id.iv_product);
                String path = item.getImage();
                if (!TextUtils.isEmpty(path)) {
                    Picasso.with(getApplicationContext()).load(item.getImage())
                            .resize(DensityUtils.dp2px(getApplicationContext(), 70), DensityUtils.dp2px(getApplicationContext(), 70))
                            .placeholder(R.mipmap.img_default).into(ivProduct);
                } else {
                    ivProduct.setImageResource(R.mipmap.img_default);
                }

                RelativeLayout rlAdded = helper.getView(R.id.rl_added);
                TextView tvReplenishment = helper.getView(R.id.tv_replenishment);

                int status = item.getStatus();
                if (status == 1) {
                    rlAdded.setVisibility(View.VISIBLE);
                    tvReplenishment.setVisibility(View.GONE);
                } else if (status == 3) {
                    rlAdded.setVisibility(View.GONE);
                    tvReplenishment.setVisibility(View.VISIBLE);
                }

                helper.setText(R.id.tv_name, item.getName());
                helper.setText(R.id.tv_format, item.getFormat());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);

                final ImageView ivReduce = helper.getView(R.id.iv_reduce);
                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setAmount(item.getAmount() - 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() - 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getAmount() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        sendBroadcast(intent);
                    }
                });

                if (item.getAmount() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(item.getAmount() + "");
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setAmount(item.getAmount() + 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() + 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getAmount() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            sendBroadcast(intent);
                        }

                        int[] start_location = new int[2];// 一个整型数组，用来存储按钮的在屏幕的X、Y坐标
                        v.getLocationInWindow(start_location);// 这是获取购买按钮的在屏幕的X、Y坐标（这也是动画开始的坐标）
                        imageView = new ImageView(getApplicationContext());
                        imageView.setImageBitmap(animUtils.getAddDrawBitMap());// 设置buyImg的图片
                        animUtils.setAnim(imageView, start_location, null);// 开始执行动画
                    }
                });
            }
        };
        lvProduct.setAdapter(productAdapter);

        registerReceiver();

        estimateId = getIntent().getStringExtra(KEY_ESTIMATE_ID);
        fetchData();
    }

    private void fetchData(){
        if (TextUtils.isEmpty(estimateId) || "0".equals(estimateId)){
            getProduct();
        } else {
            getProductWithEstimateId();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (popWindow != null && popWindow.isShowing()) {
            popWindow.dismiss();
            blackView.setVisibility(View.GONE);
            isShowCart = true;
        }
    }

    private void show(View v, final View clickView, final View endView, final ProductClassBean.ProductBean item) {
        int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, -720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                endView.setVisibility(View.VISIBLE);

                Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                intent.putExtra("info", item);
                intent.putExtra("type", KeyConstants.ADD);
                sendBroadcast(intent);

                if (cartAdapter != null) {
                    cartAdapter.notifyDataSetChanged();
                }

                clickView.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void hide(final View v, View endView) {
        final int[] startPos = new int[2];
        v.getLocationOnScreen(startPos);

        final int[] endPos = new int[2];
        endView.getLocationOnScreen(endPos);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setDuration(300);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, endPos[0] - startPos[0], 0, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(300);
        //animationSet.addAnimation(rotateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        animationSet.addAnimation(alphaAnimation);
        v.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void registerReceiver() {
        // 注册广播接收
        numberReceiver = new NumberReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(KeyConstants.ACTION_CHANGE);
        filter.addAction(KeyConstants.ACTION_CART_CLEAR);
        filter.addAction(KeyConstants.ACTION_REFRESH);
        registerReceiver(numberReceiver, filter);
    }

    @Override
    public void onRefresh() {
        fetchData();
        clearCart();
    }

    private int totalCount = 0;

    private class NumberReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(KeyConstants.ACTION_CHANGE)) { // 改变数量
                productAdapter.notifyDataSetChanged();
                ProductClassBean.ProductBean info = (ProductClassBean.ProductBean) intent.getSerializableExtra("info");
                int type = intent.getIntExtra("type", 0);

                boolean flag = true;
                for (ProductClassBean.ProductBean cartInfo : cart) {
                    if (cartInfo.getPid().equals(info.getPid())) {
                        cartInfo.setAmount(info.getAmount());
                        flag = false;
                        break;
                    }
                }

                if (flag) {
                    cart.add(info);
                }

                totalCount = Integer.parseInt(tvCount.getText().toString());

                if (type == KeyConstants.ADD) {
                    totalCount++;
                } else {
                    totalCount--;
                }

                tvCount.setTextSize(12);
                if (totalCount > 0) {
                    if (totalCount > 999) {
                        tvCount.setTextSize(6);
                    } else if (totalCount > 99) {
                        tvCount.setTextSize(8);
                    } else if (totalCount > 9) {
                        tvCount.setTextSize(10);
                    }
                    tvOk.setEnabled(true);
                    tvCount.setText(totalCount + "");
                    tvCount.setVisibility(View.VISIBLE);
                } else {
                    tvOk.setEnabled(false);
                    tvCount.setText("0");
                    tvCount.setVisibility(View.GONE);
                }
            } else if (action.equals(KeyConstants.ACTION_CART_CLEAR)) {     // 清空
                tvCount.setText("0");
                tvCount.setVisibility(View.GONE);
                tvOk.setEnabled(false);
                totalCount = 0;
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
                cart.clear();

                for (ProductClassBean productClassInfo : productClassInfos) {
                    for (ProductClassBean.ProductBean info : productClassInfo.getProducts()) {
                        info.setAmount(0);
                    }
                }

                for (ClassItem classItem : classes) {
                    classItem.setNumber(0);
                }
                productAdapter.notifyDataSetChanged();
                classAdapter.notifyDataSetChanged();
            } else if (action.equals(KeyConstants.ACTION_REFRESH)) {
                fetchData();
                clearCart();
            }
        }
    }


    /**
     * 获取菜品数据
     */
    private void getProduct() {
        mApiService.getProductList(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericArrayCallback<ProductClassBean>(getApplicationContext(), ProductClassBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (refreshLayout.isRefreshing()) {
                            refreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void success(List<ProductClassBean> data) {
                        productClassInfos.clear();
                        productClassInfos.addAll(data);

                        if (productClassInfos.size() <= 0) {
                            ToastUtils.showMessage(getApplicationContext(), "沒有数据");
                            classes.clear();
                            productInfos.clear();
                            classAdapter.notifyDataSetChanged();
                            productAdapter.notifyDataSetChanged();
                            if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                                AnimUtils.visible(llContent);
                            } else {
                                llContent.setVisibility(View.VISIBLE);
                            }
                            AnimUtils.gone(pbloading);
                            AnimUtils.gone(tvFail);
                            return;
                        }

                        classes.clear();
                        for (int i = 0; i < productClassInfos.size(); i++) {
                            ProductClassBean productClassInfo = productClassInfos.get(i);
                            ClassItem classItem = new ClassItem();

                            int sum = 0;

                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                productInfo.setPosition(i);
                                if (productInfo.getAmount() > 0) {
                                    sum += productInfo.getAmount();
                                    cart.add(productInfo);
                                }
                            }

                            totalCount += sum;

                            classItem.setNumber(sum);
                            classItem.setClassName(productClassInfo.getMenutype());
                            classes.add(classItem);
                        }

                        tvCount.setTextSize(12);
                        if (totalCount > 0) {
                            if (totalCount > 999) {
                                tvCount.setTextSize(6);
                            } else if (totalCount > 99) {
                                tvCount.setTextSize(8);
                            } else if (totalCount > 9) {
                                tvCount.setTextSize(10);
                            }
                            tvOk.setEnabled(true);
                            tvCount.setText(totalCount + "");
                            tvCount.setVisibility(View.VISIBLE);
                        } else {
                            tvOk.setEnabled(false);
                            tvCount.setText("0");
                            tvCount.setVisibility(View.GONE);
                        }

                        int selectedClassIndex = getClassIndex(selectedClassName);
                        classAdapter.setSelectPosition(selectedClassIndex);
                        classAdapter.notifyDataSetChanged();

                        productInfos.clear();
                        productInfos.addAll(productClassInfos.get(selectedClassIndex).getProducts());

                        productAdapter.notifyDataSetChanged();

                        if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                            AnimUtils.visible(llContent);
                        } else {
                            llContent.setVisibility(View.VISIBLE);
                        }
                        AnimUtils.gone(pbloading);
                        AnimUtils.gone(tvFail);
                    }
                });
    }

    /**
     * 获取菜品数据
     */
    private void getProductWithEstimateId() {
        mApiService.getProductList(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), estimateId,
                new GenericArrayCallback<ProductClassBean>(getApplicationContext(), ProductClassBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() == View.GONE) {
                            AnimUtils.crossfade(tvFail, pbloading);
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        if (refreshLayout.isRefreshing()) {
                            refreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void success(List<ProductClassBean> data) {
                        productClassInfos.clear();
                        productClassInfos.addAll(data);

                        if (productClassInfos.size() <= 0) {
                            ToastUtils.showMessage(getApplicationContext(), "沒有数据");
                            classes.clear();
                            productInfos.clear();
                            classAdapter.notifyDataSetChanged();
                            productAdapter.notifyDataSetChanged();
                            if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                                AnimUtils.visible(llContent);
                            } else {
                                llContent.setVisibility(View.VISIBLE);
                            }
                            AnimUtils.gone(pbloading);
                            AnimUtils.gone(tvFail);
                            return;
                        }

                        classes.clear();
                        for (int i = 0; i < productClassInfos.size(); i++) {
                            ProductClassBean productClassInfo = productClassInfos.get(i);
                            ClassItem classItem = new ClassItem();

                            int sum = 0;

                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                productInfo.setPosition(i);
                                if (productInfo.getAmount() > 0) {
                                    sum += productInfo.getAmount();
                                    cart.add(productInfo);
                                }
                            }

                            totalCount += sum;

                            classItem.setNumber(sum);
                            classItem.setClassName(productClassInfo.getMenutype());
                            classes.add(classItem);
                        }

                        tvCount.setTextSize(12);
                        if (totalCount > 0) {
                            if (totalCount > 999) {
                                tvCount.setTextSize(6);
                            } else if (totalCount > 99) {
                                tvCount.setTextSize(8);
                            } else if (totalCount > 9) {
                                tvCount.setTextSize(10);
                            }
                            tvOk.setEnabled(true);
                            tvCount.setText(totalCount + "");
                            tvCount.setVisibility(View.VISIBLE);
                        } else {
                            tvOk.setEnabled(false);
                            tvCount.setText("0");
                            tvCount.setVisibility(View.GONE);
                        }


                        int selectedClassIndex = getClassIndex(selectedClassName);
                        classAdapter.setSelectPosition(selectedClassIndex);
                        classAdapter.notifyDataSetChanged();

                        productInfos.clear();
                        productInfos.addAll(productClassInfos.get(selectedClassIndex).getProducts());

                        productAdapter.notifyDataSetChanged();

                        if (pbloading.getVisibility() == View.VISIBLE || tvFail.getVisibility() == View.VISIBLE) {
                            AnimUtils.visible(llContent);
                        } else {
                            llContent.setVisibility(View.VISIBLE);
                        }
                        AnimUtils.gone(pbloading);
                        AnimUtils.gone(tvFail);
                    }
                });
    }

    private int getClassIndex(String selectedClassName) {
        for (int i = 0; i < classes.size(); i++) {
            if (classes.get(i).getClassName().equals(selectedClassName)) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_retry:
                pbloading.setVisibility(View.VISIBLE);
                AnimUtils.gone(tvFail);
                fetchData();
                break;
            case R.id.iv_cart:
            case R.id.ll_cart:
                getCart();
                if (isShowCart) {
                    // 判断购物车是否为空
                    if (isCartEmpty()) {
                        ToastUtils.showMessage(getApplicationContext(), getString(R.string.cart_empty));
                    } else {
                        showPop(ivCart);
                        isShowCart = false;
                    }
                } else {
                    if (popWindow != null && popWindow.isShowing()) {
                        popWindow.dismiss();
                        blackView.setVisibility(View.GONE);
                        isShowCart = true;
                    }
                }
                break;
            case R.id.tv_ok:  // 下单
                getCart();
                submitOrder();
                break;
        }
    }

    private void submitOrder() {
        JSONArray jsonArray = new JSONArray();
        for (ProductClassBean.ProductBean cartInfo : realCart) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pid", cartInfo.getPid());
                jsonObject.put("amount", cartInfo.getAmount());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }

        mApiService.savePurchaseProduct(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), estimateId, jsonArray.toString(),
                new ZddStringCallback(getApplicationContext(), "estimate_id") {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(PurchaseActivity.this, "正在提交...");
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        DialogHelper.dismissDialog();
                    }

                    @Override
                    public void success(String data) {
                        ToastUtils.showMessage(getApplicationContext(), "提交成功");
                        EventBus.getDefault().post(new MessageEvent(EventCode.REFRESH_PRODUCT, "刷新采购产品!"));
                        finish();
                    }
                });
    }

    /**
     * 显示购物车的详情
     */
    private void showPop(View temp) {
        View v = getLayoutInflater().inflate(R.layout.cart_popwindow, null);
        LinearLayout llCart = (LinearLayout) v.findViewById(R.id.ll_cart);
        int[] location = new int[2];
        temp.getLocationOnScreen(location);
        popWindow = new PopupWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ListView menuList = (ListView) v.findViewById(R.id.card_food_list);
        cartAdapter = new CommonAdapter<ProductClassBean.ProductBean>(getApplicationContext(), realCart, R.layout.listitem_cart) {

            @Override
            public void convert(ViewHolder helper, final ProductClassBean.ProductBean item) {
                helper.setText(R.id.tv_format, item.getFormat());
                helper.setText(R.id.tv_name, item.getName());

                final TextView tvCount = helper.getView(R.id.tv_count);
                final ImageView ivAnimReduce = helper.getView(R.id.iv_anim_reduce);
                final ImageView ivAdd = helper.getView(R.id.iv_add);

                final ImageView ivReduce = helper.getView(R.id.iv_reduce);
                ivReduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductClassBean productClassInfo : productClassInfos) {
                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    productInfo.setAmount(productInfo.getAmount() - 1);
                                }
                            }
                        }

                        item.setAmount(item.getAmount() - 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() - 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getAmount() == 0) {
                            ivReduce.setEnabled(false);
                            hide(ivReduce, ivAnimReduce);
                        }

                        Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                        intent.putExtra("info", item);
                        intent.putExtra("type", KeyConstants.REDUCE);
                        sendBroadcast(intent);

                        cartAdapter.notifyDataSetChanged();
                    }
                });

                if (item.getAmount() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(item.getAmount() + "");
                    ivReduce.setVisibility(View.VISIBLE);
                } else {
                    tvCount.setVisibility(View.INVISIBLE);
                    ivReduce.setVisibility(View.INVISIBLE);
                }

                ivAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (ProductClassBean productClassInfo : productClassInfos) {
                            for (ProductClassBean.ProductBean productInfo : productClassInfo.getProducts()) {
                                if (item.getPid().equals(productInfo.getPid())) {
                                    productInfo.setAmount(productInfo.getAmount() + 1);
                                }
                            }
                        }

                        item.setAmount(item.getAmount() + 1);

                        classes.get(item.getPosition()).setNumber(classes.get(item.getPosition()).getNumber() + 1);
                        classAdapter.notifyDataSetChanged();

                        if (item.getAmount() == 1) {
                            ivAdd.setEnabled(false);
                            show(ivAnimReduce, ivAdd, ivReduce, item);
                        } else {
                            Intent intent = new Intent(KeyConstants.ACTION_CHANGE);
                            intent.putExtra("info", item);
                            intent.putExtra("type", KeyConstants.ADD);
                            sendBroadcast(intent);
                            cartAdapter.notifyDataSetInvalidated();
                        }
                    }
                });
            }
        };
        menuList.setAdapter(cartAdapter);
        popWindow.showAtLocation(temp, Gravity.BOTTOM, 0, DensityUtils.dp2px(getApplicationContext(), 40));
        blackView.setVisibility(View.VISIBLE);

        llCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popWindow != null && popWindow.isShowing()) {
                    popWindow.dismiss();
                    blackView.setVisibility(View.GONE);
                    isShowCart = true;
                }
            }
        });

        v.findViewById(R.id.tv_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog == null) {
                    dialog = new CustemConfirmDialog(PurchaseActivity.this, R.style.QQStyle, "确定清空购物车吗？"
                            , "清空",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    clearCart();
                                    dialog.dismiss();
                                }
                            });
                }
                dialog.show();
            }
        });
    }

    /**
     * 获取购物车的内容
     */
    public void getCart() {
        realCart.clear();
        for (ProductClassBean.ProductBean info : cart) {
            if (info.getAmount() > 0) {
                realCart.add(info);
            }
        }
    }


    /**
     * 判断购物车是否为空
     */
    public boolean isCartEmpty() {
        return realCart.size() <= 0;
    }

    /**
     * 清空购物车
     */
    private void clearCart() {
        // 清空列表
        Intent intent = new Intent();
        intent.setAction(KeyConstants.ACTION_CART_CLEAR);
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        classAdapter = null;
        cartAdapter = null;

        EventBus.getDefault().unregister(this);
        // 取消广播
        try {
            unregisterReceiver(numberReceiver);
        } catch (Exception e) {

        }
    }

}
