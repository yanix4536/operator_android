package com.zaodiandao.operator.me.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.DeductModel;

import java.util.List;

public class DeductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<DeductModel.OrderBean> mDatas;

    public DeductAdapter(Context context, List<DeductModel.OrderBean> datas) {
        mContext = context;
        mDatas = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_deduct, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, DeductModel.OrderBean data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            EmptyViewViewHolder emptyViewViewHolder = (EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无数据");
        } else {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            final DeductModel.OrderBean data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvOrderNumber.setText(data.getOrdernumber());
            myViewHolder.tvCheckTime.setText(data.getCheckout_time());
            myViewHolder.tvFoodMoney.setText("￥" + data.getSalesman_money());
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (DeductModel.OrderBean) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvOrderNumber, tvCheckTime, tvFoodMoney;

        public MyViewHolder(View view) {
            super(view);
            tvOrderNumber = (TextView) view.findViewById(R.id.tv_order_number);
            tvCheckTime = (TextView) view.findViewById(R.id.tv_check_time);
            tvFoodMoney = (TextView) view.findViewById(R.id.tv_food_money);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}