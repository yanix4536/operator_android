package com.zaodiandao.operator.me.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.model.SaleStatisticsModel;
import com.zaodiandao.operator.model.StockModel;

import java.util.List;

public class SaleStatisticsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<SaleStatisticsModel.SaleProduct> mDatas;

    public SaleStatisticsAdapter(Context context, List<SaleStatisticsModel.SaleProduct> datas) {
        mContext = context;
        mDatas = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_sale_statistics, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, StockModel.StockProduct data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            EmptyViewViewHolder emptyViewViewHolder = (EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无数据");
        } else {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            final SaleStatisticsModel.SaleProduct data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvProductName.setText("产品：" + data.getProduct_name());
            myViewHolder.tvFormat.setText(data.getFormat());
            myViewHolder.tvSaleAmount.setText(data.getSell_amount() + "（" + data.getUnit() + "）");
            myViewHolder.tvMoney.setText("￥" + data.getSell_money());
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (StockModel.StockProduct) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductName, tvFormat, tvSaleAmount, tvMoney;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = (TextView) view.findViewById(R.id.tv_product_name);
            tvFormat = (TextView) view.findViewById(R.id.tv_format);
            tvSaleAmount = (TextView) view.findViewById(R.id.tv_sale_amount);
            tvMoney = (TextView) view.findViewById(R.id.tv_money);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}