package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.me.adapter.PurchaseProductAdapter;
import com.zaodiandao.operator.model.PurchaseDetailModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/2/22.
 */
public class PurchaseDetailActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    public static final String KEY_ESTIMATE_ID = "estimate_id";
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<PurchaseDetailModel.PurchaseProductBean> datas = new ArrayList<>();
    private PurchaseProductAdapter adapter;
    private String estimateId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_purchase_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        adapter = new PurchaseProductAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);

        estimateId = getIntent().getStringExtra(KEY_ESTIMATE_ID);

        getData();
    }

    public void getData() {
        // 获取数据
        mApiService.getPurchaseDetail(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), estimateId,
                new GenericCallback<PurchaseDetailModel>(getApplicationContext(), PurchaseDetailModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(PurchaseDetailModel data) {
                        datas.clear();
                        datas.addAll(data.getProducts());
                        adapter.notifyDataSetChanged();
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        getData();
    }

}
