package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.me.adapter.SaleStatisticsAdapter;
import com.zaodiandao.operator.model.SaleStatisticsModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2017/2/22.
 */
public class SaleStatisticsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;
    @BindView(R.id.spinner_brand)
    Spinner spinnerBrand;
    @BindView(R.id.spinner_month)
    Spinner spinnerMonth;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<SaleStatisticsModel.SaleProduct> datas = new ArrayList<>();
    private SaleStatisticsAdapter adapter;

    private SaleStatisticsModel mSaleStatisticsModel;
    private String mBrandId = "", mDate = "";

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_sale_statistics);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        adapter = new SaleStatisticsAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);

        // 切换品牌
        spinnerBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String brandId = getBrandIds(mSaleStatisticsModel.getBrands()).get(position);
                if (!mBrandId.equals(brandId)) {
                    mBrandId = brandId;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // 切换月份
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String month = mSaleStatisticsModel.getMonths().get(position);
                if (!mDate.equals(month)) {
                    mDate = month;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getData();
    }

    /**
     * 获取品牌名称集合
     */
    private List<String> getBrandNames(List<SaleStatisticsModel.BrandBean> brands) {
        List<String> brandNames = new ArrayList<>();
        for (SaleStatisticsModel.BrandBean brandBean : brands) {
            brandNames.add(brandBean.getBrand_name());
        }
        return brandNames;
    }

    /**
     * 获取品牌id集合
     */
    private List<String> getBrandIds(List<SaleStatisticsModel.BrandBean> brands) {
        List<String> brandIds = new ArrayList<>();
        for (SaleStatisticsModel.BrandBean brandBean : brands) {
            brandIds.add(brandBean.getBrand_id());
        }
        return brandIds;
    }

    public void getData() {
        // 获取数据
        mApiService.getSaleStatistics(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericCallback<SaleStatisticsModel>(getApplicationContext(), SaleStatisticsModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(SaleStatisticsModel data) {
                        mSaleStatisticsModel = data;
                        datas.clear();
                        datas.addAll(data.getPdata());
                        adapter.notifyDataSetChanged();
                        // 初始化spinner
                        List<SaleStatisticsModel.BrandBean> brands = data.getBrands();
                        List<String> months = data.getMonths();
                        mBrandId = brands.get(0).getBrand_id();
                        mDate = months.get(0);
                        spinnerBrand.setAdapter(new ArrayAdapter<>(SaleStatisticsActivity.this, R.layout.spinner_item, R.id.tv_name, getBrandNames(brands)));
                        spinnerMonth.setAdapter(new ArrayAdapter<>(SaleStatisticsActivity.this, R.layout.spinner_item, R.id.tv_name, months));
                        spinnerBrand.setSelection(0);
                        spinnerMonth.setSelection(0);

                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    public void requestData(final boolean isShowDialog) {
        // 获取数据
        mApiService.getSaleStatistics(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mBrandId, mDate,
                new GenericCallback<SaleStatisticsModel>(getApplicationContext(), SaleStatisticsModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(SaleStatisticsModel data) {
                        mSaleStatisticsModel = data;
                        datas.clear();
                        datas.addAll(data.getPdata());
                        adapter.notifyDataSetChanged();

                        // 初始化spinner
                        List<SaleStatisticsModel.BrandBean> brands = data.getBrands();
                        List<String> months = data.getMonths();
                        spinnerBrand.setAdapter(new ArrayAdapter<>(SaleStatisticsActivity.this, R.layout.spinner_item, R.id.tv_name, getBrandNames(brands)));
                        spinnerMonth.setAdapter(new ArrayAdapter<>(SaleStatisticsActivity.this, R.layout.spinner_item, R.id.tv_name, months));
                        int brandIndex = getBrandIds(brands).indexOf(mBrandId);
                        spinnerBrand.setSelection(brandIndex);
                        int monthIndex = months.indexOf(mDate);
                        spinnerMonth.setSelection(monthIndex);

                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        if (isShowDialog) {
                            DialogHelper.showDimDialog(SaleStatisticsActivity.this, getString(R.string.dialog_loading_text));
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }

                        if (isShowDialog) {
                            DialogHelper.dismissDialog();
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        requestData(false);
    }
}
