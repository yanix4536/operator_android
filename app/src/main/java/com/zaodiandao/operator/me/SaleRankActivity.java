package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.socks.library.KLog;
import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.model.HistorySale;
import com.zaodiandao.operator.model.SaleRank;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.SPUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static com.zaodiandao.operator.me.MeFragment.KEY_BRAND_ID;
import static com.zaodiandao.operator.me.MeFragment.KEY_DATE;

/**
 * Created by yanix on 2016/11/28.
 * 业务员历史销售数据
 */
public class SaleRankActivity extends BaseActivity {
    @BindView(R.id.tv_sale_rank_title)
    TextView mTvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    private String mBrandId;
    private String mDate;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_sale_rank);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mBrandId = getIntent().getStringExtra(KEY_BRAND_ID);
        mDate = getIntent().getStringExtra(KEY_DATE);
        getData();
    }

    private void getData(){
        // 获取数据
        mApiService.saleRank(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mBrandId, mDate,
                new GenericCallback<SaleRank>(getApplicationContext(), SaleRank.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(SaleRank data) {
                        mTvTitle.setText(data.getMonth() + data.getBrandname());
                        mRecyclerView.setAdapter(new SaleRankAdapter(data.getSaledata()));
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    class SaleRankAdapter extends RecyclerView.Adapter<SaleRankAdapter.MyViewHolder> {
        private List<SaleRank.SaledataBean> mSaleDatas;

        public SaleRankAdapter(List<SaleRank.SaledataBean> saleDatas) {
            mSaleDatas = saleDatas;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(getApplicationContext()).inflate(R.layout.listitem_history_sale, parent, false));
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            SaleRank.SaledataBean data = mSaleDatas.get(position);
            holder.tvMonth.setText(data.getSalesname());
            holder.tvSaleAmount.setText(data.getSalesamount());
            holder.tvPayment.setText(data.getPayment()+"");
            holder.tvRiseAmount.setText(data.getRiseamount());
        }

        @Override
        public int getItemCount() {
            return mSaleDatas == null ? 0 : mSaleDatas.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvMonth, tvSaleAmount, tvPayment, tvRiseAmount;

            public MyViewHolder(View view) {
                super(view);
                tvMonth = (TextView) view.findViewById(R.id.tv_month);
                tvSaleAmount = (TextView) view.findViewById(R.id.tv_sale_amount);
                tvPayment = (TextView) view.findViewById(R.id.tv_payment);
                tvRiseAmount = (TextView) view.findViewById(R.id.tv_rise_amount);
            }
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }
}
