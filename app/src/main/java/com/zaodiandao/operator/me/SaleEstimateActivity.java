package com.zaodiandao.operator.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.me.adapter.SaleEstimateAdapter;
import com.zaodiandao.operator.model.SaleEstimateBean;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2017/2/22.
 */
public class SaleEstimateActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<SaleEstimateBean.EstimateItem> datas = new ArrayList<>();
    private SaleEstimateAdapter adapter;

    private int number;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_sale_estimate);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventBus.getDefault().register(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        adapter = new SaleEstimateAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new SaleEstimateAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, SaleEstimateBean.EstimateItem data) {
                Intent intent = new Intent(SaleEstimateActivity.this, PurchaseDetailActivity.class);
                intent.putExtra(PurchaseDetailActivity.KEY_ESTIMATE_ID, data.getEstimate_id());
                startActivity(intent);
            }
        });
        getData();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_estimate, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.estimate:
                if (number > 0) {
                    ToastUtils.showMessage(getApplicationContext(), "当前还有未审核的采购单，无法继续采购");
                    return true;
                }
                startActivity(new Intent(this, PurchaseActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getData() {
        // 获取数据
        mApiService.getEstimateList(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericCallback<SaleEstimateBean>(getApplicationContext(), SaleEstimateBean.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(SaleEstimateBean data) {
                        number = data.getE_no();
                        datas.clear();
                        datas.addAll(data.getE_list());
                        adapter.notifyDataSetChanged();
                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void onRefresh() {
        getData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.code == EventCode.REFRESH_PRODUCT)
            getData();
    }

}
