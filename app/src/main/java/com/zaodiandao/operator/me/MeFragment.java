package com.zaodiandao.operator.me;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zaodiandao.operator.BaseFragment;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.ZDDWebViewActivity;
import com.zaodiandao.operator.config.EventCode;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.event.MessageEvent;
import com.zaodiandao.operator.model.OperatorInfo3;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by yanix on 2016/11/22.
 * 个人信息界面
 */
public class MeFragment extends BaseFragment {
    public static final String KEY_OPERATOR_NAME = "key_operator_name";
    public static final String KEY_BRAND_ID = "key_brand_id";
    public static final String KEY_DATE = "key_date";
    private String mOperatorName;

    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mLayoutContent;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.iv_avatar)
    ImageView mIvAvatar;
    @BindView(R.id.tv_name)
    TextView mTvName;
    @BindView(R.id.tv_mobile)
    TextView mTvMobile;

    @BindView(R.id.tv_open_num)
    TextView tvOpenNum;
    @BindView(R.id.tv_openable_num)
    TextView tvOpenableNum;
    @BindView(R.id.tv_sale_amount)
    TextView tvSaleAmount;
    @BindView(R.id.tv_deduct_factor)
    TextView tvDeductFactor;
    @BindView(R.id.tv_deduct_estimate)
    TextView tvDeductEstimate;
    @BindView(R.id.tv_deduct_factor_next_month)
    TextView tvDeductFactorNextMonth;
    @BindView(R.id.tv_next_factor)
    TextView tvNextFactor;
    @BindView(R.id.tv_difference)
    TextView tvDifference;
    @BindView(R.id.line)
    View line;
    @BindView(R.id.ll_next_factor)
    LinearLayout llNextFactor;
    @BindView(R.id.tv_deduct_explain)
    TextView tvDeductExplain;
    @BindView(R.id.spinner_city)
    Spinner spinnerCity;
    @BindView(R.id.ll_hide)
    LinearLayout llHide;
    @BindView(R.id.v_hide)
    View vHide;
    @BindView(R.id.sv_hide)
    ScrollView svHide;

    private String mCityId;
    private OperatorInfo3 mOperatorInfo3;

    View root;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View getView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_me, null);
        ButterKnife.bind(this, root);
        EventBus.getDefault().register(this);
        tvDeductExplain.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvDeductExplain.getPaint().setAntiAlias(true);//抗锯齿

        // 切换状态
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String cityId = getCityIds(mOperatorInfo3.getCity_info()).get(position);
                if (!mCityId.equals(cityId)) {
                    mCityId = cityId;
                    int index = getCityIds(mOperatorInfo3.getCity_info()).indexOf(mCityId);
                    setData(mOperatorInfo3.getCity_info().get(index));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;
    }

    /**
     * 获取仓库名称集合
     */
    private List<String> getCityNames(List<OperatorInfo3.DeductBean> cities) {
        List<String> cityNames = new ArrayList<>();
        for (OperatorInfo3.DeductBean city : cities) {
            cityNames.add(city.getCity_name());
        }
        return cityNames;
    }

    private List<String> getCityIds(List<OperatorInfo3.DeductBean> cities) {
        List<String> cityIds = new ArrayList<>();
        for (OperatorInfo3.DeductBean city : cities) {
            cityIds.add(city.getCity_id());
        }
        return cityIds;
    }

    @OnClick(R.id.tv_deduct_explain)
    public void deductExplain(View v) {
        Intent intent = new Intent(getActivity(), ZDDWebViewActivity.class);
        intent.putExtra(ZDDWebViewActivity.TITLE, "提成说明");
        intent.putExtra(ZDDWebViewActivity.URL, "http://www.izaodiandao.com/mobile/commission/index?id=" + (int)SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1) + "&city=" + mCityId);
        startActivity(intent);
    }

    @OnClick(R.id.tv_work_profile)
    public void workProfile() {
        startActivity(new Intent(getActivity(), WorkProfileActivity.class));
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.manage);
        if ("24".equals(SPUtils.get(getActivity().getApplicationContext(), KeyConstants.LEVEL, ""))) {
            item.setVisible(true);
        } else {
            item.setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_person_manage, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.manage:
                startActivity(new Intent(getActivity(), PersonManageActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void getData() {
        // 获取数据
        mApiService.getOperatorInfo(requestTag, (int) SPUtils.get(getActivity().getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericCallback<OperatorInfo3>(getActivity().getApplicationContext(), OperatorInfo3.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (mLayoutContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(OperatorInfo3 data) {
                        mOperatorInfo3 = data;
                        if (!TextUtils.isEmpty(data.getAvatar()))
                            Picasso.with(getActivity().getApplicationContext()).load(data.getAvatar()).into(mIvAvatar);
                        mOperatorName = data.getName();
                        mTvName.setText(data.getName());
                        mTvMobile.setText(data.getMobile());

                        if ("1".equals(data.getShow_sale_data())) {

                            if (data.getCity_info().size() > 0) {
                                mCityId = data.getCity_info().get(0).getCity_id();
                                setData(data.getCity_info().get(0));

                                spinnerCity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.spinner_item, R.id.tv_name, getCityNames(data.getCity_info())));
                                spinnerCity.setSelection(0);
                            }

                        } else {
                            llHide.setVisibility(View.GONE);
                            vHide.setVisibility(View.GONE);
                            svHide.setVisibility(View.GONE);
                        }

                        mLayoutContent.setVisibility(View.VISIBLE);
                        getActivity().invalidateOptionsMenu();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void setData(OperatorInfo3.DeductBean data) {
        tvOpenNum.setText(data.getShop_numed());
        tvOpenableNum.setText(data.getShop_numing());
        tvSaleAmount.setText("￥" + data.getStaplefood());
        tvDeductFactor.setText("️x" + data.getCoefficiented());
        tvDeductEstimate.setText("￥" + data.getSalesmanfee());
        tvDeductFactorNextMonth.setText("x️" + data.getCoefficienting());
        if ("0".equals(data.getCoefficientnext())) {
            line.setVisibility(View.GONE);
            llNextFactor.setVisibility(View.GONE);
        } else {
            line.setVisibility(View.VISIBLE);
            llNextFactor.setVisibility(View.VISIBLE);
            tvNextFactor.setText("升至x️" + data.getCoefficientnext() + "还差");
            tvDifference.setText("￥" + data.getStaplefoodnext());
        }
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        getData();
    }

    @OnClick(R.id.ll_operator_info)
    public void changeInfo(){
        Intent intent = new Intent(getActivity(), OperatorInfoActivity.class);
        intent.putExtra(KEY_OPERATOR_NAME, mOperatorName);
        startActivity(intent);
    }

    @OnClick(R.id.tv_shop_detail)
    public void toShopDetail(View v) {
        startActivity(new Intent(getActivity(), ShopSellActivity.class));
    }

    @OnClick(R.id.tv_deduct_detail)
    public void toDeduct(View v) {
        startActivity(new Intent(getActivity(), DeductActivity.class));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.code == EventCode.CHANGE_MOBILE)
            getData();
    }
}
