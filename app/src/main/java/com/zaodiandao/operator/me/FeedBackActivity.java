package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.net.ZddStringCallback;
import com.zaodiandao.operator.util.AppUtils;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;
import com.zaodiandao.operator.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Request;

/**
 * Created by yanix on 2017/3/22.
 * 意见反馈
 */
public class FeedBackActivity extends BaseActivity {
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.rb_1)
    RadioButton rb1;
    @BindView(R.id.rb_2)
    RadioButton rb2;
    @BindView(R.id.et_problem_desc)
    EditText etProblemDesc;
    @BindView(R.id.et_contact)
    EditText etContact;

    private String classId;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                classId = checkedId == R.id.rb_1 ? "1" : "2";
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_submit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.submit:
                if (TextUtils.isEmpty(classId)) {
                    ToastUtils.showMessage(getApplicationContext(), "请选择反馈类型");
                    return true;
                }
                String problemDesc = etProblemDesc.getText().toString().trim();
                if (TextUtils.isEmpty(problemDesc)) {
                    ToastUtils.showMessage(getApplicationContext(), "请输入问题描述");
                    return true;
                }
                String contact = etContact.getText().toString().trim();
                if (TextUtils.isEmpty(contact)) {
                    contact = " ";
                }

                mApiService.feedback(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                        classId, problemDesc, contact, AppUtils.getInstance().getVersion(getApplicationContext()),
                        new ZddStringCallback(getApplicationContext(), "message") {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        DialogHelper.showDimDialog(FeedBackActivity.this, "正在提交...");
                    }

                    @Override
                    public void onAfter(int id) {
                        DialogHelper.dismissDialog();
                        super.onAfter(id);
                    }

                    @Override
                    public void success(String response) {
                        ToastUtils.showMessage(getApplicationContext(), response);
                        finish();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
