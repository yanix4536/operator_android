package com.zaodiandao.operator.me.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.me.PurchaseActivity;
import com.zaodiandao.operator.model.SaleEstimateBean;

import java.util.List;

public class SaleEstimateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int VIEW_TYPE_EMPTY = -1;
    private Context mContext;
    private List<SaleEstimateBean.EstimateItem> mDatas;

    public SaleEstimateAdapter(Context context, List<SaleEstimateBean.EstimateItem> datas) {
        mContext = context;
        mDatas = datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_EMPTY) {
            return new EmptyViewViewHolder(LayoutInflater.from(mContext).inflate(R.layout.empty_recycler_view, parent, false));
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.listitem_sale_estimate, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            view.setOnClickListener(this);
            return holder;
        }
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    //define interface
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, SaleEstimateBean.EstimateItem data);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_EMPTY) {
            EmptyViewViewHolder emptyViewViewHolder = (EmptyViewViewHolder) holder;
            emptyViewViewHolder.tvEmptyView.setText("暂无预估单");
        } else {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            final SaleEstimateBean.EstimateItem data = mDatas.get(position);
            myViewHolder.itemView.setTag(data);
            myViewHolder.tvProductName.setText("订单号：" + data.getOrdernumber());
            myViewHolder.tvCreateTime.setText(data.getCreate_time());
            myViewHolder.tvCheckTime.setText(data.getCheck_time());
            myViewHolder.tvStatus.setText(data.getStatus_label());
            int status = data.getStatus_id();
            myViewHolder.line.setVisibility(View.GONE);
            myViewHolder.tvChange.setVisibility(View.GONE);
            myViewHolder.tvChange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, PurchaseActivity.class);
                    intent.putExtra(PurchaseActivity.KEY_ESTIMATE_ID, data.getEstimate_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            });
            if (status == 1) {  // 未审核
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#FDB53B"));
                myViewHolder.line.setVisibility(View.VISIBLE);
                myViewHolder.tvChange.setVisibility(View.VISIBLE);
            } else if (status == 2) {   // 已审核
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#4AB1FC"));
            } else if (status == 3) {   // 已配送
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#00A02C"));
            } else if (status == 4) {   // 已入库
                myViewHolder.tvStatus.setTextColor(Color.parseColor("#EA2E33"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mDatas != null && mDatas.size()>0) ? mDatas.size() : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas == null || mDatas.size() == 0) {
            return VIEW_TYPE_EMPTY;
        }
        return position;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取数据
            mOnItemClickListener.onItemClick(v, (SaleEstimateBean.EstimateItem) v.getTag());
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductName, tvStatus, tvCreateTime, tvCheckTime, tvChange;
        View line;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = (TextView) view.findViewById(R.id.tv_product_name);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvCreateTime = (TextView) view.findViewById(R.id.tv_create_time);
            tvCheckTime = (TextView) view.findViewById(R.id.tv_check_time);
            tvChange = (TextView) view.findViewById(R.id.tv_change);
            line = view.findViewById(R.id.line);
        }
    }

    static class EmptyViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmptyView;

        public EmptyViewViewHolder(View itemView) {
            super(itemView);
            tvEmptyView = (TextView) itemView.findViewById(R.id.tv_empty_view);
        }
    }
}