package com.zaodiandao.operator.me;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.config.KeyConstants;
import com.zaodiandao.operator.me.adapter.DeductAdapter;
import com.zaodiandao.operator.model.DeductModel;
import com.zaodiandao.operator.net.GenericCallback;
import com.zaodiandao.operator.util.DialogHelper;
import com.zaodiandao.operator.util.SPUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by yanix on 2016/11/22.
 * 门店管理界面
 */
public class DeductActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.pb_loading)
    ContentLoadingProgressBar mProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.spinner_city)
    Spinner spinnerCity;
    @BindView(R.id.spinner_brand)
    Spinner spinnerBrand;
    @BindView(R.id.spinner_month)
    Spinner spinnerMonth;
    @BindView(R.id.tv_retry)
    TextView mTvRetry;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<DeductModel.OrderBean> datas = new ArrayList<>();
    private DeductAdapter adapter;

    private DeductModel mDeductModel;

    // 类型
    private String mCityId;
    private String mBrandId;
    private String mMonth;

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_deduct);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);

        adapter = new DeductAdapter(getApplicationContext(), datas);
        mRecyclerView.setAdapter(adapter);

        // 切换状态
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String cityId = getCityIds(mDeductModel.getCitys()).get(position);
                if (!mCityId.equals(cityId)) {
                    mCityId = cityId;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // 切换状态
        spinnerBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String brandId = getBrandIds(mDeductModel.getBrands()).get(position);
                if (!mBrandId.equals(brandId)) {
                    mBrandId = brandId;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // 切换状态
        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String month = mDeductModel.getMonths().get(position);
                if (!mMonth.equals(month)) {
                    mMonth = month;
                    requestData(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        fetchData();
    }

    /**
     * 获取仓库名称集合
     */
    private List<String> getCityNames(List<DeductModel.City> cities) {
        List<String> cityNames = new ArrayList<>();
        for (DeductModel.City city : cities) {
            cityNames.add(city.getName());
        }
        return cityNames;
    }

    private List<String> getCityIds(List<DeductModel.City> cities) {
        List<String> cityIds = new ArrayList<>();
        for (DeductModel.City city : cities) {
            cityIds.add(city.getId());
        }
        return cityIds;
    }

    private List<String> getBrandNames(List<DeductModel.Brand> brands) {
        List<String> brandNames = new ArrayList<>();
        for (DeductModel.Brand brand : brands) {
            brandNames.add(brand.getName());
        }
        return brandNames;
    }

    private List<String> getBrandIds(List<DeductModel.Brand> brands) {
        List<String> brandIds = new ArrayList<>();
        for (DeductModel.Brand brand : brands) {
            brandIds.add(brand.getId());
        }
        return brandIds;
    }

    public void fetchData() {
        // 获取数据
        mApiService.getDeduct(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1),
                new GenericCallback<DeductModel>(getApplicationContext(), DeductModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(DeductModel data) {
                        mDeductModel = data;
                        datas.clear();
                        datas.addAll(data.getOrders());
                        datas.add(new DeductModel.OrderBean("", "合计", "共" + data.getTotal_ordernum() + "单", data.getTotal_money()));

                        adapter.notifyDataSetChanged();

                        mCityId = data.getCity_id();
                        List<DeductModel.City> cities = data.getCitys();

                        spinnerCity.setAdapter(new ArrayAdapter<>(DeductActivity.this, R.layout.spinner_item, R.id.tv_name, getCityNames(cities)));
                        spinnerCity.setSelection(getCityIds(cities).indexOf(mCityId));

                        mBrandId = data.getBrand_id();
                        List<DeductModel.Brand> brands = data.getBrands();
                        spinnerBrand.setAdapter(new ArrayAdapter<>(DeductActivity.this, R.layout.spinner_item, R.id.tv_name, getBrandNames(brands)));
                        spinnerBrand.setSelection(getBrandIds(brands).indexOf(mBrandId));

                        mMonth = data.getMonth();
                        spinnerMonth.setAdapter(new ArrayAdapter<>(DeductActivity.this, R.layout.spinner_item, R.id.tv_name, data.getMonths()));
                        spinnerMonth.setSelection(data.getMonths().indexOf(mMonth));

                        llContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }


    public void requestData(final boolean isShowDialog) {
        // 获取数据
        mApiService.getDeduct(requestTag, (int) SPUtils.get(getApplicationContext(), KeyConstants.OPERATOR_ID, -1), mCityId, mBrandId, mMonth,
                new GenericCallback<DeductModel>(getApplicationContext(), DeductModel.class) {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        super.onError(call, e, id);
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure() {
                        super.failure();
                        if (llContent.getVisibility() != View.VISIBLE)
                            mTvRetry.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void success(DeductModel data) {
                        mDeductModel = data;
                        datas.clear();
                        datas.addAll(data.getOrders());
                        datas.add(new DeductModel.OrderBean("", "合计", "共" + data.getTotal_ordernum() + "单", data.getTotal_money()));
                        adapter.notifyDataSetChanged();

                        mCityId = data.getCity_id();
                        mBrandId = data.getBrand_id();
                        mMonth = data.getMonth();
                    }

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        if (isShowDialog) {
                            DialogHelper.showDimDialog(DeductActivity.this, getString(R.string.dialog_loading_text));
                        }
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        mProgressBar.setVisibility(View.GONE);
                        if (mRefreshLayout.isRefreshing()) {
                            mRefreshLayout.setRefreshing(false);
                        }

                        if (isShowDialog) {
                            DialogHelper.dismissDialog();
                        }
                    }
                });
    }

    @OnClick(R.id.tv_retry)
    public void retry(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        mTvRetry.setVisibility(View.GONE);
        fetchData();
    }

    @Override
    public void onRefresh() {
        requestData(false);
    }
}
