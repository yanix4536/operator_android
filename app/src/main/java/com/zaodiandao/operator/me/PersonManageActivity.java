package com.zaodiandao.operator.me;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zaodiandao.operator.BaseActivity;
import com.zaodiandao.operator.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by yanix on 2017/2/22.
 */
public class PersonManageActivity extends BaseActivity {

    @Override
    protected void getView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_person_manage);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.tv_stock_search)
    public void toStockSearch(View v) {
        startActivity(new Intent(this, StockSearchActivity.class));
    }

    @OnClick(R.id.tv_sale_statistics)
    public void toSaleStatistics(View v) {
        startActivity(new Intent(this, SaleStatisticsActivity.class));
    }

    @OnClick(R.id.tv_sale_estimate)
    public void toSaleEstimate(View v) {
        startActivity(new Intent(this, SaleEstimateActivity.class));
    }
}
