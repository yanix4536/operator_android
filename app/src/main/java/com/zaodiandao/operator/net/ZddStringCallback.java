package com.zaodiandao.operator.net;

import android.content.Context;

import com.zaodiandao.operator.R;
import com.zaodiandao.operator.util.ToastUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

/**
 * Created by yanix on 2016/11/24.
 * 根据业务封装网络回调
 */
public abstract class ZddStringCallback extends StringCallback {
    Context mContext;
    String mKey;

    public ZddStringCallback(Context context, String key) {
        mContext = context;
        mKey = key;
    }

    @Override
    public void onError(Call call, Exception e, int id) {
        if (!call.isCanceled())
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_no_network));
    }

    @Override
    public void onResponse(String response, int id) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int code = jsonObject.getInt("code");
            if (code == 200) {
                JSONObject data = jsonObject.getJSONObject("data");
                String resp = data.getString(mKey);
                success(resp);
            } else {
                String error = jsonObject.getString("error");
                ToastUtils.showMessage(mContext, error);
                failure();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_json));
            failure();
        }
    }

    public abstract void success(String response);

    public void failure(){}
}
