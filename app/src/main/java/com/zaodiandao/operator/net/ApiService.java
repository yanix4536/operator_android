package com.zaodiandao.operator.net;

import android.os.Build;
import android.text.TextUtils;

import com.zaodiandao.operator.config.URLConstants;
import com.zaodiandao.operator.model.ShopInfoPost;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

/**
 * Created by yanix on 2016/11/24.
 * api接口服务类
 */
public class ApiService {
    private static ApiService instance = null;

    private ApiService() {
    }

    public static ApiService getInstance() {
        if (instance == null)
            instance = new ApiService();
        return instance;
    }

    /**
     * 业务员登录
     * @param tag           请求TAG
     * @param account       账号
     * @param password      密码
     * @param callback      回调接口
     */
    public void login(String tag, String account, String password, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_LOGIN)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("account", account)
                .addParams("password", password)
                .build()
                .execute(callback);
    }

    /**
     * 商户分布
     * @param tag           请求TAG
     * @param clerkId       业务员ID
     * @param cityId        城市ID
     * @param brandId       品牌ID
     * @param callback      回调接口
     */
    public void shopDistribute(String tag, int clerkId, String cityId, String brandId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_DISTRIBUTE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .addParams("city_id", cityId)
                .addParams("brand_id", brandId)
                .build()
                .execute(callback);
    }

    /**
     * 获取业务员个人信息
     * @param tag           请求TAG
     * @param clerkId       业务员ID
     * @param callback      回调接口
     */
    public void getOperatorInfo(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_OPERATOR_INFO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_3)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取业务员个人信息
     * @param tag           请求TAG
     * @param clerkId       业务员ID
     * @param callback      回调接口
     */
    public void getOperatorInfo2(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_OPERATOR_INFO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 修改密码
     * @param tag
     * @param clerkId
     * @param password      新密码
     * @param callback
     */
    public void changePassword(String tag, int clerkId, String password, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_CHANGE_PASSWORD_OR_MOBILE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("password", password)
                .build()
                .execute(callback);
    }

    /**
     * 修改手机号
     * @param tag
     * @param clerkId
     * @param mobile        新手机号
     * @param callback
     */
    public void changeMobile(String tag, int clerkId, String mobile, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_CHANGE_PASSWORD_OR_MOBILE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("mobile", mobile)
                .build()
                .execute(callback);
    }

    /**
     * 获取业务员历史销售数据
     * @param tag
     * @param clerkId
     * @param brandId       品牌ID
     * @param callback
     */
    public void historySale(String tag, int clerkId, String brandId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_HISTORY_SALE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("brand_id", brandId)
                .build()
                .execute(callback);
    }

    /**
     * 获取业务员销售排名
     * @param tag
     * @param clerkId
     * @param brandId
     * @param date         销售月份
     * @param callback
     */
    public void saleRank(String tag, int clerkId, String brandId, String date, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SALE_RANK)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("brand_id", brandId)
                .addParams("date", date)
                .build()
                .execute(callback);
    }

    /**
     * 门店管理
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void shopManage(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_MANAGE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 门店管理
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void shopManage(String tag, int clerkId, String brandId, String statusId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_MANAGE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .addParams("brand_id", brandId + "")
                .addParams("status_id", statusId + "")
                .build()
                .execute(callback);
    }

    /**
     * 门店查看
     * @param tag
     * @param clerkId
     * @param restaurantId      门店ID
     * @param callback
     */
    public void shopView(String tag, int clerkId, String restaurantId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_VIEW)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("restaurant_id", restaurantId)
                .build()
                .execute(callback);
    }

    /**
     * 商户开店
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void shopApply(String tag, int clerkId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_APPLY)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("tag", type)
                .build()
                .execute(callback);
    }

    /**
     * 商户信息
     * @param tag
     * @param clerkId
     * @param shopId        商户ID
     * @param callback
     */
    public void shopDetail(String tag, int clerkId, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 修改商户信息
     * @param tag
     * @param clerkId
     * @param shopId        商户ID
     * @param shopInfoPost  商户信息参数类
     * @param callback
     */
    public void updateShopInfo(String tag, int clerkId, String shopId, ShopInfoPost shopInfoPost, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_UPDATE_OR_SAVE_SHOP)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .addParams("city_id", shopInfoPost.getCityId() + "")
                .addParams("country_id", shopInfoPost.getCountry() + "")
                .addParams("name", shopInfoPost.getName())
                .addParams("address", shopInfoPost.getAddress())
                .addParams("linkman", shopInfoPost.getLinkman())
                .addParams("mobile", shopInfoPost.getMobile())
                .addParams("longitude", shopInfoPost.getLongitude() + "")
                .addParams("latitude", shopInfoPost.getLatitude() + "")
                .build()
                .execute(callback);
    }

    /**
     * 创建商户
     * @param tag
     * @param clerkId
     * @param shopInfoPost
     * @param callback
     */
    public void createShopInfo(String tag, int clerkId, ShopInfoPost shopInfoPost, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_UPDATE_OR_SAVE_SHOP)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .addParams("city_id", shopInfoPost.getCityId() + "")
                .addParams("country_id", shopInfoPost.getCountry() + "")
                .addParams("name", shopInfoPost.getName())
                .addParams("address", shopInfoPost.getAddress())
                .addParams("linkman", shopInfoPost.getLinkman())
                .addParams("mobile", shopInfoPost.getMobile())
                .addParams("longitude", shopInfoPost.getLongitude() + "")
                .addParams("latitude", shopInfoPost.getLatitude() + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取城市区县等信息
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getCityData(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_GET_CITY_DATA)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取商户证照
     * @param tag
     * @param clerkId
     * @param shopId
     * @param callback
     */
    public void getShopPhoto(String tag, int clerkId, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_PHOTO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 删除商户图片
     * @param tag
     * @param clerkId
     * @param shopId
     */
    public void deleteShopPhoto(String tag, int clerkId, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_DELETE_FILE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取App版本信息
     * @param tag
     * @param clerkId
     * @param appVersion
     * @param appVersionCode
     * @param callback
     */
    public void getUpdateInfo(String tag, int clerkId, String appVersion, String appVersionCode, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_APP_UPDATE_INFO)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("mobile_models", Build.BRAND + ": " + Build.MODEL)
                .addParams("mobile_version", Build.VERSION.SDK_INT + ": " + Build.VERSION.RELEASE)
                .addParams("app_version", appVersion)
                .addParams("app_version_code", appVersionCode)
                .build()
                .execute(callback);
    }

    /**
     * 获取订货单列表
     * @param tag
     * @param clerkId
     * @param shopId
     * @param callback
     */
    public void getReserveOrders(String tag, int clerkId, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_RESERVE_ORDER)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取订货单详情
     * @param tag
     * @param clerkId
     * @param orderId
     * @param callback
     */
    public void getReserveOrderDetail(String tag, int clerkId, String orderId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_RESERVE_ORDER_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("order_id", orderId)
                .build()
                .execute(callback);
    }

    /**
     * 获取商户品牌
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getShopBrand(String tag, int clerkId, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_BRAND)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 获取配货列表
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getOrderList(String tag, int clerkId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("type", type + "")
                .build()
                .execute(callback);
    }

    /**
     * 今日同城订单
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getOrderTodayList(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 搜索订单
     * @param tag
     * @param clerkId
     * @param keyword
     * @param callback
     */
    public void searchOrder(String tag, int clerkId, String keyword, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SEARCH_ORDER)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("keyword", keyword)
                .build()
                .execute(callback);
    }

    /**
     * 历史订单
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getOrderMonthList(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_MONTH_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 历史订单
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getOrderMonthList(String tag, int clerkId, String step, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_MONTH_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION_2)
                .addParams("clerk_id", clerkId + "")
                .addParams("step", step)
                .build()
                .execute(callback);
    }

    /**
     * 获取配货单详情
     * @param tag
     * @param clerkId
     * @param orderId
     * @param callback
     */
    public void getOrderDetail(String tag, int clerkId, String orderId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ORDER_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("order_id", orderId)
                .build()
                .execute(callback);
    }

    /**
     * 库存查看
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getStockSearch(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_STOCK_SEARCH)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 库存查看
     * @param tag
     * @param clerkId
     * @param storeId
     * @param callback
     */
    public void getStockSearch(String tag, int clerkId, String storeId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_STOCK_SEARCH)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("store_id", storeId)
                .build()
                .execute(callback);
    }

    /**
     * 销量统计
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getSaleStatistics(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SALE_STATISTICS)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 销量统计
     * @param tag
     * @param clerkId
     * @param brand_id
     * @param month
     * @param callback
     */
    public void getSaleStatistics(String tag, int clerkId, String brand_id, String month, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SALE_STATISTICS)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("brand_id", brand_id)
                .addParams("month", month)
                .build()
                .execute(callback);
    }

    /**
     * 获取预估单列表
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getEstimateList(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_ESTIMATE_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取采购列表
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getProductList(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_PURCHASE_PRODUCT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取采购列表
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getProductList(String tag, int clerkId, String estimateId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_PURCHASE_PRODUCT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("estimate_id", estimateId)
                .build()
                .execute(callback);
    }

    /**
     * 保存采购单
     * @param tag
     * @param clerkId
     * @param estimateId
     * @param products
     * @param callback
     */
    public void savePurchaseProduct(String tag, int clerkId, String estimateId, String products, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_SAVE_PRODUCT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("estimate_id", TextUtils.isEmpty(estimateId) ? "0" : estimateId)
                .addParams("products", products)
                .build()
                .execute(callback);
    }

    /**
     * 获取审核列表
     * @param tag
     * @param clerkId
     * @param type
     * @param callback
     */
    public void getAuditList(String tag, int clerkId, String type, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_AUDIT_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("tag", type)
                .build()
                .execute(callback);
    }

    /**
     * 获取审核列表
     * @param tag
     * @param clerkId
     * @param type
     * @param callback
     */
    public void getAuditList(String tag, int clerkId, String type, String status, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_AUDIT_LIST)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("tag", type)
                .addParams("status", status)
                .build()
                .execute(callback);
    }

    /**
     * 获取预估单详情
     * @param tag
     * @param clerkId
     * @param estimateId
     * @param callback
     */
    public void getPurchaseDetail(String tag, int clerkId, String estimateId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_PURCHASE_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("estimate_id", estimateId)
                .build()
                .execute(callback);
    }

    /**
     * 获取审核详情
     * @param tag
     * @param clerkId
     * @param shopapplyId
     * @param callback
     */
    public void getAuditDetailWithShop(String tag, int clerkId, String shopapplyId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_AUDIT_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shopapply_id", shopapplyId)
                .build()
                .execute(callback);
    }

    /**
     * 获取审核详情
     * @param tag
     * @param clerkId
     * @param restaurantId
     * @param callback
     */
    public void getAuditDetailWithBrand(String tag, int clerkId, String restaurantId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_AUDIT_DETAIL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("restaurant_id", restaurantId)
                .build()
                .execute(callback);
    }

    /**
     * 审核操作
     * @param tag
     * @param clerkId
     * @param shopapplyId
     * @param restaurantId
     * @param type
     * @param remark
     * @param salesmanId
     * @paraallback
     */
    public void actionAudit(String tag, int clerkId, String shopapplyId, String restaurantId, String type, String remark, String salesmanId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_SUBMIT_AUDIT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shopapply_id", shopapplyId)
                .addParams("restaurant_id", restaurantId)
                .addParams("type", type)
                .addParams("remark", remark)
                .addParams("salesman_id", salesmanId)
                .build()
                .execute(callback);
    }

    /**
     * 商户经营
     * @param tag
     * @param clerkId
     * @param shopId
     * @param callback
     */
    public void actionShopOperate(String tag, int clerkId, String shopId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_OPERATE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("shop_id", shopId)
                .build()
                .execute(callback);
    }

    /**
     * 意见反馈
     * @param tag
     * @param clerkId
     * @param type
     * @param content
     * @param linknumber
     * @param appVersion
     * @param callback
     */
    public void feedback(String tag, int clerkId, String type, String content, String linknumber, String appVersion, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.post()
                .url(URLConstants.URL_FEEDBACK)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("type", type)
                .addParams("content", content)
                .addParams("linknumber", linknumber)
                .addParams("app_version", appVersion)
                .addParams("mobile_info", Build.BRAND + ": " + Build.MODEL + "     " + Build.VERSION.SDK_INT + ": " + Build.VERSION.RELEASE)
                .build()
                .execute(callback);
    }

    /**
     * 获取商户销售详情
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getShopSell(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_SELL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取商户销售详情
     * @param tag
     * @param clerkId
     * @param cityId
     * @param brandId
     * @param month
     * @param callback
     */
    public void getShopSell(String tag, int clerkId, String cityId, String brandId, String month, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_SHOP_SELL)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("city_id", cityId)
                .addParams("brand_id", brandId)
                .addParams("month", month)
                .build()
                .execute(callback);
    }

    /**
     * 获取提成
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void getDeduct(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_DEDUCT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }

    /**
     * 获取提成
     * @param tag
     * @param clerkId
     * @param cityId
     * @param brandId
     * @param month
     * @param callback
     */
    public void getDeduct(String tag, int clerkId, String cityId, String brandId, String month, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_DEDUCT)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .addParams("city_id", cityId)
                .addParams("brand_id", brandId)
                .addParams("month", month)
                .build()
                .execute(callback);
    }

    /**
     * 工作资料
     * @param tag
     * @param clerkId
     * @param callback
     */
    public void actionWorkProfile(String tag, int clerkId, Callback callback) {
        OkHttpUtils.getInstance().cancelTag(tag);
        OkHttpUtils.get()
                .url(URLConstants.URL_WORK_PROFILE)
                .tag(tag)
                .addParams("version", URLConstants.API_VERSION)
                .addParams("clerk_id", clerkId + "")
                .build()
                .execute(callback);
    }
}
