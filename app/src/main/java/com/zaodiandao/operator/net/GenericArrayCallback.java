package com.zaodiandao.operator.net;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.zaodiandao.operator.R;
import com.zaodiandao.operator.util.ToastUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.Call;

/**
 * Created by yanix on 2016/11/25.
 * 将json装换为单个实体类
 */
public abstract class GenericArrayCallback<T> extends StringCallback {
    private Context mContext;
    Class<T> mClass;

    public GenericArrayCallback(Context context, Class<T> cls) {
        mContext = context;
        mClass = cls;
    }

    @Override
    public void onError(Call call, Exception e, int id) {
        if (!call.isCanceled())
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_no_network));
    }

    @Override
    public void onResponse(String response, int id) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int code = jsonObject.getInt("code");
            if (code == 200) {
                String data = jsonObject.getString("data");
                success(JSON.parseArray(data, mClass));
            } else {
                String error = jsonObject.getString("error");
                ToastUtils.showMessage(mContext, error);
                failure();
            }
        } catch (JSONException e) {
            ToastUtils.showMessage(mContext, mContext.getString(R.string.error_json));
            failure();
        }
    }

    public abstract void success(List<T> data);

    public void failure() {

    }
}
