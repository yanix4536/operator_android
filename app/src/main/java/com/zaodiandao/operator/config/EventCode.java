package com.zaodiandao.operator.config;

/**
 * Created by yanix on 2016/11/30.
 * Event Bus事件状态码
 */
public class EventCode {
    // 修改手机号成功
    public static final int CHANGE_MOBILE = 1;
    // 修改商户信息成功
    public static final int CHANGE_SHOP_INFO = 2;
    // 创建商户成功
    public static final int CREATE_SHOP_INFO = 3;
    // 创建门店成功
    public static final int CREATE_RESTAURANT = 4;
    // 刷新采购产品
    public static final int REFRESH_PRODUCT = 5;
    // 刷新
    public static final int REFRESH = 6;
}
