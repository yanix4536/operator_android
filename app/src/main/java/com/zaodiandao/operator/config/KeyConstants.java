package com.zaodiandao.operator.config;

/**
 * Created by yanix on 2016/11/24.
 */
public class KeyConstants {
    // 业务员登录ID
    public static final String OPERATOR_ID = "operator_id";
    public static final String LEVEL = "level";

    public static final String ORIG_X = "orig_x";
    public static final String ORIG_Y = "orig_y";
    public static final String ORIG_WIDTH = "orig_width";
    public static final String ORIG_HEIGHT = "orig_height";
    public static final String IMG_URL = "img_url";

    public static final String ACTION_CHANGE = "action_change";             // 订货数量改变
    public static final String ACTION_CART_CLEAR = "action_cart_clear";     // 清空购物车
    public static final String ACTION_REFRESH = "action_refresh";     // 刷新菜品列表
    // 金额保留小数点后的位数
    public static final int SCALE = 2;
    public static final int ADD = 1;
    public static final int REDUCE = 2;
    public static final int ANIM_DURATION = 400;

    public static final String TAG = "yanix";
}
