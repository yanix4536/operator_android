package com.zaodiandao.operator.config;

/**
 * Created by yanix on 2016/11/24.
 * API接口地址
 */
public class URLConstants {
    // public static final String API_BASE_URL = "http://smapi.zdd.dailingna.cn/";
    public static final String API_BASE_URL = "http://salesman.izaodiandao.com/";
    // API版本控制
    public static final String API_VERSION = "v1";
    public static final String API_VERSION_2 = "v2";
    public static final String API_VERSION_3 = "v3";
    // 业务员登录
    public static final String URL_LOGIN = API_BASE_URL + "v1/clerk/login";
    // 商户分布
    public static final String URL_SHOP_DISTRIBUTE = API_BASE_URL + "v1/shop/restaurantdistribution";
    // 业务员个人信息
    public static final String URL_OPERATOR_INFO = API_BASE_URL + "v1/clerk/home";
    // 修改密码或手机号
    public static final String URL_CHANGE_PASSWORD_OR_MOBILE = API_BASE_URL + "v1/clerk/updateinfo";
    // 业务员历史销售数据
    public static final String URL_HISTORY_SALE = API_BASE_URL + "v1/clerk/salesmanhistory";
    // 业务员销售排名
    public static final String URL_SALE_RANK = API_BASE_URL + "v1/clerk/fellows";
    // 门店管理
    public static final String URL_SHOP_MANAGE = API_BASE_URL + "v1/restaurant/restauranthome";
    // 商户品牌
    public static final String URL_SHOP_BRAND = API_BASE_URL + "v1/shop/join";
    // 门店查看
    public static final String URL_SHOP_VIEW = API_BASE_URL + "v1/restaurant/restaurantinfo";
    // 商户开店
    public static final String URL_SHOP_APPLY = API_BASE_URL + "v1/shop/shophome";
    // 商户信息
    public static final String URL_SHOP_DETAIL = API_BASE_URL + "v1/shop/shopinfo";
    // 修改或保存商户信息
    public static final String URL_UPDATE_OR_SAVE_SHOP = API_BASE_URL + "v1/shop/shopsave";
    // 获取城市区县等数据
    public static final String URL_GET_CITY_DATA = API_BASE_URL + "v1/shop/jumpaddshop";
    // 获取商户证件照
    // public static final String URL_SHOP_PHOTO = API_BASE_URL + "v1/shop/shopinfopicture";
    public static final String URL_SHOP_PHOTO = API_BASE_URL + "v1/shop/poster";
    // 上传商户证照
    // public static final String URL_UPLOAD_FILE = API_BASE_URL + "v1/shop/uploadimage";
    public static final String URL_UPLOAD_FILE = API_BASE_URL + "v1/shop/upposter";
    // 删除文件
    public static final String URL_DELETE_FILE = API_BASE_URL + "v1/shop/deleteimage";
    // 上传门店照片
    public static final String URL_UPLOAD_SHOP_PHOTO = API_BASE_URL + "v1/restaurant/restaurantsave";
    // app更新信息
    public static final String URL_APP_UPDATE_INFO = API_BASE_URL + "v1/clerk/appupdate";
    // 获取订货单列表
    public static final String URL_RESERVE_ORDER = API_BASE_URL + "v1/shop/stationorders";
    // 获取订货单详情
    public static final String URL_RESERVE_ORDER_DETAIL = API_BASE_URL + "v1/shop/orderdetail";
    // 获取配货列表
    public static final String URL_ORDER_LIST = API_BASE_URL + "v1/stationorder/list";
    public static final String URL_ORDER_MONTH_LIST = API_BASE_URL + "v1/stationorder/monthList";
    public static final String URL_SEARCH_ORDER = API_BASE_URL + "v1/stationorder/search";
    // 获取配货单详情
    public static final String URL_ORDER_DETAIL = API_BASE_URL + "v1/stationorder/detail";
    // 库存查看
    public static final String URL_STOCK_SEARCH = API_BASE_URL + "v1/info/storestock";
    // 销量统计
    public static final String URL_SALE_STATISTICS = API_BASE_URL + "v1/info/sellbill";
    // 预估单列表
    public static final String URL_ESTIMATE_LIST = API_BASE_URL + "v1/estimate/list";
    // 采购产品列表
    public static final String URL_PURCHASE_PRODUCT = API_BASE_URL + "v1/estimate/eproduct";
    // 保存或修改采购产品
    public static final String URL_SAVE_PRODUCT = API_BASE_URL + "v1/estimate/save";
    // 获取审核列表
    public static final String URL_AUDIT_LIST = API_BASE_URL + "v1/restaurant/schedule";
    // 预估单详情
    public static final String URL_PURCHASE_DETAIL = API_BASE_URL + "v1/estimate/detail";
    // 获取审核详情
    public static final String URL_AUDIT_DETAIL = API_BASE_URL + "v1/restaurant/scheduleDetail";
    // 审核提交
    public static final String URL_SUBMIT_AUDIT = API_BASE_URL + "v1/restaurant/shopAction";
    // 商户经营
    public static final String URL_SHOP_OPERATE = API_BASE_URL + "v1/info/operate";
    // 意见反馈
    public static final String URL_FEEDBACK = API_BASE_URL + "v1/info/appfeedback";
    // 获取商户销售详情
    public static final String URL_SHOP_SELL = API_BASE_URL + "v1/clerk/shoporderdetail";
    // 获取提成
    public static final String URL_DEDUCT = API_BASE_URL + "v1/clerk/foodmoneydetail";
    // 工作资料
    public static final String URL_WORK_PROFILE = API_BASE_URL + "v1/article/list";
}
